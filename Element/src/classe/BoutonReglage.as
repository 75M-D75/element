package classe {
	
	import flash.display.MovieClip;
	
	public class BoutonReglage extends MovieClip {
		
		private var activer:Boolean;
		
		public function BoutonReglage() {
			// constructor code
		}

		public function active():void{
			gotoAndStop(1);
			activer = true;
		}

		public function desactive():void{
			gotoAndStop(2);
			activer = false;
		}

		public function getActiver():Boolean{
			return activer;
		}
	}
	
}

//http://vulkan-news.com/quantico-saison-1-episode-21-143894.htm