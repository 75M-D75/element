package classe{
	
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.display.Sprite;
    
    import classe.Effect;
    
	public class Bulles extends Sprite {
    
        var effet:Effect = new Effect();//pour l'eclate bulle
    
        var vitesseX:Number;
        var vitesseY:Number;
        var vitesseDeforme:Number;
        var acceleration:Number;
        var directionX:Number;
        var directionY:Number;
        var positionX:Number;
        var positionY:Number;
        var finY:int;
        var taille:Number;
        
        /*Constructeur*/
		public function Bulle(positionX:Number, positionY:Number, taille:Number, opacity:Number, minimum_y:int=150, marge:int=200):void{
            /*Inite*/
            update(positionX, positionY, taille, opacity);
            
            /*Creation de la bulle*/
            var bull:bulle = new bulle();
            bull.x = positionX;
            bull.y = positionY;
            bull.width = bull.height = taille;
            bull.alpha = opacity;
            bull.addEventListener(Event.ENTER_FRAME, move);
            
            this.directionY = -1;
            this.directionX = Math.cos(Math.ceil(Math.random()*2));
            this.vitesseY = Math.random()*3+0.1;
            this.vitesseX = Math.random()*2+0.1;
            this.vitesseDeforme = Math.random()*0.4+0.1;
            this.finY = Math.ceil(Math.random()*marge+minimum_y);
            this.addChild(bull);
        }
        
        private function update(positionX:Number, positionY:Number, taille:Number, opacity:Number):void{
            this.positionX = positionX;
            this.positionY = positionY;
            this.taille = taille;
            this.alpha = opacity;
        }
 
        private function move(evt:Event):void{
			if( evt.target is MovieClip ){
				var bull:MovieClip = MovieClip(evt.target);
	            bull.y -= this.vitesseY;
	            bull.x += this.vitesseX*this.directionX;
	
	            if( bull.y < this.finY || bull.x < 0+this.taille || bull.x > 400-this.taille ){
	                
                    effet.effet3_frame(this, bull.x, bull.y, Math.ceil(Math.random()*6+3), 2, 7);
	                this.removeChild(bull);
	                bull.removeEventListener(Event.ENTER_FRAME, move);
	            }
			}
        }
        
	}
}