package  classe{
	
    import flash.display.*;
    import fl.motion.Color;
    import fl.transitions.Tween;
    import fl.transitions.easing.*;
    import fl.transitions.TweenEvent;
    
	import classe.Colore;

	public class Cercle extends Sprite {
		
        /*Variable*/
        public var position_x:Number;
        public var position_y:Number;
        private var petit:Boolean = true;
        private var temp:Number = 20;
        private var rayon:int;
        private var color:Colore = new Colore();

        private var squareTarget:Sprite;
		
		/*Twenns Tab*/
		private var twenn:Array = new Array();
		private var twennH:Array = new Array();
		private var twennA :Array = new Array();
		private var twenn2:Array = new Array();
		private var twenn2H:Array = new Array();
		private var twenn2A :Array = new Array();
        
        /*Constructeur*/
        public function Cercle(rayon:int){
            squareTarget = new TargetSquare;
            //this.graphics.lineStyle(5, 0xAAAAFF, .8);
            //this.graphics.drawRoundRect(-rayon, -rayon, rayon*2, rayon*2, 5);
            //this.graphics.lineStyle(4, 0xAAAAFF, .8);
            //this.graphics.drawRoundRect(-rayon+10, -rayon+10, (rayon*2)-20, (rayon*2)-20, 5);
            this.rayon = rayon;
            this.alpha = 0;
            squareTarget.width = rayon  + 10;
            squareTarget.height = rayon + 10;
            this.x = this.y = -100;
            addChild(squareTarget);
        }
       
        public function isPetit():Boolean{
            return petit;
        }

        public function grossir_cercle():void{
            twenn.push (  new Tween(this, "width", Bounce.easeOut, 300, rayon  + 10, temp));
            twennH.push(  new Tween(this, "height", Bounce.easeOut, 300, rayon  + 10, temp));
            twennA.push(  new Tween(this, "alpha", Regular.easeOut, 0, 1, temp));
            
            Tween(twennH[twennH.length-1]).addEventListener(TweenEvent.MOTION_CHANGE, change);
            petit = false;
        }

        public function retrecir_cercle():void{
            twenn2.push ( new Tween(this, "width" , Bounce.easeOut , rayon  + 10, 10, temp));
            twenn2H.push( new Tween(this, "height", Bounce.easeOut , rayon  + 10, 10, temp));
            twenn2A.push( new Tween(this, "alpha" , Regular.easeOut, 1, 0, temp));
            
            Tween(twenn2H[twenn2H.length-1]).addEventListener(TweenEvent.MOTION_CHANGE, change);
            petit = true;
        }
        
        private function change(evt:TweenEvent):void{
            update();
        }
        
        public function update():void{
            this.x = this.position_x;
            this.y = this.position_y;
            //rect_mask.x = this.x;
            //rect_mask.y = this.y;
        }
        
		public function change_color(num_color:int):void{
			color.colore(num_color);

			var new_color:Color = new Color(); 
            new_color.setTint( color.getColor(), 1 );
			this.transform.colorTransform = new_color;
		}
	
	}
}