package  classe{
	
    import flash.display.MovieClip;

	import fl.transitions.Tween;
	import fl.transitions.easing.*;
    import fl.transitions.TweenEvent;
    import flash.events.Event;
    import flash.display.Sprite; 
    import flash.filters.*; 
    import flash.geom.*;

	public class Chargement extends Sprite {
        
        public var car:MovieClip = new etoile();
        
        // Apply the glow filter to the cross shape. 
        private var glow:GlowFilter = new GlowFilter(); 
       
        private var lueur:GlowFilter = new GlowFilter();
        private var tweenHolder:Array = new Array();
        private var sprite:Sprite = new Sprite();
        
        public function Chargement(){
            car.alpha = 0.001;
            car.height = 60;
            car.scaleX = car.scaleY;
            this.addChild(car);
            
            glow.color = couleur;
            glow.alpha = 0.8;
            glow.blurX = 8;
            glow.blurY = 8;
            glow.quality = BitmapFilterQuality.MEDIUM; 
            
            lueur.color = 0xDD0000;
            lueur.blurX = 9;
            lueur.blurY = 9;
            lueur.quality = BitmapFilterQuality.MEDIUM;
			this.width = 60;
			this.height = 60;
            addChild(sprite);
        }

        var blur:BlurFilter;
        var origin:Point;

        var angleX:Number = 0;
        var angleY:Number = 0;
        var rayonY:Number = 20;
        var rayonX:Number = 45;
        var couleur:* = 0xA3091E;

        public function effet3(pox:Number, poy:Number, nb:int=5, color:*=0x47D1F0, angd:Number=0):void{
            
            var tab:Array = [];

            for(var i:int=0; i<nb; i++){
                var trian:MovieClip = new MovieClip();
                trian.sprite = new Sprite();
                trian.sprite.graphics.beginFill(color);
                trian.sprite.graphics.drawCircle(0, 0, 3);
                trian.angleX = angleX;
                trian.angleY = angleY;
                trian.addChild(trian.sprite);
				//trian.blendMode = BlendMode.HARDLIGHT
                addChild(trian);
                tab.push(trian);
            }

            tween();

            function tween():void{
                var temps = 0.9;
                for(var i = 0; i < tab.length; i++){

                    angleY += 0.02;
                    angleX += 0.02/2;
                    tab[i].angleX = angleX*2 + (i*0.02/2);
                    tab[i].angleY = angleY*2 + (i*0.02);
                    tab[i].y = poy+(Math.sin((angd)  +tab[i].angleY) * rayonY);
                    tab[i].x = pox+(Math.cos((angd/2)+tab[i].angleX) * rayonX);
                    
                    tweenHolder.push(new Tween(tab[i], "height", Regular.easeInOut, 7, 1, temps, true));
                    tweenHolder.push(new Tween(tab[i], "width" , Regular.easeInOut, 7, 1, temps, true));
                    //tweenHolder.push(new Tween(tab[i], "alpha" , Regular.easeNone, 1, 0, temps, true));
                    tweenHolder[tweenHolder.length-1].addEventListener(TweenEvent.MOTION_FINISH, onFinish2);
                }
            }
            
            function onFinish2(e:TweenEvent):void{
                if(Math.ceil(Math.random()*5) == 2)
                    effet3_frame(sprite, e.target.obj.x, e.target.obj.y, color);
                var obj = e.target.obj; 
                e.target.removeEventListener(TweenEvent.MOTION_FINISH, onFinish2);
                removeChild(obj);
            }
        }

        private var degre:Number = Math.PI;
        private var rayon:Number = 30;

        public function effet3_frame(gameSprite:Sprite, pox:Number,poy:Number, color:*=0x47D1F0 , nbr:Number=1):void{
            
            for(var i:int=0; i<nbr; i++){
                var trian:MovieClip = new MovieClip();
                trian.sprite = new Sprite();
                trian.sprite.graphics.beginFill(color); 
                //trian.sprite.graphics.lineStyle(1, 0x6600CC, .8);
                trian.sprite.graphics.drawCircle(0, 0, Math.random()*2+1);

                trian.addChild(trian.sprite);
                trian.mouseEnabled = false;
                
                degre += Math.random()*5;
                trian.fin_x = pox+(Math.sin(degre+i)*rayon);
                trian.fin_y = poy+(Math.cos(degre+i)*rayon);
                
                //trian.width = trian.height = 1;
                
                trian.x = pox;
                trian.y = poy;
             
                trian.addEventListener(Event.ENTER_FRAME, move);
                gameSprite.addChild(trian);
            }

            /*Animation (Mouvement) element effet*/
            function move(evt:Event):void{
                
                if( Math.abs(evt.target.x - evt.target.fin_x) > 2){
                    evt.target.x -= (evt.target.x - evt.target.fin_x) * 0.2;
                }
                if( Math.abs(evt.target.y - evt.target.fin_y) > 2){
                    evt.target.y -= (evt.target.y - evt.target.fin_y) * 0.2;
                }
                if( Math.abs(evt.target.x - evt.target.fin_x) < 2  &&  Math.abs(evt.target.y - evt.target.fin_y) < 2){
                    evt.target.removeEventListener(Event.ENTER_FRAME, move);
                    var obj =  evt.target;
                    gameSprite.removeChild(obj);
                }   
            }
        }

	}
}