package  classe{
	
    import flash.display.*;
    
	public class Colore extends Sprite{
		private var couleur:*;
		
        public function colore(num_color:int):*{
		
			switch(num_color){
                case 3:
					couleur = 0xFF0000;
                    return 0xFF0000;/*Rouge*/
					
                case 5:
					couleur = 0x747474;
                    return 0x747474;/*Gris*/
					
                case 1:
					couleur = 0xB43104;
                    return 0xB43104;/*Maron*/
                    
                case 2:
					couleur = 0x2ECCFA;
                    return 0x2ECCFA;/*Bleu*/
					
                case 4:
					couleur = 0xFF0099;
                    return 0xFFFFFF;/*Rose*/
					
				default:
					couleur = 0x2ECCFA;
                    return 0x2ECCFA;/*Bleu*/
            
            }
		}
		
		public function getColor():*{
			return this.couleur;
		}
        
	
	}
}