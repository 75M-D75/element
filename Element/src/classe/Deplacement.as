package classe{
	
    import flash.events.Event;
    import flash.utils.Timer;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import fl.transitions.Tween;
    import fl.transitions.easing.*;
    import fl.transitions.TweenEvent;
    import flash.display.*;
    import flash.utils.*;
	
	public class Deplacement extends Sprite {
    
        public const spacing:Number = 50;
        public var offsetX:Number = 0;
        public var offsetY:Number = 0;
        public var gameSprite:Sprite = new Sprite();
        
        var cible:Sprite = new Sprite();
        var cacheCible:Boolean = true;
        
        var twenn:Tween;
        
        /*
         * Constructeur : cree la cible
         */
        public function Deplacement(){
            this.createSp();
        }

        public function assert(truth:*): void
        {
            if ( !truth ){
                throw new Error("Assertion failed!");
            }
        }
        
        /**
         * Replacement des pion d'un tableau:grid selon leur spacing, offsetY, et offsetX
         */
        public function replacer(grid:Array, col:int ,row:int):void{
            //trace("replacer")
            if( grid[col][row] != null ){
            
                // Déplacement vers le bas
                if ( grid[col][row].y < grid[col][row].row*spacing+offsetY && grid[col][row].can_tombe != false){
                    if( grid[col][row].y+50 <= grid[col][row].row*spacing+offsetY ){
                        grid[col][row].y += 50;
                    }
                    else{
                        grid[col][row].y = grid[col][row].row*spacing+offsetY;
                    }
                    
                } 
                
                // Déplacement vers le haut
                if ( grid[col][row].y > grid[col][row].row*spacing+offsetY ) {
                    grid[col][row].y -= 10;
                } 
                
                if( grid[col][row].can_deplace ){

                    // Déplacement vers la droite
                    if ( ( grid[col][row].x < grid[col][row].col*spacing+offsetX && grid[col][row].selectionner != true && grid[col][row].movee != true ) 
                    && ( col-1 >= 0 && ( grid[col-1][row] == null || grid[col-1][row].selectionner != true || verf(grid, grid[col][row]) == true  ) ) 
                    ){
                        //trace("ici")
                        grid[col][row].x = grid[col][row].col*spacing+offsetX;
                        grid[col][row].posx = null;
                        grid[col][row].movee = false;
                           
                    }

                    // Déplacement vers la gauche
                    else if ( ( grid[col][row].x > grid[col][row].col*spacing+offsetX && grid[col][row].selectionner != true && grid[col][row].movee != true ) 
                    && ( col+1 < 6 && ( grid[col+1][row] == null || grid[col+1][row].selectionner != true || verf(grid, grid[col][row]) == true  )  ) 
                    ){
                        
                        grid[col][row].x = grid[col][row].col*spacing+offsetX;
                        grid[col][row].posx = null;
                        grid[col][row].movee = false;
                        
                        //trace("deplace 4")
                    }
                    else if ( grid[col][row].x > grid[col][row].col*spacing+offsetX && grid[col][row].selectionner != true && grid[col][row].x > 250){
                        
                        grid[col][row].x = grid[col][row].col*spacing+offsetX;
                        grid[col][row].posx = null;
                        grid[col][row].movee = false;
                    }
                }
            }
        }
        
        
        /**
         * Verifie si il y'a une correspondance avec l'element désigné
         * grid : Tableau de jeu
         * ob : Element à verifier
         * @return boolean Retourne vrai si des Element de meme type sont alignés
         */
        public function verf(grid:Array, ob:Object):Boolean{
			if( ob is Element ){
				var o:Element = Element(ob);
				
	            if( (o != null && !o.mur && ( o.col+2 <= 5 && grid[o.col+2][o.row] != null && grid[o.col+1][o.row] != null && grid[o.col+1][o.row].sameType(o) && grid[o.col+2][o.row].sameType(o) && o.can_elimine != false )||
	            
	            ( o.col-2 >= 0 && grid[o.col-2][o.row] != null && grid[o.col-1][o.row] != null && grid[o.col-1][o.row].sameType(o) && grid[o.col-2][o.row].sameType(o) && o.can_elimine != false ) ||
	            
	            ( o.col-1 >= 0 && o.col+1 <= 5 && grid[o.col+1][o.row] != null && grid[o.col-1][o.row] != null && grid[o.col-1][o.row].sameType(o) && grid[o.col+1][o.row].sameType(o) && o.can_elimine != false ) ||
	            
	            ( o.row+2 < 12 && grid[o.col][o.row+1] != null && grid[o.col][o.row+2] != null && !grid[o.col][o.row+1].mur && !grid[o.col][o.row+2].mur && grid[o.col][o.row+1].sameType(o) && grid[o.col][o.row+2].sameType(o) && o.can_elimine != false ) ||
	            
	            ( o.row-1 >= 0 && o.row+1 < 12 && grid[o.col][o.row+1] != null && grid[o.col][o.row-1] != null && !grid[o.col][o.row-1].mur && !grid[o.col][o.row+1].mur && grid[o.col][o.row+1].sameType(o) && grid[o.col][o.row-1].sameType(o) && o.can_elimine != false )||
	            
	            ( o.row-2 >= 0 && grid[o.col][o.row-1] != null && grid[o.col][o.row-2] != null && !grid[o.col][o.row-1].mur && !grid[o.col][o.row-2].mur && grid[o.col][o.row-1].sameType(o) && grid[o.col][o.row-2].sameType(o) && o.can_elimine != false ) 
	            ) ){
	                return true;
	            }
			}
            return false;
        }

        /* Cible */
        private function createSp():void{
            var couleur:* = 0xCC3333;
            cible.graphics.lineStyle(5, couleur, 1);
            cible.graphics.drawCircle(0, 0, 20);
            cible.graphics.lineStyle(3, couleur, 1);
            cible.graphics.drawCircle(0, 0, 20 - 7);
            cible.graphics.lineStyle(1, couleur, 1);
            cible.graphics.drawCircle(0, 0, 20 - 14);
            cible.blendMode = "hardlight";
        }

        private var twennCible:Array = new Array;

        private function apparitionCible():void{
            twennCible.push( new Tween(cible, "scaleX", Bounce.easeOut, 4, 1, 0.8, true) );
            twennCible.push( new Tween(cible, "scaleY", Bounce.easeOut, 4, 1, 0.8, true) );
        }
        
        /* 
         * Pour deplacement rapide en dessous d'un pion tombant 
         * grid : Tableau de jeu
         * pion_selectionner:Element selectionné
         */
        public function deplace_pion( grid:Array, pion_selectionner:Object ):void{
            if(pion_selectionner != null && pion_selectionner.posx != null && grid[pion_selectionner.col][pion_selectionner.row+1] != null 
                && pion_selectionner.selectionner == true && pion_selectionner.x % 25 == 0 && pion_selectionner.aligne != true && pion_selectionner.posx <= 251 
                && pion_selectionner.can_deplace
                ){

                var col = pion_selectionner.col;
                var row = pion_selectionner.row;

                if( cacheCible && ( pion_selectionner.x - Math.ceil(pion_selectionner.posx) < -25 || pion_selectionner.x - Math.ceil(pion_selectionner.posx) >= 25) 
                    && ((col-1 >= 0 && grid[col-1][row] != null && grid[col-1][row].aligne ) || (col+1 < 6 && grid[col+1][row] != null && grid[col+1][row].aligne)) ){
                    cible.alpha = 0.7;
                    cible.y = pion_selectionner.y;
                    addChild(cible);
                    cacheCible = false;
                    apparitionCible();
                }

                if( pion_selectionner.x - Math.ceil(pion_selectionner.posx) < -25/*exemple si 150-155>25*/){
                    cible.x = (pion_selectionner.col+1)*spacing+offsetX;

                    if( grid[pion_selectionner.col+1][pion_selectionner.row] == null || (grid[pion_selectionner.col+1][pion_selectionner.row] != null && grid[pion_selectionner.col+1][pion_selectionner.row].aligne != true 
                     ) ){
                        pion_selectionner.x += 50;
                        //pion_selectionner.movee = true;
                        pion_selectionner.posx = null;
                        
                        twenn = new Tween(cible, "alpha", Bounce.easeOut, cible.alpha, 0, 0.8, true);

                        twenn.addEventListener(TweenEvent.MOTION_FINISH, onFinish);
                    }
                }
                else if(pion_selectionner.x - Math.ceil(pion_selectionner.posx) >= 25/*exemple si 150-145 > 25*/){
                    
                    cible.x = (pion_selectionner.col-1)*spacing+offsetX;

                    if(pion_selectionner.col-1 >= 0 && grid[pion_selectionner.col-1][pion_selectionner.row] != null && grid[pion_selectionner.col-1][pion_selectionner.row].aligne != true || pion_selectionner.col-1 >= 0 && grid[pion_selectionner.col-1][pion_selectionner.row] == null || pion_selectionner.col == 0 ){
                        pion_selectionner.x -= 50;
                        //pion_selectionner.movee = true;
                        pion_selectionner.posx = null;

                        twenn = new Tween(cible, "alpha", Bounce.easeOut, cible.alpha, 0, 0.8, true);

                        twenn.addEventListener(TweenEvent.MOTION_FINISH, onFinish);
                    }
                }
            }

            if( pion_selectionner != null && pion_selectionner.posx != null && pion_selectionner.selectionner != true && pion_selectionner.movee == true/*&&Math.ceil(grid[col][row].x)-Math.ceil(grid[col][row].posx)==0*/){;
                pion_selectionner.posx = null;
                //pion_selectionner.movee=false;
                changeCache();
            }
            else if(pion_selectionner != null && pion_selectionner.selectionner == false && !cacheCible && twenn != null && !twenn.isPlaying ){
                twenn = new Tween(cible, "alpha", Bounce.easeOut, 0.7, 0, 0.8, true);
                twenn.addEventListener(TweenEvent.MOTION_FINISH, onFinish);
            }

            function changeCache():void{
                if( !cacheCible ){
                    removeChild(cible);
                    cacheCible = true;
                    twennCible[twennCible.length-1].stop();
                    twennCible = new Array;
                }
            }

            function onFinish(evt:TweenEvent):void{
                changeCache();
            }
        }
        
       
        /**
         * Verifie si il y'a des elements mur en dessou de l'element (piece) 
         * grid : Tableau de jeu
         * piece: Element
         */
        private function is_mur(grid:Array, piece:Element):Boolean{

            for(var row:int=piece.row+1; row<12; row++){
                if( grid[piece.col][row] != null && grid[piece.col][row].mur == true ){
                    return true;
                }
            }

            return false;
        }
        

        /**
         * Deplacement de pion non selectionner
         * A coté ou non d'un pion selectionner
         * grid : Tableau de jeu
         * col : colonne du tableau (entier)
         * row : ligne du tableau
         */
        public function lignedeplace_2( grid:Array, col:int, row:int):Array{
            //trace("lignedeplace_2");
            if( (grid[col][row] != null && grid[col][row].aligne != true && grid[col][row].y == grid[col][row].row*spacing+offsetY ) 
            && ( grid[col][row].enr > grid[col][row].x || grid[col][row].enr < grid[col][row].x)
            ){
                //trace("lignedeplace_22");
                var col_plus:int = col+1;
                var col_moin:int = col-1;
                var i:Number;
                var difference:Number;
                var objet_en_chute:Object;
                var bol:Boolean;
                
                if(col_moin >= 0 && grid[col_moin][row] == null && grid[col][row] != null && grid[col][row+1] != null && grid[col][row].aligne != true && grid[col][row].x < grid[col][row].enr
                ){
                    for(i=12; i>=0 && i>row; i--){
                        if(grid[col_moin][i] != null && grid[col_moin][i].y < grid[col_moin][i].row*spacing+offsetY && grid[col_moin][i].row > grid[col][row].row){
                            objet_en_chute = grid[col_moin][i];
                            i = -1;
                        }
                    }

                    if( grid[col][row].x <= grid[col][row].enr-50/2 && objet_en_chute == null ){
                        grid[col][row].col--;
                        grid[col_moin][row] = grid[col][row];
                        grid[col][row] = null;
                        grid[col_moin][row].enr = grid[col_moin][row].enr-50;
                        //trace("grid[col_moin][row].enr "+grid[col_moin][row].enr)

                        return grid;
                    }
                    else if( objet_en_chute != null && objet_en_chute.y < grid[col][row].y-49 && objet_en_chute.row > grid[col][row].row){

                        difference = (Math.abs(objet_en_chute.row - grid[col][row].row));
                        
                        for( i=0; i <= grid[col][row].row + difference; i++){
                            if(grid[col_moin][i] != null){
                                bol = is_mur(grid, grid[col_moin][i]);
                            }
                            
                            /* Faire remonté le Pion */
                            if(grid[col_moin][i] != null && grid[col_moin][i].y != grid[col_moin][i].row*spacing+offsetY && grid[col_moin][i].mur != true && bol != true){
                                grid[col_moin][i].row -= difference+1;
                                grid[col_moin][i-(difference+1)] = grid[col_moin][i];
                                grid[col_moin][i] = null;
                            }
                        }

                        if( grid[col][row].x <= grid[col][row].enr -50/2 ){
                            grid[col][row].col--;
                            grid[col_moin][row] = grid[col][row];
                            grid[col][row] = null;
                            grid[col_moin][row].enr = grid[col_moin][row].enr - 50;
                            return grid;
                        }
                    }
                    return grid;
                }
                else if(col_moin >= 0 && grid[col][row+1] != null && grid[col_moin][row] != null && grid[col_moin][row].y < grid[col][row].y-49 && grid[col][row].x < grid[col][row].enr){
                    
                    for( i = 12; i >= 0; i--){
                        if(grid[col_moin][i] != null && grid[col_moin][i].y < grid[col_moin][i].row*spacing+offsetY){
                            objet_en_chute = grid[col_moin][i];
                            i = -1;
                        }
                    }

                    if( grid[col][row].x <= grid[col][row].enr-50/2 ){
                        if ( grid[col_moin][row] != null && objet_en_chute != null && objet_en_chute.y < grid[col][row].y-49){
                            
                            difference = (Math.abs(objet_en_chute.row - grid[col][row].row));
                            
                            for( i=0; i <= grid[col][row].row+difference; i++){
                                if(grid[col_moin][i] != null){
                                    bol = is_mur(grid, grid[col_moin][i]);
                                }

                                if(grid[col_moin][i] != null && grid[col_moin][i].y != grid[col_moin][i].row*spacing+offsetY && grid[col_moin][i].mur != true && bol != true){
                                    grid[col_moin][i].row -= difference+1;
                                    grid[col_moin][i-(difference+1)] = grid[col_moin][i];
                                    grid[col_moin][i] = null;
                                }
                            }
                            
                            if(grid[col][row].x <= grid[col][row].enr-50/2){
                                grid[col][row].col--;
                                grid[col_moin][row] = grid[col][row];
                                grid[col][row] = null;
                                grid[col_moin][row].enr = grid[col_moin][row].enr - 50;
                                return grid;
                            }
                        }
                    }
                    return grid;
                }
                else if(col_plus <= 5 && grid[col_plus][row] == null && grid[col][row+1] != null && grid[col][row].aligne != true && grid[col][row].x > grid[col][row].enr
                ){
                    for(i=12; i>=0 && i>row; i--){
                        if(grid[col_plus][i] != null && grid[col_plus][i].y < grid[col_plus][i].row*spacing+offsetY && grid[col_plus][i].row > grid[col][row].row ){
                            objet_en_chute = grid[col_plus][i];
                            i=-1;
                        }
                    }

                    if(grid[col][row] != null && objet_en_chute==null){
                        if(grid[col][row].x>grid[col][row].enr+50/2){
                            grid[col][row].col++;
                            grid[col_plus][row]=grid[col][row];
                            grid[col][row]=null;
                            grid[col_plus][row].enr=grid[col_plus][row].enr+50;
                            return grid;
                        }
                    }
                    else if(grid[col][row] != null && grid[col_plus][row]==null && objet_en_chute != null && objet_en_chute.y < grid[col][row].y-49 && objet_en_chute.row > grid[col][row].row){
                        difference=(Math.abs(objet_en_chute.row-grid[col][row].row));
                        for( i=0;i <= grid[col][row].row+difference;i++){
                            if(grid[col_plus][i] != null){
                                bol = is_mur(grid, grid[col_plus][i]);
                            }
                            if(grid[col_plus][i] != null && grid[col_plus][i].y != grid[col_plus][i].row*spacing+offsetY && grid[col_plus][i].mur != true && bol != true){
                                grid[col_plus][i].row -= difference+1;
                                grid[col_plus][i-(difference+1)] = grid[col_plus][i];
                                grid[col_plus][i] = null;
                            }
                        }
                        
                        if(grid[col][row].x>grid[col][row].enr+50/2){
                            grid[col][row].col++;
                            grid[col_plus][row]=grid[col][row];
                            grid[col][row]=null;
                            grid[col_plus][row].enr=grid[col_plus][row].enr+50;
                            return grid;
                        }
                    }
                    return grid;
                }
                else if(col_plus <= 5 && grid[col][row] != null && grid[col][row+1] != null && grid[col_plus][row] != null && grid[col_plus][row].y<grid[col_plus][row].row*spacing+offsetY && grid[col][row].x > grid[col][row].enr
                ){
                    for(i=12; i>=0 && i>row;i--){
                        if(grid[col_plus][i] != null&&grid[col_plus][i].y < grid[col_plus][i].row*spacing+offsetY){
                            objet_en_chute = grid[col_plus][i];
                            i = -1;
                        }
                    }

                    if(grid[col][row] != null && grid[col][row].x > grid[col][row].enr+50/2
                    ){
                        if (grid[col][row] != null && grid[col_plus][row] != null && objet_en_chute != null && objet_en_chute.y < grid[col][row].y-49
                        ){
                            difference = ( Math.abs(objet_en_chute.row-grid[col][row].row) );
                            for( i=0;i <= grid[col][row].row+difference;i++){
                                if(grid[col_plus][i] != null){
                                    bol = is_mur(grid, grid[col_plus][i]);
                                }
                                if(grid[col_plus][i] != null && grid[col_plus][i].y != grid[col_plus][i].row*spacing+offsetY && grid[col_plus][i].mur != true && bol != true){
                                    grid[col_plus][i].row -= difference+1;
                                    grid[col_plus][i-(difference+1)]=grid[col_plus][i];
                                    grid[col_plus][i]=null;		
                                }
                            }
                            if(grid[col][row].x > grid[col][row].enr+50/2){
                                grid[col][row].col++;
                                grid[col_plus][row]=grid[col][row];
                                grid[col][row]=null;
                                grid[col_plus][row].enr = grid[col_plus][row].enr+50;
                                return grid;
                            }
                        }
                    }
                    return grid;
                }
                if(col-1 >= 0 && grid[col][row] != null && grid[col-1][row] != null && grid[col][row+1] != null && grid[col-1][row].selectionner != true 
                && grid[col][row].movee == true && grid[col][row].enr > grid[col][row].x && grid[col][row].aligne != true && grid[col-1][row].aligne != true 
                && grid[col][row].hitTestObject(grid[col-1][row]) && grid[col][row].y == grid[col-1][row].y && grid[col-1][row+1] != null 
                && grid[col][row+1].y == grid[col-1][row+1].row*spacing+offsetY
                ){
                    //trace("gauche");
                    if( Math.abs(grid[col][row].enr-50+grid[col][row].enr-grid[col][row].x) < grid[col][row].enr && grid[col][row].x <= grid[col][row].enr){
                        grid[col-1][row].x = Math.abs(grid[col][row].enr-50+grid[col][row].enr-grid[col][row].x);
                        //trace("gauche "+(col)+" "+(col-1));
                    }		
                    if(grid[col][row].x < grid[col][row].enr-50/2){
                        grid[col-1][row].col += 1;
                        grid[6][row]=grid[col-1][row];
                        grid[col-1][row] = null;
                        grid[col][row].col--;
						
                        grid[col-1][row]=grid[col][row];
                        grid[col][row]=null;
                        grid[col][row]=grid[6][row];
                        grid[6][row]=null;
						
                        grid[col-1][row].enr = grid[col-1][row].enr-50;
                        grid[col][row].enr = grid[col][row].enr+50;
                    }
                }
                else if(grid[col][row] != null && grid[col+1][row] != null && grid[col][row+1] != null && grid[col][row].movee == true 
                && grid[col][row].enr < grid[col][row].x && grid[col][row].aligne != true && grid[col+1][row].aligne != true && grid[col][row].hitTestObject(grid[col+1][row]) == true 
                && grid[col][row].col+1 <= 5 && grid[col][row].y == grid[col+1][row].y && grid[col+1][row+1] != null && grid[col][row+1].y == grid[col+1][row+1].row*spacing+offsetY
                ){
                    //trace("droite");
                    grid[col+1][row].enr = grid[col+1][row].col*spacing+offsetX;
                    if( ( (grid[col+1][row].enr) - (grid[col][row].x-grid[col][row].enr) ) > grid[col][row].enr ){
                        grid[col+1][row].x = (grid[col+1][row].enr) - (grid[col][row].x-grid[col][row].enr);
                        //trace("droite "+(col+1)+" "+(row));
                    }
                    if( grid[col][row].x > grid[col][row].enr+(50/2) ){
                        grid[col+1][row].col-=1;
                        grid[6][row]=grid[col+1][row];
                        grid[col+1][row]=null;
                        grid[col][row].col+=1;
						
                        grid[col+1][row]=grid[col][row];
                        grid[col][row]=null;
                        grid[col][row] = grid[6][row];
                        grid[6][row]=null;
						
                        grid[col+1][row].enr = grid[col+1][row].enr+50;
                        grid[col][row].enr = grid[col][row].enr-50;
                    }
                }
            }
            return grid;
        }

     	
	}
}