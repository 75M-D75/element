﻿package  classe{
	
import flash.events.Event;
import flash.utils.Timer;
import flash.utils.*;
import flash.display.MovieClip;
import fl.motion.Color;
import flash.display.Sprite;
import classe.Deplacement;

import fl.transitions.easing.*;
import fl.transitions.*;
	
	public class Effect extends Sprite {
	
		/**
				 Attribut 
								*/
		private var degre:Number = Math.PI;
		private var rayon:Number = 20;

		/**
				Setter 
							**/
		public function set_degre(degre:int):void{
			this.degre = degre;
		}


		public function set_rayon(rayon:int):void{
			this.rayon = rayon;
		}
		
		/** 
				Methode  
							**/
		public function effet_frame( gameSprite:Sprite, pox:Number,poy:Number, obj:Object, numImage:Number=1):void{
			
			for(var i:int=0; i<4; i++){
			
				var boule:MovieClip = new neigee();
				boule.mouseEnabled = false;
				boule.gotoAndStop( numImage );
				boule.taille = Math.abs( obj.height - 20 % obj.height);
				
				if( numImage == 1 )
					boule.alpha = .6;

				/* Choix de la direction */
				if(i==0){
					
					boule.signeX=1
					boule.signeY=-1
				}
				else if(i==1){

					boule.signeX=-1
					boule.signeY=-1
				}
				else if(i==2){

					boule.signeX=-1
					boule.signeY=1
				}
				else if(i==3){

					boule.signeX=1
					boule.signeY=1
				}

				var rand = Math.ceil(Math.random()*3);
				
				boule.fin_x = pox+(boule.taille*boule.signeX)+((Math.sin(degre+rand)*50)*boule.signeX);
				boule.fin_y = poy+(boule.taille*boule.signeY)+((Math.sin(degre+rand)*50)*boule.signeY);

				boule.width = boule.height = boule.taille;
				
				boule.x = pox;
				boule.y = poy;
				
				boule.addEventListener(Event.ENTER_FRAME, move);
				gameSprite.addChild(boule);
			}
			
			
			/* Animation (Mouvement) element effet*/
			function move(evt:Event){
				var vitesse:Number = 0.14;
				//TODO > ou < peut etre a corrigé
				if( Math.abs(evt.target.x - evt.target.fin_x) > 1.5){
					evt.target.x -= (evt.target.x - evt.target.fin_x) * vitesse;
				}
				if( Math.abs(evt.target.y - evt.target.fin_y) > 1.5){
					evt.target.y -= (evt.target.y - evt.target.fin_y) * vitesse;
				}
				if( Math.abs(evt.target.x - evt.target.fin_x) <= 1.5  &&  Math.abs(evt.target.y - evt.target.fin_y) <= 1.5){
					evt.target.removeEventListener(Event.ENTER_FRAME, move);
					var obj = evt.target;
					gameSprite.removeChild(obj);
				}
				
				/* Reduction de taille */
				evt.target.width -= 1;
				evt.target.scaleY = evt.target.scaleX;
				if( Math.ceil(Math.random()*3) == 1 )
					effet3_frame(gameSprite, evt.target.x, evt.target.y, 2, numImage);
			}
			
		}


		
		public function effet3_frame(gameSprite:Sprite, pox:Number,poy:Number, nbr:Number=2, numImage:int=1, taille:int=9):void{
			
			for(var i:int=0; i<nbr; i++){
			
				var boule:MovieClip = new neigee();
				boule.mouseEnabled = false;
				boule.gotoAndStop(numImage);
				
				degre += Math.random() * 5;
				boule.fin_x = pox+(Math.sin(degre+i)*rayon);
				boule.fin_y = poy+(Math.cos(degre+i)*rayon);
				boule.vitesse = 0.4;
				boule.width = boule.height = taille;
				
				boule.x = pox;
				boule.y = poy;
			 
				boule.addEventListener(Event.ENTER_FRAME, move);
				gameSprite.addChild(boule);

			}
			
			/*Animation (Mouvement) element effet*/
			function move(evt:Event):void{
				if( Math.abs(evt.target.x - evt.target.fin_x) > 1.5){
					evt.target.x -= (evt.target.x - evt.target.fin_x) * evt.target.vitesse;
				}
				if( Math.abs(evt.target.y - evt.target.fin_y) > 1.5){
					evt.target.y -= (evt.target.y - evt.target.fin_y) * evt.target.vitesse;
				}
				if( Math.abs(evt.target.x - evt.target.fin_x) <= 1.5 &&  Math.abs(evt.target.y - evt.target.fin_y) <=1.5){
					evt.target.removeEventListener(Event.ENTER_FRAME, move);
					var obj =  evt.target;
					gameSprite.removeChild(obj);
				}
			}
		}
	

		
		public function effet_combo(gameSprite:Sprite, centreX:Number,centreY:Number, taille:int=50, nbr:int=3, rayon:int=150, numImage:Number=1):void{
		   
			var tab:Array = new  Array();
			var tab2:Array = [[],[],[],[],[],[],[],[],[]];
			var vitesse:Number = 0.2;
			var tabe:Array = [[],[],[],[],[],[],[],[],[]];
			
			for(var i:int=0; i<nbr; i++){
				var boule:neigee = new neigee();
				boule.mouseEnabled = false;
				boule.gotoAndStop(numImage);
				boule.graphics.lineStyle(50, 0xAAAAFF, 0.5);
				boule.graphics.beginFill(0xAAAAFF); 
				boule.graphics.drawCircle(0, 0, 100);
				boule.graphics.endFill();
				boule.width = boule.height = taille;
				
				boule.rayon = 150;
				boule.angles = (2*(Math.PI)+2*i*(Math.PI))/nbr;
				boule.x = centreX+(Math.cos(boule.angles)*boule.rayon);
				boule.y = centreY+(-Math.sin(boule.angles)*boule.rayon);
				gameSprite.addChild(boule);
				tab.push(boule);
				
				//Point traking
				for(var a:int=0; a<5 ; a++){
					var boule2:neigee = new neigee();
					boule2.mouseEnabled = false;
					boule2.gotoAndStop(numImage);
					boule2.width = boule2.height = taille-(a*2);
					boule2.t = 0;
					boule2.vitesse = vitesse - (a/150);
					
					boule2.rayon = 150 + (a*2);
					boule2.angles = (2*(Math.PI)+2*i*(Math.PI))/nbr;
					boule2.x = centreX+(Math.cos(boule2.angles)*boule2.rayon);
					boule2.y = centreY+(-Math.sin(boule2.angles)*boule2.rayon);
					boule2.addEventListener(Event.ENTER_FRAME, animeTracking);
					//gameSprite.addChild(boule2);
					tab2[i].push(boule2);
				}
				
			}
			
			function traking(sp:Sprite, tab:Array, num:int){
				var ligne:Sprite = new Sprite;
				for(var i=1; i<tab.length; i++){

					if( i == 1 )
						ligne.graphics.lineStyle((tab.length-i)+10, 0x00CCCC, 0.2);
					else
						ligne.graphics.lineStyle((tab.length-i)+10, 0x00CCCC, 0.8);
					
					ligne.graphics.moveTo(tab[i-1].x, tab[i-1].y);
					ligne.graphics.lineTo(tab[i].x, tab[i].y);
				}
				
				tabe[num].push(ligne);
				sp.addChild(ligne);
			}
			
			function delTraking(tabe:Array ,sp:Sprite, last:Boolean=false){
				
				for(var e=0; e<tabe.length-1; e++){
					if(tabe[e] != null && sp.contains(tabe[e])){
						sp.removeChild(tabe[e]);
						tabe[e] = null;
					}
				}

				
				if(last && sp.contains(tabe[tabe.length-1])){
					
					for( e=0; e<tabe.length; e++){
						if(tabe[e] != null && sp.contains(tabe[e])){
							sp.removeChild(tabe[e]);
							tabe[e] = null;
						}
					}
				}
			}
			
			function animeTracking(evt:Event):void{
				var obj = evt.target;
				obj.rayon -= 5;
				obj.angles -= obj.vitesse;
				obj.x = centreX+( Math.cos(obj.angles) * obj.rayon);
				obj.y = centreY+(-Math.sin(obj.angles) * obj.rayon);
				obj.t += obj.vitesse; 
				if(obj.rayon < 0){
					
					obj.removeEventListener(Event.ENTER_FRAME, animeTracking);
					obj.rayon = rayon;
					if(gameSprite.contains(obj))
						gameSprite.removeChild(obj);
					obj = null;
				}
			}
			
			addEventListener(Event.ENTER_FRAME, anime);
			
			/*Mouvement circulaire ce rapprochant du centre*/
			function anime():void{

				for(var e=0; e<tab.length; e++){
					tab[e].rayon -= 5;
					tab[e].angles -= 0.2;
					tab[e].x = centreX+(Math.cos(tab[e].angles)*tab[e].rayon);
					tab[e].y = centreY+(-Math.sin(tab[e].angles)*tab[e].rayon);
					traking(gameSprite, tab2[e], e);
					delTraking(tabe[e], gameSprite);
				}
					
				
				if(boule.rayon < 0){
					
					removeEventListener(Event.ENTER_FRAME, anime);
					boule.rayon = rayon;
					
					for(e=0; e<tab.length; e++){
						gameSprite.removeChild(tab[e]);
						tab[e] = null;
						delTraking(tabe[e], gameSprite, true);
					}
					
					
					
					tab.splice(0, tab.length);
				}
			}
		}

		private function abs(nombre:Number):Number{
			return Math.abs(nombre);
		}

		//Pour le lancement de boule
		public function effet_roue(gameSprite:Sprite, ex:int, efx:int, ey:int, efy:int, deplacement:Deplacement):void{
					
			var tab:Array = new Array;
			var sp:Sprite = new Sprite;
			var rayon:Number;
			var vitesse:Number = Math.random() * 0.2 + 0.2;
			var hypotenus:Number;
			var opposer:Number;
			var adjacant:Number;
			var rotZ:Number;
			var neg:Number = 1;//permet de determiner le sens negatif de la courbe
			var e:int;
			var tabe:Array = new Array;

			var posInitDeplacement:int = deplacement.offsetY;

			for(var i=0; i<10; i++){
				var boule:MovieClip = new neigee();
				boule.mouseEnabled = false;

				if( i==0 ){
					boule.gotoAndStop(4);
				}
				else{
					boule.gotoAndStop(4);
				}

				boule.vitesse = vitesse-(i/150);
				boule.height = boule.width = 16-i;
				boule.t = 0;//temps
				boule.num = i;
				if( i == 0 )
					sp.addChild(boule);

				gameSprite.addChild(sp);
				sp.x = ex;
				sp.y = ey;

				//Calcule du rayon et de l'angle de rotation (sur l'axe Z)
				adjacant = abs(efx-ex);
				opposer = abs(efy-ey);

				hypotenus = (adjacant*adjacant) + (opposer*opposer);
				hypotenus = Math.sqrt(hypotenus);
				rotZ = Math.acos(adjacant/hypotenus) * 57.296;

				if( ex >= efx && ey >= efy ){
					hypotenus = -hypotenus;
				}
				else if( ex >= efx && ey < efy  ){
					hypotenus = -hypotenus;
					rotZ = -rotZ;
					neg = -1;
				}
				else if( ex < efx && ey >= efy ){
					rotZ = -rotZ;
					neg = -1;
				}
				else if( ex < efx && ey < efy ){
					//hypotenus = -hypotenus;
					//neg = -1;
				}

				sp.rotationZ = rotZ;
				boule.rotationZ = rotZ;
				boule.rayon = hypotenus/6.2;
				tab.push(boule);
				boule.addEventListener(Event.ENTER_FRAME, anime);
				
			}
			
			traking(sp);
			delTraking(sp);
			
			function traking(sp:Sprite){
				var ligne:Sprite = new Sprite;
				for(var i=1; i<tab.length; i++){

					if( i == 1 )
						ligne.graphics.lineStyle((tab.length-i)+10, 0x00CCCC, 0.4);
					else
						ligne.graphics.lineStyle((tab.length-i)+10, 0x00CCCC, 0.8);
					
					ligne.graphics.moveTo(tab[i-1].x, tab[i-1].y);
					ligne.graphics.lineTo(tab[i].x, tab[i].y);
				}

				tabe.push(ligne);
				sp.addChild(ligne);
				tab[tab.length-1].sp = sp;
			}
			
			function delTraking(sp:Sprite, last:Boolean=false){
				
				for( e=0; e<tabe.length-1; e++){
					if(tabe[e] != null && sp.contains(tabe[e])){
						sp.removeChild(tabe[e]);
						tabe[e] = null;
					}
				}

				
				if(last && sp.contains(tabe[tabe.length-1])){
					
					for( e=0; e<tabe.length; e++){
						if(tabe[e] != null && sp.contains(tabe[e])){
							sp.removeChild(tabe[e]);
							tabe[e] = null;
						}
					}
				}
			}

			
			/*Mouvement circulaire ce rapprochant du centre*/
			function anime(evt:Event):void{ 
				var obj = evt.target;
				//if( obj == tab[tab.length-1] )
					//trace("obj.t ",obj.t);
				//arc de cercle
				// x(t) = r(t − sin t)
				// y(t) = r(1 − cos t)
				// obj.x = obj.rayon * (obj.t - Math.sin(obj.t));
				// obj.y = neg * (obj.rayon) * (1 - Math.cos(obj.t) );
				
				//coeur
				// x(t) = sin(t)^3
				// y(t) = cos(t) - cos(t)^4
				//semi coeur
				// x(t) = sin(t)^3
				// y(t) = cos(t) - cos(t)^4
				// obj.x = 100 * Math.pow(Math.sin(obj.t) , 2);
				// obj.y = neg * (100) * (Math.cos(obj.t) - Math.pow(Math.cos(obj.t), 3) );

				//x(t) = 4*(sin(t/2)^1)
				//y(t) = 3* (cos(t) - cos(t)^(3))
				var posy = posInitDeplacement - deplacement.offsetY;
				if(obj.t <= 0 + obj.vitesse){
					obj.x = obj.rayon * ( Math.pow(Math.sin(obj.t/2) , 1) );
					obj.y = neg * (100) * (Math.cos(obj.t) - Math.pow(Math.cos(obj.t), 3) );
				}
				else if(obj.t > 0 + obj.vitesse){
					obj.x = obj.rayon * (obj.t - Math.sin(obj.t));
					obj.y = posy + (neg * (obj.rayon) * (1 - Math.cos(obj.t) ));
				}

				obj.t += obj.vitesse;
				if( obj.num == 9 ){
					traking(obj.sp);
				}
				
				if(obj.num == 9)
					delTraking(obj.sp);
				
				//supression des boules si l'objectif est ateint
				if(obj.t >= 6 + obj.vitesse){
					obj.removeEventListener(Event.ENTER_FRAME, anime);
					if( obj == tab[tab.length-1] ){
						for(var i:int=0; i<tab.length; i++)
							if( sp.contains(tab[i]) )
								sp.removeChild(tab[i]);
						delTraking(obj.sp, true);
					}
				}
			}
		}

		
		public function effetRebondSize(element:Object, sizeDep:Number, sizeFin:Number){
			var twennArray:Array = new Array()
			var tween_menu_gagne:Tween  = new Tween(element, "width ", Regular.easeOut, sizeDep, sizeFin, 0.1, true);
			var tween_menu_gagne2:Tween = new Tween(element, "height", Regular.easeOut, sizeDep, sizeFin, 0.1, true);

			tween_menu_gagne2.addEventListener(TweenEvent.MOTION_FINISH, onFinish);
			
			function onFinish(evt:TweenEvent){
				var tween_menu_gagne:Tween  = new Tween(element, "width ", Regular.easeOut, sizeFin, sizeDep, 0.7, true);
				var tween_menu_gagne2:Tween = new Tween(element, "height", Regular.easeOut, sizeFin, sizeDep, 0.7, true);
			}
		}

		var tweenHolder:Array = new Array;

		public function cibleEffect(sp:Sprite, xc:Number, yc:Number){
			var cible:Sprite = new Sprite;
			cible.x = xc;
			cible.y = yc;
			createSp(cible);
			sp.addChild(cible);
			tweenHolder.push(new Tween(cible, "height", Regular.easeIn, 1, 400, 0.4, true));
			tweenHolder.push(new Tween(cible, "width" , Regular.easeIn, 1, 400, 0.4, true));
			tweenHolder.push(new Tween(cible, "alpha" , Regular.easeIn, 1, 0, 0.4, true));
			
			tweenHolder[tweenHolder.length-1].addEventListener(TweenEvent.MOTION_FINISH, onFinish2);
			
			function onFinish2(evt:TweenEvent){
				var obj = evt.target.obj;
				sp.removeChild(obj);
				obj = null;
			}
		}

		function createSp(cible:Sprite){
			var couleur = 0xFFFFFF;
			cible.graphics.lineStyle(3, couleur, 0.9);
			cible.graphics.drawCircle(0, 0, 50);
			/*cible.graphics.lineStyle(9, couleur, 0.8);
			cible.graphics.drawCircle(0, 0, 50 -9);
			cible.graphics.lineStyle(8, couleur, 0.7);
			cible.graphics.drawCircle(0, 0, 50 -18);
			cible.graphics.beginFill(0xFFFFFF);
			cible.graphics.drawCircle(0, 0, 50 -27);*/
			cible.alpha = 0.8;
			
			cible.blendMode = "hardlight";
		}
	
	}
}