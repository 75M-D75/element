
package  classe{
    import fl.motion.Color;
    import flash.display.Sprite;
	import flash.utils.Dictionary;
	import flash.utils.*;
	
    import flash.display.Sprite;
    import flash.geom.*;
	import flash.display.*;
    import flash.display.Shape;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	
	import classe.Validiter;
	
	public class Fct_Glace extends Sprite {
	
		private var validiter:Validiter = new Validiter();
		
		/*Ajouter Glace*/
		public function addGlace( element:Element, glace:Glace, grid:Array){
			//trace("add");	
			if( element != null && element.type != 3 && element.glace == null && !condition_del_glace(grid, element.col, element.row) && condition_add_glace(grid, element.col, element.row) ){
				//trace("add_in "+element.x+" size "+element.col+" alpha "+element.row);	
				element.addGlace(glace);
			}
		}
		
		
		/*Supprimer Glace*/
		public function delGlace( element:Element ){
			
			if( element != null && element.glace != null && element.glace_delete != true ){
				element.glace_delete = true;
				var obj = element;
				
				//var tweenAlpha = new Tween(element.glace, "alpha", Regular.easeOut, 1, 0.1, 2, true);
				var tweenScaleY = new Tween(element.glace, "scaleY", Regular.easeOut, 1, 0, 1, true);
				var tweenScaleX = new Tween(element.glace, "scaleX", Regular.easeOut, 1, 0, 1, true);
				setTimeout(disparition, 1100, obj);
				
				tweenScaleY.addEventListener(TweenEvent.MOTION_FINISH, onFinish2);
				
				function onFinish2(evt:TweenEvent):void{
					obj.can_elimine = true;
					obj.can_deplace = true;
					obj.can_select = true;
					obj.removeChild( obj.glace );
					obj.glace = null;
				}
			}
		}
		
		private function disparition( obj:Object ){
			if(obj != null){
				obj.can_elimine = true;
				obj.can_deplace = true;
				if( obj.glace != null ){
					obj.removeChild( obj.glace );
					obj.glace = null;
				}
			}
		}
		
		public function condition_del_glace(grid:Array, col,row:Number):Boolean{
			if(grid[col][row] != null && grid[col][row].glace != null){
				if( col+1<=5 && grid[col+1][row] != null && grid[col+1][row].type == 3 && grid[col+1][row].aligne == true
					||col-1>=0 && grid[col-1][row] != null  && grid[col-1][row].type == 3 && grid[col-1][row].aligne == true
					||row+1<=11 && grid[col][row+1] != null && grid[col][row+1].type == 3 && grid[col][row+1].aligne == true 
					||row-1>=0 && grid[col][row-1] != null  && grid[col][row-1].type == 3 && grid[col][row-1].aligne == true 
				){
					return true;
				}
			}
			return false;
		}

		/* ??? */
		public function condition_add_glace(grid:Array, col,row:Number):Boolean{
			return grid != null && grid[col][row] != null && verf(grid, grid[col][row]);
		}

		/**
         * @return boolean Retourne vrai si des Element de meme type sont alignés
         */
        public function verf(grid:Array, o:Object):Boolean{

            if( (o != null && !o.mur && ( o.col+2 <= 5 && grid[o.col+2][o.row] != null && grid[o.col+1][o.row] != null && grid[o.col+1][o.row].sameType(o) && grid[o.col+2][o.row].sameType(o) && o.can_elimine != false )||
            
            ( o.col-2 >= 0 && grid[o.col-2][o.row] != null && grid[o.col-1][o.row] != null && grid[o.col-1][o.row].sameType(o) && grid[o.col-2][o.row].sameType(o) && o.can_elimine != false ) ||
            
            ( o.col-1 >= 0 && o.col+1 <= 5 && grid[o.col+1][o.row] != null && grid[o.col-1][o.row] != null && grid[o.col-1][o.row].sameType(o) && grid[o.col+1][o.row].sameType(o) && o.can_elimine != false ) ||
            
            ( o.row+2 < 12 && grid[o.col][o.row+1] != null && grid[o.col][o.row+2] != null && !grid[o.col][o.row+1].mur && !grid[o.col][o.row+2].mur && grid[o.col][o.row+1].sameType(o) && grid[o.col][o.row+2].sameType(o) && o.can_elimine != false ) ||
            
            ( o.row-1 >= 0 && o.row+1 < 12 && grid[o.col][o.row+1] != null && grid[o.col][o.row-1] != null && !grid[o.col][o.row-1].mur && !grid[o.col][o.row+1].mur && grid[o.col][o.row+1].sameType(o) && grid[o.col][o.row-1].sameType(o) && o.can_elimine != false )||
            
            ( o.row-2 >= 0 && grid[o.col][o.row-1] != null && grid[o.col][o.row-2] != null && !grid[o.col][o.row-1].mur && !grid[o.col][o.row-2].mur && grid[o.col][o.row-1].sameType(o) && grid[o.col][o.row-2].sameType(o) && o.can_elimine != false ) 
            ) ){
                return true;
            }
            
            return false;
        }

		public function lookForMatches_glace(grid:Array):Array {
			var row:int
			var col:int
			var matchList:Array = new Array();

			// Rechercher les correspondances horizontales
			for(col=0; col<6; col++) {
				for (row=0;row<10;row++){
					var match:Array = getMatchVert_glace(grid, col,row);
					if (match.length > 2 && verif_glace_existe(match)==true) {
						matchList.push(match);
						row += match.length-1;
					}
				}
			}

			// Rechercher les correspondances vertical
			for(col=0;col<4;col++) {
				for (row=0;row<12;row++){
					match = getMatchHoriz_glace(grid, col,row);
					if (match.length > 2 && verif_glace_existe(match)==true) {
						matchList.push(match);
						col += match.length-1;
					}
				}
			}

			return matchList;
		}

		public function verif_glace_existe(tab:Array):Boolean{
			for(var col=0; col<tab.length; col++) {
				if(tab[col] != null && tab[col].glace != null){
					return true;
				}
			}
			return false;
		}

		private function getMatchHoriz_glace(grid:Array, col,row:int):Array {
			var match:Array = new Array(grid[col][row]);
			for(var i:int=1; col+i<6; i++) {
				if(grid[col][row] != null){
				
					var bol:Boolean = validiter.piece_nul(grid, grid[col][row]);
					
					if(bol == true){
					
						if (grid[col+i][row] != null && grid[col][row] != null && grid[col][row].type == grid[col+i][row].type
							&& grid[col][row].y == grid[col+i][row].y && grid[col][row].x == grid[col+i][row].x-i*50
							&& grid[col+i][row].aligne != true && grid[col][row].aligne != true && grid[col][row+1] != null && grid[col+i][row+1] != null
							){
							match.push(grid[col+i][row]);
						} 
						else{
							return match;
						}
					}
				}
			}
			return match;
		}

		private function getMatchVert_glace(grid:Array, col,row):Array{
			var match:Array = new Array(grid[col][row]);
			for(var i:int=1;row+i<12;i++){
				if(grid[col][row] != null){
					
					var bol:Boolean = validiter.piece_nul(grid, grid[col][row]);
					if(bol == true){
						var h=i+1
						if(grid[col][row+i] != null && grid[col][row] != null && grid[col][row].type == grid[col][row+i].type
							&& grid[col][row].y == grid[col][row+i].y-i*50 && grid[col][row].x == grid[col][row+i].x && grid[col][row+h] != null
						    && grid[col][row+i].aligne != true && grid[col][row].aligne != true
							) {
							match.push(grid[col][row+i]);
						} 
						else{
							return match;
						}
					}
				}
			}
			return match;
		}


		public function fn_glace( tab:Array, grid:Array ){	
			for(var i=0; i<tab.length; i++){
				for(var e=0; e<tab[i].length; e++){
					if( tab[i][e] != null /*&& tab[i][e].can_deplace != true*/  ){
						addGlace(tab[i][e], new Glace(50, 0, 0), grid);
					}
				}
			}
		}

	}
}