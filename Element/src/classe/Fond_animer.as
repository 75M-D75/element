package  classe{
	
    import flash.display.Shape;
    import flash.events.Event;
    import fl.motion.Color;
    import flash.display.Sprite;
    
	public class Fond_animer extends Sprite {
        
        public function colore(i:int):*{
            switch(i)
            {
                case 0:
                {
                    return 0x3366CC;/*Bleu*/
                }
                case 1:
                {
                    return 0x339966;/*Vert*/
                }
                case 2:
                {
                    return 0xFF0066;/*Rouge*/
                }
            }
            return 0xCC00FF;
        }

        public var mult = 5;
        public function trac_angle(X,Y:int, color=0x5555EE){

            stage.addEventListener(Event.ENTER_FRAME, fram);
            
            var px = X;
            var py = Y;
            var neg;
            function fram(ev:Event){
                px++;
                if( px*mult >= stage.stageWidth/2){
                    py--;
                    neg = 1;
                }
                else{
                    py++;
                    neg = -1;
                }
                var charp:Sprite = new Sprite();
                charp.graphics.beginFill(color); 
                charp.graphics.lineStyle(4, color, 0.5);
                charp.graphics.drawCircle(0, 0, 1);
                if( (px+1)*mult < (stage.stageWidth/2)-5 && (px+1)*mult > 0 || (px)*mult >= (stage.stageWidth/2) ){
					var trait:Shape = new Shape();
					trait.graphics.beginFill(color); 
					trait.graphics.lineStyle(8, color, 0.9);
					trait.graphics.lineTo(10, -10*neg);
					charp.addChild(trait);
				}
                
                charp.graphics.endFill();
                charp.x = px*mult;
                charp.y = py*mult;
                //charp.filters = [glow];
                charp.addEventListener(Event.ENTER_FRAME, disparition);
                //trait.filters = [glow];
                //charp.blendMode = "layer";
                addChild(charp);
                
                if( px*mult > stage.stageWidth )
                    stage.removeEventListener(Event.ENTER_FRAME, fram);
            }

            function disparition(ev:Event){
                ev.target.alpha -= 0.005;

                if( ev.target.alpha < 0.1 ){
                    var obj = ev.target;
                    obj.removeEventListener(Event.ENTER_FRAME, disparition);
                    removeChild(obj);
                    obj = null;
                }
            }
        }

        public function trac_2(X,Y:int, direction_y = 1, direction_x = 1, color=0x5555EE){

            stage.addEventListener(Event.ENTER_FRAME, fram);
            
            var px = X;
            var py = Y;
            var neg_x;
            var neg_y;
            function fram(ev:Event){
                /*Y*/
                if( direction_y == 1 ){
                    py--;
                    neg_y = 1;
                }
                else{
                    py++;
                    neg_y = -1;
                }
                /*X*/
                if( direction_x == 1 ){
                    px--;
                    neg_x = -1;
                }
                else{
                    px++;
                    neg_x = 1;
                }
                
                var charp:Sprite = new Sprite();
                charp.graphics.beginFill(color); 
                charp.graphics.lineStyle(2, color, 0.5);
                charp.graphics.drawCircle(0, 0, 1);
                
                var trait:Shape = new Shape();
                trait.graphics.beginFill(color); 
                trait.graphics.lineStyle(4, color, 0.9);
                trait.graphics.lineTo(10*neg_x, -10*neg_y);
                
                charp.graphics.endFill();
                charp.x = px*mult;
                charp.y = py*mult;
                //charp.filters=[glow];
                charp.addEventListener(Event.ENTER_FRAME, disparition);

                //charp.blendMode = "layer";
                addChild(charp);
                charp.addChild(trait);
                if( px*mult > stage.stageWidth || px*mult < 0 || py*mult > stage.stageHeight || py*mult < 0 )
                    stage.removeEventListener(Event.ENTER_FRAME, fram);
            }

            function disparition(ev:Event){
                ev.target.alpha -= 0.005;

                if( ev.target.alpha < 0.1 ){
                    var obj = ev.target;
                    obj.removeEventListener(Event.ENTER_FRAME, disparition);
                    removeChild(obj);
                    obj = null;
                }
            }

        }
	}
}