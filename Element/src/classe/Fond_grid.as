package  classe{
	
    import flash.events.Event;
    import flash.display.*;
    import flash.utils.Timer;
    import fl.motion.Color;
    import fl.transitions.Tween;
    import fl.transitions.easing.*;
    import fl.transitions.TweenEvent;
    import flash.events.Event;
    import flash.utils.*;
    
    import flash.display.Sprite;
    
	public class Fond_grid extends Sprite {
       
        /*Variable*/
        public var tab_rect:Array = new Array();
        public var fond_sprite:Sprite = new Sprite();
        
         
        public function make_fond_grid(gameSprite:Sprite, colore:uint = 0x0000AA, alpha:Number = 0.2){
        
            for(var i=0; i<14; i++){
                for( var e=0; e<6; e++){

					var rect_arrondi:MovieClip = new MovieClip(); 
				 
					rect_arrondi.graphics.beginFill(colore); 
                    if( i>0 ){
                        //rect_arrondi.graphics.lineStyle(2, 0x000011, .5);
					    rect_arrondi.graphics.drawRect((e*50)-25, (i*50)-75, 50, 50);
                    }
                    else{
                        //rect_arrondi.graphics.lineStyle(2, 0x000011, .5);
                        rect_arrondi.graphics.drawRect((e*50)-25, (i*50)-75, 50, 50);
                    }
					rect_arrondi.alpha = alpha;
					
					fond_sprite.addChild(rect_arrondi);
					fond_sprite.setChildIndex(rect_arrondi, 0);
					gameSprite.addChild(fond_sprite);
					gameSprite.setChildIndex(fond_sprite, 0);
					tab_rect.push(rect_arrondi);
                }
            }
            
        }
		
		
		/*ajouter un carre dans fond_sprite*/
		public function add_rect(col,row:int){
			if(tab_rect[6*row+col] == null){
				var rect_arrondi:MovieClip = new MovieClip(); 
						 
				rect_arrondi.graphics.beginFill(0x000000); 
				//rect_arrondi.graphics.lineStyle(2, 0x000011);
				rect_arrondi.graphics.drawRect((col*50)-25, (row*50)-25, 50, 50);
				fond_sprite.addChild(rect_arrondi);
				tab_rect[6*row+col] = rect_arrondi;
			}
		}
		
		/*supprimer un carre dans fond_sprite*/
		public function del_rect(col,row:int){
			if(fond_sprite != null && tab_rect[6*row+col] != null){
				fond_sprite.removeChild(tab_rect[6*row+col]);
				tab_rect[6*row+col] = null;
			}
		}
        
        private var tab_case_red:Array = new Array();

        public function change_color_case(col,row:int, colore:int = 1){
            var  red_c:Color = new Color(); 
            red_c.setTint( 0xFF0000 ,  1 ) ; 
            var  blue_c:Color = new Color(); 
            blue_c.setTint( 0x0000AA ,  1 ) ;
            var  black_c:Color = new Color(); 
            black_c.setTint( 0x00CC99 ,  1 ) ;
            
            if(colore == 1){
                tab_rect[6*(row+1)+col].transform.colorTransform = red_c;
                tab_rect[6*(row+1)+col].col = col;
                tab_rect[6*(row+1)+col].row = row;
                tab_rect[6*(row+1)+col].alpha = 0.3;
                tab_case_red.push(tab_rect[6*(row+1)+col]);
            }
            else if(colore == 2){
                tab_rect[6*(row+1)+col].transform.colorTransform = blue_c;
                tab_rect[6*(row+1)+col].alpha = 0.3;
            }
            else{
                tab_rect[6*(row+1)+col].transform.colorTransform = black_c;
                tab_rect[6*(row+1)+col].alpha = 0.3;
            }
        
        }
        
        public function recolor(){
            for(var i=0; i<tab_case_red.length; i++){
                if(tab_case_red[i] != null){
                    change_color_case(tab_case_red[i].col, tab_case_red[i].row, 2);
                }
            }
            tab_case_red = [];
        
        }
        
	}
}