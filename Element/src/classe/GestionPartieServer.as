package classe
{ 
    import flash.display.Sprite; 
    import flash.events.Event;
     
    public class GestionPartieServer
    { 
        
        private var donner;//?
        private var complet:Boolean = false;
        private var idGestion:int;
        private var numTabServeur:int;

        public var joueur:Array = new Array(2);

        /* C */
        public function GestionPartieServer( joueur:Object ){
            this.joueur[0] = joueur;
        }

        /* Getter */
        public function getIdGestion():int{
            return idGestion;
        }

        public function getNumTabServeur():int{
            return numTabServeur;
        }

        /* Setter */
        public function setIdGestion(idGestion:int){
            this.idGestion = idGestion;
        }

        public function setNumTabServeur(numTabServeur:int){
            this.numTabServeur = numTabServeur;
        }

        /* Methode */
        public function isComplet():Boolean{
            return joueur[0] != null && joueur[1] != null;
        }

        public function addPlayer( player:Object ){
            if( !complet && joueur[0] != null ){
                joueur[1] = player;
                complet = true;
            }
        }
		
		
    }

}