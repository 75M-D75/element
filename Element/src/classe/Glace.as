
package  classe{

	import flash.utils.Dictionary;
    import flash.geom.*;
	import flash.display.*;
	
	public class Glace extends Sprite {
	
		public var type:int = 14;
		public var num_tab:int;
		
		public function Glace(taille:Number, xe:Number=0, ye:Number=0 ){
			var M_glace:Sprite = new Sprite();
			
			var glaceG:Shape = new Shape();

			var glace:Glace_T = new Glace_T();
			glace.width = glace.height = taille;
			glace.alpha = 0.25;

			/*Degrade*/
			var fillType:String = GradientType.LINEAR;
			var colors:Array = [0x00AAAA, 0x00FFFF, 0x00AAAA];
			var alphas:Array = [0.5 ,1, 0.5];
			var ratios:Array = [0x00 ,0x00, 0xFF];
			var matr:Matrix = new Matrix();
			matr.createGradientBox(0.25*taille, 0.25*taille, 0, 0, 0);
			matr.rotate(Math.PI/4);

			var spreadMethod:String = SpreadMethod.REFLECT;
			glaceG.graphics.lineStyle(0.07*taille, 0x00AAAA, 0.8); 

			glaceG.graphics.beginGradientFill(fillType, colors, alphas, ratios, matr, spreadMethod);        
			glaceG.graphics.drawRect(-25, -25, 50, 50);
			glaceG.alpha = 0.35;

			M_glace.x = xe;
			M_glace.y = ye;
			
			M_glace.addChild(glaceG);
			M_glace.addChild(glace);
			
			this.addChild(M_glace);
		}
		
	}
}