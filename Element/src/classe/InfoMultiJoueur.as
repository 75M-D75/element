package classe{

	import flash.display.MovieClip;

	public class InfoMultiJoueur extends Object {

		private var _finChain:Boolean = false;
		private var _nombreChains:int = 0;
		private var _idMur:int = 0;

		//
		public function InfoMultiJoueur(nombreChains:int, idMur:int){
			this._nombreChains = nombreChains;
			this._idMur = idMur;
		}

		//
		public function get finChain():Boolean{
			return _finChain;
		}

		public function get nombreChains():int{
			return _nombreChains;
		}

		public function get idMur():int{
			return _idMur;
		}

		public function set idMur(idMur:int):void{
			this._idMur = idMur;
		}

		//
		public function finChains():void{
			_finChain = true;
		}
	}
}