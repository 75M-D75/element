package classe {
	import flash.geom.Matrix;
	import flash.display.InterpolationMethod;
	import flash.display.SpreadMethod;
	import flash.display.GradientType;
    import flash.display.Sprite;
	
	
	public class Jauge extends Sprite {

        public var jauge_Sprite:Sprite = new Sprite();
        public var fn_jauge:Sprite;
        public var jauge:Sprite;
        public var jaugeWidth:Number;

        public var load:Number = 1;

        public function Jauge(x:int, y:int, rotationZ:int, widthJ:Number){
            jaugeWidth = widthJ;
            fn_jauge = cree_fond_jauge(widthJ);
            jauge = cree_jauge(widthJ);
            jauge_Sprite.addChild(fn_jauge);
            jauge_Sprite.addChild(jauge);
            addChild(jauge_Sprite);
            super.x = x;
            super.y = y;
            super.rotationZ = rotationZ;
           
        }
        
        public function loading( pourcentage:Number ):void{ 
            load -= (Math.abs(load) - pourcentage)*0.2; 
            setBarProgress(load); 
        }

        public function setBarProgress(value:Number):void { 
            jauge.scaleY = value;
        }

        /*Creation du fond de la jauge*/
        public function cree_fond_jauge(widthJ:Number):Sprite{

            var fond_jauge:Sprite = new Sprite();
                   
            fond_jauge.graphics.beginFill(0xDDDDDD); 
            
            /*Dessiner */
            fond_jauge.graphics.drawRect(0, 0, 10, -widthJ);
            fond_jauge.graphics.endFill();

            fond_jauge.x = fond_jauge.y = 10;
            return fond_jauge;
        }

        /*Creation de la jauge*/
        public function cree_jauge(widthJ:Number):Sprite{
            var jauge:Sprite = new Sprite();
            var type:String = GradientType.LINEAR; 
            //var colors:Array = [0x00CCFF, 0x001188]; 
            var colors:Array = [0xFF1122, 0x440000];
            var alphas:Array = [1, 1]; 
            var ratios:Array = [0, 255]; 
            var spreadMethod:String = SpreadMethod.PAD; 
            var interp:String = InterpolationMethod.LINEAR_RGB; 
            var focalPtRatio:Number = 0;
            
            var matrix:Matrix = new Matrix(); 
            var boxWidth:Number = 10; 
            var boxHeight:Number = -255; 
            var boxRotation:Number = 3*Math.PI/2; // 90° 
            var tx:Number = 0; 
            var ty:Number = 0;
            matrix.createGradientBox(boxWidth, boxHeight, boxRotation, tx, ty); 

            /* Eau */
            jauge.graphics.beginGradientFill(type,  
                            colors, 
                            alphas, 
                            ratios,  
                            matrix,  
                            spreadMethod,  
                            interp,  
                            focalPtRatio);
            //jauge.graphics.beginFill(0xFF3399);  
            
            /*Dessiner */
            jauge.graphics.drawRect(0, 0, 10, -widthJ);
            jauge.x = jauge.y = 10;
            return jauge;
        }
        
	
	}
}