package  classe{
	
    import flash.display.Sprite;
    import flash.utils.*;
    import fl.transitions.Tween;
    import fl.transitions.easing.*;
    import fl.transitions.TweenEvent;
	
	public class MainTuto extends Sprite {
		
		private var tween:Tween;
		public var main:Main;
		private var intervalSet:uint;
		private var x_dep:int = 50;
		private var x_fin:int = 50*4;
		private var y_dep:int = 50;
		private var y_fin:int = 50*4;
		private var marche:Boolean = false;

		public function MainTuto(){
			main = new Main();
			main.y = 100;
			main.height = 50;
			main.scaleX = main.scaleY;
			main.mouseEnabled = false;
		}

		private function move_mainX( x_dep:int, x_fin:int):void{
			main.alpha = 1;
			tween = new Tween(main, "x", Regular.easeOut, x_dep, x_fin, 1, true);
			tween.addEventListener(TweenEvent.MOTION_FINISH, onFinish);
						
			function onFinish(evt:TweenEvent):void{
				var twee:Tween = new Tween(main, "alpha", Regular.easeOut, 1, 0, 0.6, true);
			}
		}
		
		private function move_mainY( y_dep:int, y_fin:int):void{
			main.alpha = 1;
			tween = new Tween(main, "y", Regular.easeOut, y_dep, y_fin, 1, true);
			tween.addEventListener(TweenEvent.MOTION_FINISH, onFinish);
						
			function onFinish(evt:TweenEvent):void{
				var twee:Tween = new Tween(main, "alpha", Regular.easeOut, 1, 0, 0.6, true);
			}
		}

		public function deplaceX_or_Y( num:int ):void{
			if( !marche ){
				switch( num ){
					case 1:
						marche = true;
						x_dep = 50;
						x_fin = 50*4;
						main.y = 10*50;
						addChild(main);
						move_mainX( x_dep, x_fin );
						intervalSet = setInterval(move_mainX ,2000, x_dep, x_fin);
						break;
					case 11:
						marche = true;
						y_dep = 500;
						y_fin = 350;
						main.x = 300;
						addChild(main);
						move_mainY(  y_dep, y_fin );
						intervalSet = setInterval(move_mainY ,2000, y_dep, y_fin);
						break;
					default:
					
				}
			}
		
		}

		public function stop():void{
			if( marche ){
				clearInterval(intervalSet);
				removeChild(main);
				marche = false;
			}
		}
		

		public function appartionScale(){
			var tweenH:Tween = new Tween(main, "scaleY", Bounce.easeOut, 3, 0.2, 1, true);
			var tweenW:Tween = new Tween(main, "scaleX", Bounce.easeOut, 3, 0.2, 1, true);
			var tweenA:Tween = new Tween(main, "alpha", Regular.easeOut, 0, 1, 0.5, true);
		}
	}
}