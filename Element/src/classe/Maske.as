package  classe{
	
    import flash.display.*;
    
	import classe.Fond_grid;
		
	public class Maske extends Fond_grid {
		
		private var rect:Sprite;
		private var fond_gride:Fond_grid;
		
		public function make_maske(gameSprite:Sprite, tab:Array):void{
		
		    rect = new Sprite();
			
			fond_gride = new Fond_grid(); 
			fond_gride.make_fond_grid(rect, 0x000000, 0.4);
			if( tab == null )
				return;

            for(var i:int=0; i<14; i++){
                for( var e:int=0; e<6; e++){
					if( (tab.length > 12*6 && tab[6*i+e] == 0) || tab.length < 12*6 ){
						var rect_arrondi:MovieClip = new MovieClip(); 
					 
						rect_arrondi.graphics.beginFill(0x00); 
						rect_arrondi.graphics.drawRect((e*50)-25, (i*50)-25, 50, 50);
						
						fond_sprite.addChild(rect_arrondi);
						fond_sprite.setChildIndex(rect_arrondi, 0);
						gameSprite.addChild(fond_sprite);
						gameSprite.setChildIndex(fond_sprite, 0);
						tab_rect.push(rect_arrondi);
					}
					else if( (tab.length > 12*6 && tab[6*i+e] != 0) ){
						tab_rect.push(null);
					}
                }
            }
			
			rect.mask = fond_sprite;

			addChild(rect);
            
        }
        
		public function del_maske():void{
			if( contains(rect) ){
				removeChild(rect);
			}
				rect = null;
				fond_gride = null;

		}
        
	}
}