﻿package classe
{ 
    import flash.display.*;
	import flash.utils.*;
	import flash.events.*;

    import classe.Element;
    import classe.Effect;
    import classe.Sounds; 

    public class Mur extends Sprite{
        
        private static var vitesseAnimeDevoile:int = 200;
        private static var indiceSonPlop:int = 1;
        private static var pause:Boolean = false;
		private static var typeSonPlop:int = 1;
		private var nombreTypeElement:int;
        private var nombreEtages:int;
        private var nombreEtagesAjouter:int = 0;
        private var nombreColonnes:int = 6;
        private var type_image_effect:int = 3;
        private var spriteEffet:Sprite = new Sprite;
        private var sound:Sounds = new Sounds;
        private var temp_attente_avt_supr:int = 300;
        private var vitesse_clignotement:int = 100;
        private var entierementDevoiler:Boolean = false;
        private var ceDevoile:Boolean = false;
        private var effet:Effect = new Effect;

        /* Variable de placement Position Pion */
        public const spacing:Number = 50;
        public var offsetX:Number = 0;
        public var offsetY:Number = 0;
		
        public var elements:Array = [[],[],[],[],[],[],[]];

        /* Constructeur */
        public function Mur(nombreEtages:int, nombreTypeElement:int=5){
            this.nombreEtages = nombreEtages;
            this.nombreTypeElement = nombreTypeElement;
            var element:Element;

            for(var row=0; row<nombreEtages; row++){
                for(var col=0; col<6; col++){
                    elements[col][row] = new Element(col, row, Math.ceil( Math.random() * nombreTypeElement ) );
                    element = elements[col][row];
                    element.mur = true;
                    element.can_deplace = false;
                    element.can_elimine = false;
                    element.x = col * spacing+offsetX;
                    element.y = -(row*spacing+offsetY);
                    imageMur(element, col, row);
                    //element.gotoAndStop("grid1");
                    element.initeMurBlock(this);
                }
            }
        }

        /*=======
            Getter
        ========*/

        public function getNombreEtages():int{
            return nombreEtages;
        }

        public function getNombreEtagesAjouter():int{
            return nombreEtagesAjouter;
        }
        
        public function getEntierementDevoiler():Boolean{
            return entierementDevoiler;
        }

        public function getCeDevoile():Boolean{
            return ceDevoile;
        }

        public function getPause():Boolean{
            return pause;
        }


        /*=======
            Setter
        ========*/

        public function setNombreEtagesAjouter(nombreEtagesAjouter:int):void{
            this.nombreEtagesAjouter = nombreEtagesAjouter;
        }

        public function setTypeSonPlop(num:int):void{
            typeSonPlop = num;
        }

        public function remiseZeroIndiceSonPlop():void{
            indiceSonPlop = 1;
        }

        public function setPause(bol:Boolean):void{
            pause = bol;
        }

        /*=======
            Methode
        ========*/

        /* reduid le mur */
        public function reduire():void{
            if( !ceDevoile ){
                ceDevoile = true;
                var element:Element;

                for(var row:int=0; row<nombreEtagesAjouter; row++) {
                    for(var col:int=0; col<nombreColonnes; col++){
                        element = elements[col][row];
                        element.ateri = false;
                        element.can_tombe = false;
                        element.aligne = true;
                    }
                }

                animationClignote();
            }
        }

        /* Fais clignoter le mur */
        private function animationClignote():void{
            
            /* timer pour repetition*/
            var time_clignote:Timer = new Timer(vitesse_clignotement, 7);
            time_clignote.addEventListener(TimerEvent.TIMER, time);
            time_clignote.start();
            
            pause_timer_local(time_clignote);
            
            function time(){
                pause_timer_local(time_clignote);
                changeStyleImage();
            }
        }

        /* Change l'image du pion */
        private function changeStyleImage():void{
            var element:Element;

            for(var row:int=0; row<nombreEtagesAjouter; row++) {
                for(var col:int=0; col<nombreColonnes; col++){
                    element = elements[col][row];
                    
                    if( element != null ){
                        if( element.color_change ){
                            element.color_change = false;
                            imageMur(element, col, row);
                            //element.gotoAndStop( "grid1" );
                        }
                        else{
                            element.color_change = true;
                            element.gotoAndStop( "partieMur" );
                        }
                    }
                }
            }
        }

        /* Affiche la bonne image du mur selon la position col row */
        private function imageMur(element:Element, col:int, row:int):void{
            
            if( col < 3 ){
                if( row == nombreEtages ){
                    element.gotoAndStop("grid6");
                }
                else if( row == 0 ){
                    element.gotoAndStop("grid1");
                }
                else if( row < nombreEtages/2 && col == 2 ){
                    element.gotoAndStop("grid3");
                }
                else{
                    element.gotoAndStop("grid2");
                }

            }
            else{
                if( row == nombreEtages ){
                    element.gotoAndStop("grid6");
                }
                else if( row == 0 ){
                    element.gotoAndStop("grid1");
                }
                else if( row < nombreEtages/2 && col == 3 ){
                    element.gotoAndStop("grid4");
                }
                else{
                    element.gotoAndStop("grid2");
                }

            }
        }


        /* */
        public function devoilerElements():void{
            var col:int = 0;
            var row:int = 0;
            
            var element:Element;

            /*timer pour repetition*/
            var time_supression:Timer = new Timer(vitesseAnimeDevoile, (nombreEtagesAjouter * nombreColonnes) + 1 );
            time_supression.addEventListener(TimerEvent.TIMER, time);
            time_supression.start();

            pause_timer_local(time_supression);

            function time(){
                pause_timer_local(time_supression);
                
                while( row < nombreEtagesAjouter ){

                    while( col < 6 ){
                        element = elements[col][row];
                        
                        /**/
                        if( row == 0 )
                            element.gotoAndStop(element.type);
                        else
                            imageMur(element, col, row);
                            //element.gotoAndStop("grid1");

                        element.color_change = false;

                        //setChildIndex(effet, numChildren-1);
                        //setChildIndex(spriteEffet, numChildren-1);
                        effet.effet_frame(spriteEffet, element.x, element.y, element, type_image_effect);
                        sound.joue_song_plop(indiceSonPlop, typeSonPlop);
                        col++;
                        indiceSonPlop++;
                        return;
                    }
                    col = 0;
                    row++;
                }
                entierementDevoiler = true;                
            }
        }

        /**/
        public function afterAnimeDevoilement():void{
            var element:Element;

            for(var col=0; col<nombreColonnes; col++){
                element = elements[col][0];
                element.mur = false;
                element.can_deplace = true;
                element.can_elimine = true;
                element.can_tombe = true;
                element.ateri = false;
                element.aligne = false;
                element.removeMurBlock();
            }

            for(var row:int=0; row<nombreEtages-1; row++){
                for( col=0; col<nombreColonnes; col++){
                    elements[col][row] = elements[col][row+1];
                    elements[col][row].can_tombe = true;
                    elements[col][row].aligne = false;
                }
            }

            ceDevoile = false;
            entierementDevoiler = false;
            if( nombreEtages > 0 ){
                nombreEtages--;
                //trace("--1")
                nombreEtagesAjouter--;
            }
            indiceSonPlop = 1;
        }

        /* Verifi si le mur est descendu au complet */
        public function isComplet():Boolean{
            return nombreEtages == nombreEtagesAjouter;
        }

        /* Verifi si le mur à était completement detruit */
        public function isVide():Boolean{
            return nombreEtages == 0;
        }


        /**
         * Stop les timer en cas de pause et reprend aprés la pause
         * @param timer le timer à gérer
         * Stop le timer si pause est vrai et que le timer est en cours
           Sinon relance le timer si pause est faux et que le timer n'est pas en cours
         */
        private function pause_timer_local(timer:Timer):void{
            verif_pause();
            
            function verif_pause(){
                if( pause && timer.running){
                    timer.stop();
                    addEventListener(Event.ENTER_FRAME, verif_pause);
                }
                else if( !pause && !timer.running ){
                    timer.start();
                    removeEventListener(Event.ENTER_FRAME, verif_pause);
                }
            }
        }

        /**/
        public function addSpriteEffet(sprite:Sprite):void{
            if( !sprite.contains(effet) )
                sprite.addChild(effet);
            spriteEffet = sprite;
        }

        override public function toString():String{
            return "[ nombreEtages " + nombreEtages + " ]";
        }
    }
}