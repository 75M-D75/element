﻿package  classe{
import flash.events.Event;
import fl.motion.Color;
import flash.display.MovieClip;
import flash.display.Shape;
import flash.display.DisplayObject;
import flash.display.Sprite;
	
	public class Neige extends Sprite {
		private var _X:Number = 0; 
		private var _Y:Number = 0;
		
		private static var sens = 1;
		private var acceleration = 0;	
		private var vitX;
		
		public function Neige(X,Y:Number) {
			
			x = X + Math.random() * 10;
			
			if( x > X+3 && x < X+6){
				y = (Y+Math.random()*5)+3;
			}
			else{
				y = Y;
			}

			scaleX = scaleY = Math.random()*0.3 + 0.2;
			alpha  = 0.8;
			_X     = x + Math.random() * 0.4 - 0.3;

			vitX = Math.random()*0.6;
			acceleration = Math.random() * 0.05 + 0.01;
			addEventListener(Event.ENTER_FRAME, update);
		}


		public static function changeSense():void{
			sens *= -1;
		}

		public static function choisirSense(s:int):void{
			sens = s;
		}
		
		private function update(e:Event):void{
			//in_sin += valeur_in_sin;
			_X += (0.2 + vitX) * (sens);
			x  =  _X;
			y  += _Y;

			_Y += acceleration;
		}
 
		public function kill():void{
			removeEventListener(Event.ENTER_FRAME, update);
		}
	}
	
}
