﻿package  classe{
	
	import flash.utils.*;
	import flash.events.*;
	import flash.display.Sprite;

	/*Importation des classe*/
	import classe.VerifInit;
	import classe.Plateau;

	public class Nivo extends Sprite {

		/*Declaration des class*/
		private var verifInite:VerifInit = new VerifInit();
		private var interval:*;

		public var jauge:Jauge;
		public var plateau:* = new Plateau();


		private var tabType:Array = [1,2,3,4,5];

		/*  */
		private var glaceOk:Boolean = true;
		private var replacementOk:Boolean = true;
		private var scoreJaugeOk:Boolean = true;
		private var deplacementOk:Boolean = true;
		private var corespondanceOk:Boolean = true;
		private var murOk:Boolean = false;
		private var cercleOk:Boolean = true;

		public var numero:int;

		public function Nivo(num:int = 1){
			numero = num;
			if( num < 0 ){
				plateau = new PlateauMultiJoueur;
				trace("Multi Multi");
			}
		}

		public function lauch_nivo(num:int):void{
			contexteNivo(num);

			if(tabType != null){
				plateau.tabType = tabType;
				plateau.initGrid.saisi.initTabType(tabType);
				//trace("yo tabType");
			}

			plateau.nivau = num;
			plateau.objectifs.objectifs(num);
			plateau.inite();
			plateau.menuBeginApparition();
			nivo();
		}


		public function lauchNivoTuto(num:int):void{
			contexteNivoTuto(num);

			if(tabType != null){
				plateau.tabType = tabType;
				plateau.initGrid.saisi.initTabType(tabType);
			}

			plateau.nivau = num;
			//plateau.objectifs.objectifs(num);
			plateau.initeTuto();
			nivoTuto();
		}


		public function start_gestion_nivo():void{
			gestion_nivo();
		}

		/*  */
		private function gestion_nivo():void{
			interval = setInterval(plateau.lose_touche_plafond, 1000);
			this.addEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler_nivo);
		}

		public function stop_gestion_nivo():void{
			clearInterval(interval);
			this.removeEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler_nivo);
		}

		/* Fais tourner le jeu */
		private function fl_EnterFrameHandler_nivo(event:Event):void{
			frame();
		}

		public function frame():void{

			/*Replacement des Pions*/
			if( replacementOk ){
				for(var col=0; col<6; col++) {
					for(var row=0; row<13; row++) {
						plateau.deplacement.replacer(plateau.grid, col, row);
						
						//Glace
						if( glaceOk ){
							if( plateau.fct_Glace.condition_del_glace(plateau.grid, col, row) ){
								if(plateau.grid[col][row].glace_delete != true){
									plateau.reload_objectif( plateau.grid[col][row].glace );
								}
								plateau.fct_Glace.delGlace(plateau.grid[col][row]);
							}
						}
					}
				}
			}

			//Objectif score
			if( scoreJaugeOk ){
				if( plateau.objectifs.getScore() != null && plateau.get_gameScore()/plateau.objectifs.getScore() < 1)
					jauge.loading(plateau.get_gameScore()/plateau.objectifs.getScore());
				else
					jauge.loading(1);
			}

			//Gestion deplacemnt element selectionner
			if( deplacementOk ){
				plateau.deplacement.deplace_pion(plateau.grid, plateau.pion_selectionner );
				if( plateau.pion_selectionner != null && plateau.pion_selectionner.selectionner )
					plateau.deplacement.lignedeplace_2( plateau.grid, plateau.pion_selectionner.col, plateau.pion_selectionner.row);
			}

			
			if( murOk ){
				plateau.gestionDevoilementMur();
			}

			//Correspondance
			if( corespondanceOk ){
				plateau.fndAndRemoveMatches(plateau.gameSprite, plateau.grid);
				var v:Boolean = verifInite.nul_avc_temp(plateau.gameSprite, plateau.grid);/*Descente de pion en cas de place*/
				if(v && murOk){
					setTimeout(plateau.ajouterResteDuMur, 201);
				}
			}

			//Murs
			if( murOk ){
				setTimeout(plateau.ajouterResteDuMur, 201);
				verifInite.nulMur(plateau.grid);
			}
			
			//Cercle
			if( cercleOk ){
				/*plateau.cercle de selection*/
				if( plateau.cercle.isPetit() == false && plateau.pion_selectionner != null ){
					plateau.cercle.position_y = plateau.pion_selectionner.y;
					plateau.cercle.update();
				}
			}
		}

		public function nivo():void{
			
			//plateau.animation_inite_plateau_1(plateau.grid);
			plateau.timer_monter_start();
			plateau.lauch_time();

			addChild(plateau);

			gestion_nivo();
		}

		public var monterOk:Boolean = false;

		public function nivoTuto():void{
			
			//plateau.animation_inite_plateau_1(plateau.grid);
			if(monterOk){
				plateau.timer_monter_start();
				plateau.begin = true;
			}

			addChild(plateau);

			//gestion_nivo();
		}

		private function contexteNivo(num:int):void{
			
			/*glaceOk = true;
			replacementOk = true;
			scoreJaugeOk = true;
			deplacementOk = true;
			corespondanceOk = true;
			murOk = false;
			cercleOk = true;*/
			murOk = true;
			switch (num){
				case 1 :{
					glaceOk = false;
					break;
				}
				case 2 :{
					glaceOk = false;
					break;
				}
				case 3 :{
					glaceOk = false;
					break;
				}
				case 4 :{
					glaceOk = false;
					break;
				}
				case 5 :{
					tabType = [1,6,3,4,5];
					break;
				}
				case 6 :{
					glaceOk = false;
					break;
				}
				case 7 :{
					tabType = [1,6,3,4,5];
					break;
				}
				case 8 :{
					tabType = [1,6,3,4,5];
					break;
				}
				case 9 :{
					glaceOk = false;
					break;
				}
				case 10 :{
					tabType = [1,6,3,4,5];
					monterOk = true;
					break;
				}
				default:{
					trace("default");
				}

			}

		}

		private function contexteNivoTuto(num:int):void{
			
			/*glaceOk = true;
			replacementOk = true;
			scoreJaugeOk = true;
			deplacementOk = true;
			corespondanceOk = true;
			murOk = false;
			cercleOk = true;*/
			scoreJaugeOk = false;

			switch (num){
				case 1 :{
					glaceOk = false;
					break;
				}
				case 2 :{
					glaceOk = false;
					break;
				}
				case 3 :{
					glaceOk = false;
					break;
				}
				case 4 :{
					glaceOk = false;
					break;
				}
				case 5 :{
					glaceOk = false;
					break;
				}
				case 6 :{
					glaceOk = false;
					break;
				}
				case 7 :{
					glaceOk = false;
					break;
				}
				case 8 :{
					glaceOk = false;
					break;
				}
				case 9 :{
					glaceOk = false;
					break;
				}
				case 10 :{
					glaceOk = false;
					break;
				}
				case 11 :{
					tabType = [6,6,6,6,6];
					glaceOk  = false;
					monterOk = true;
					break;
				}
				case 16 :{
					break;
				}
				default:{
					trace("default");
				}

			}
		}
	}
}