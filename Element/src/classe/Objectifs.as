package  classe{
	
    import flash.display.Sprite;
    import flash.utils.Dictionary;
    
	public class Objectifs extends Sprite {
        
        private var score = null;
        private var dico_objet = null;
        private var dico_handicap = null;
		
        /**
         * retourne le score;
         * @param none;
         */
        public function getScore(){
            return score;
        }
        
        public function getDico(){
            return dico_objet;
        }
		
		public function getDicoHandicap(){
			return dico_handicap;
		}
    
        public function objectifs(num_niv:int){
		
			objectifs_nul();
			
            switch(num_niv){
                case 0:
                    this.score = 0;
                    break;
                case 1:
                    this.score = 1000;
                    //this.dico_objet = {"1b":26, "2b":15, "3b":9,"4b":6, "5b":6};
                    break;
					
                case 2:
                    this.score = 2000;
                    this.dico_objet = {"1b":10, "2b":10, "4b":5};
                    break;
					
                case 3:
                    this.score = 5000;
                    this.dico_objet = {"1b":15, "2b":10, "3b":15};
                    break;
                    
                case 4:
                    this.score = 5000;
                    this.dico_objet = {"1b":26, "2b":15, "3b":9,"4b":6, "5b":6};
                    break;
					
                case 5:
					this.score = 3000;
                    this.dico_objet = {"1b":3*2, "6b":3*4, "5b":3*4};
                    this.dico_handicap = {"glace":"ok"};
                    break;
					
                case 6:
                    this.score = 1000;
                    this.dico_objet = {"3b":12*2, "5b":12*3};
                    break;
					
                case 7:
                    this.score = 5000;
                    this.dico_objet = {"4b":5*3, "glace":4};
					this.dico_handicap = {"glace":"ok"};
                    break;
                case 8:
                    this.score = 1000;
                    this.dico_objet = {"4b":5*3, "glace":7};
                    this.dico_handicap = {"glace":"ok"};
                    break;
                case 9:
                    this.score = 3000;
                    this.dico_objet = {"3b":13*2};
                    break;
                case 10:
                    this.score = 1000;
                    this.dico_objet = {"1b":3*2, "5b":7*3, "glace":9};
                    this.dico_handicap = {"glace":"ok"};
                    break;
				default :
					this.score = 6000;
					this.dico_objet = {"2b":5*3, "glace":3};
					this.dico_handicap = {"glace":"ok"};
                    break;
            }
        }
        
        private function objectifs_nul(){
            score = null;
            dico_objet = null;
        }

	}
}