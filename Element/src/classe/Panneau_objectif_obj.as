package  classe{
    import fl.motion.Color;
    import flash.display.Sprite;
	import flash.utils.Dictionary;
	
    import flash.display.Sprite;
    import flash.geom.*;
    import flash.display.*;
	
	import classe.Glace;
	
	public class Panneau_objectif_obj extends Sprite {
		
		private var objectif_Sprite:Sprite = new Sprite();

		public var tab_piece_obj:Array = new Array();

		public function getTabLength():int{
			return tab_piece_obj.length;
		}

		public function Panneau_objectif_obj( dico_objectif ):void{
			
			var u:int = 0;

			for(var i in dico_objectif){
				var myTextBox:obj_txt = new obj_txt();
				myTextBox.txt.text = "text";

				if( i != "glace" ){

					var piece_obj:ElementObjectif = new ElementObjectif();
					piece_obj.nombre = dico_objectif[i];
					piece_obj.num_tab = u;
					piece_obj.scaleX = piece_obj.scaleY = 0.6;
					piece_obj.y = 25;
					piece_obj.x = (piece_obj.width * u ) * 1.5;
					piece_obj.type = Number(i.charAt(0));
					piece_obj.gotoAndStop(piece_obj.type);

					//
					myTextBox.width = piece_obj.width;
					myTextBox.height = piece_obj.height;
					myTextBox.x = piece_obj.x + 15;
					myTextBox.y = piece_obj.y;
					myTextBox.border = true;
					myTextBox.mouseEnabled = false;
					
					piece_obj.ar = myTextBox;
					piece_obj.ar.txt.text = dico_objectif[ String(piece_obj.type) + "b"];

					objectif_Sprite.addChild(myTextBox);
					objectif_Sprite.addChild(piece_obj);
					
					tab_piece_obj.push(piece_obj);
					tab_piece_obj[u].valide = false;
				}
				else if( i == "glace"){

					var element_obj:ElementObjectif = new ElementObjectif();
					var glace = new Glace( 50, 0, 0 );
					element_obj.nombre = dico_objectif[i];
					element_obj.scaleX = element_obj.scaleY = 0.6;
					element_obj.y = 25;
					element_obj.x = (element_obj.width*u)*1.5;
					element_obj.type = 14;
					element_obj.gotoAndStop(14);
					element_obj.addChild(glace);

					//
					myTextBox.width = element_obj.width;
					myTextBox.height = element_obj.height;
					myTextBox.x = element_obj.x + 15;
					myTextBox.y = element_obj.y;
					myTextBox.border = true;
					myTextBox.mouseEnabled = false;
					
					element_obj.ar = myTextBox;
					element_obj.ar.txt.text = dico_objectif["glace"];
					
					objectif_Sprite.addChild(myTextBox);
					objectif_Sprite.addChild(element_obj);
					
					tab_piece_obj.push(element_obj);
				}
				
				u++;
			}

			this.addChild(objectif_Sprite);
			objectif_Sprite.y = (this.height / 2) - (objectif_Sprite.height / 2);
			
		}

        public function valide(element:Object):void{
			var coche:coche_valide = new coche_valide();
			coche.width  = coche.height = element.width;
			coche.x = element.x;
			coche.y = element.y;
			element.valide = true;
			objectif_Sprite.addChild(coche);
		}
	
	}
}