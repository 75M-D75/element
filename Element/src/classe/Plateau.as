﻿package classe{
	
	import flash.utils.*;
	import flash.utils.Timer;
	import flash.events.*;
	import fl.motion.Color;
	import flash.display.*;
	import flash.text.*;
	import flash.utils.Timer;
	import flash.display.MovieClip;
	import fl.transitions.easing.*;
	import fl.transitions.*;
	import flash.utils.Timer;
	import flash.utils.Dictionary;
	import flash.net.URLRequest;
	import flash.geom.Matrix; 
	import flash.geom.*;

	/* Importation des classes */
	import classe.Deplacement;
	import classe.Validiter;
	import classe.VerifInit;
	import classe.InitGrid;
	import classe.Effect;
	import classe.Sounds;
	import classe.Cercle;
	import classe.Panneau_objectif_obj;
	import classe.Objectifs;
	import classe.Text_info;
	import classe.Fct_Glace;
	import classe.Point_Burster;
	import classe.Mur;
	import classe.TextSuccess;

	import classe.swingpants.effect.Flamer;
	import classe.extension.Vibrations;

	public class Plateau extends Sprite {
		
		/*Declaration des class*/
		public var validiter:Validiter = new Validiter();
		public var verif_inite:VerifInit = new VerifInit();
		public var initGrid:InitGrid = new InitGrid();
		public var effect:Effect = new Effect();
		public var sound:Sounds = new Sounds();

		public var cercle:Cercle = new Cercle(40);
		public var fct_Glace:Fct_Glace = new Fct_Glace;
		public var deplacement:Deplacement = new Deplacement();
		public var PointBuster:Point_Burster = new Point_Burster();
		public var objectifs:Objectifs = new Objectifs();
		public var panneau_objectif_obj:Panneau_objectif_obj;

		/* Proprieter (Variable) */
		public var nivau:int = 1;
		public var vitesse_up:int = 10;
		public var vitesse_de_monter:int = 2000;
		public var save:*;
		public var grid:Array = new Array();
		public var pion_selectionner:MovieClip = null;
		public var gameSprite:Sprite = new Sprite();
		public var vibrationActiver:Boolean = true;
		
		public var type_image_effect:int = 3;
		public var numPieces:int = initGrid.nbPions;
		public var nb_chain:int = 2;
		public var gameScore:int = 0;
		public var var_pause:Boolean = false;
		public var menu_paus:menu_pause = new menu_pause();
		public var dico_objectif:*;
		public var num_song_monde:int;
		public var tabGlace:Array = new Array;
		public var tabBoue:Array = new Array;
		public var menuBegin:MenuBegin = new MenuBegin();
		public var textSucces:TextSuccess = new TextSuccess("GRRRRR");

		public var tabType:Array = [1, 2, 3, 4, 5];

		public var stageWidth:Number;
		public var stageHeight:Number;

		public var begin:Boolean = false;
		
		public var Murs:Array = new Array();
		var main:MainTuto = new MainTuto();

		/* Vitesse de l'animation de disparition des pion un par un */
		public var vitesse_anime_supresion:int = 200;

		public function Plateau(stageWidth:Number = 400, stageHeight:Number = 700, nivau:int = 1 ):void{
			this.stageWidth = stageWidth;
			this.stageHeight = stageHeight;
			this.nivau = nivau;
			
		}

		public function get_num_song_monde():int{
			return num_song_monde;
		}

		public function variable_null():void{
			if( gameSprite != null && this.contains(gameSprite))
				removeChild(gameSprite);

			if( menu_paus != null && this.contains(menu_paus))
				removeChild(menu_paus);

			if(contains(panneau_objectif_obj))
				removeChild(panneau_objectif_obj);

			validiter = null;
			verif_inite = null;
			initGrid = null;
			effect = null;
			sound = null;
			cercle = null;
			fct_Glace = null;
			deplacement = null;
			gameSprite = null;
			grid = null;
			pion_selectionner = null;
			PointBuster = null;
			menu_paus = null;
			objectifs = null;
			save = null;
			dico_objectif = null;
			panneau_objectif_obj = null;
			textSucces = null;
			tabGlace = null;
		}


		//?
		/* Pour initialiser le Plateau */
		public function inite():void{

			/* Inatialisation de la grille grid */
			initGrid.setUpGrid_aleatoir();
			gameSprite 	= initGrid.getGameSprite();
			grid 		= initGrid.getGrid();
			addChild(gameSprite);

			gameSprite.x += 75;
			gameSprite.y += 75;
			gameSprite.addChild(cercle);


			initGrid.saisi.noircir_dernier_ligne_pion(grid);

			gameSprite.addChild( deplacement );
			gameSprite.addChild(main);

			/* Panneau d'affichage Objectif*/
			dico_objectif 			= objectifs.getDico();
			panneau_objectif_obj 	= new Panneau_objectif_obj( dico_objectif );
			panneau_objectif_obj.x 	= 30;
			panneau_objectif_obj.y 	= 0;

			if(dico_objectif != null)
				addChild(panneau_objectif_obj);
			
			if( objectifs.getDicoHandicap() != null && objectifs.getDicoHandicap()["glace"] != null ){
				tabGlace = new Array;
				var i:int;
				for(var row:int=0; row<12; row++){
					for( i=0; i<6; i++){
						if( grid[i][row] != null && grid[i][row].type == 6 ){
							grid[i][row].initRebour(1);
							tabGlace.push(grid[i][row]);
						}
					}
				}
			}

			/* Placement menu pause */
			menu_paus.width = stageWidth - 80;
			menu_paus.scaleY = menu_paus.scaleX;
			menu_paus.x = (stageWidth/2);
			menu_paus.y = (stageHeight/2);
			menu_paus.z = -500;
			addChild(menu_paus);

			/* Menu de commencement */
			menuBegin.width = stageWidth - 80;
			//trace("stageWidth ",stageWidth, " ",stageHeight);
			menuBegin.scaleY = menuBegin.scaleX;
			menuBegin.x = (stageWidth/2);
			menuBegin.y = (stageHeight/2);
			menuBegin.name = "menuBegin";
			addChild(menuBegin);

			square.graphics.beginFill(0x000000); 
			square.graphics.drawRect(-100, 0, stageWidth+200, stageHeight);
			square.alpha = 0.7;
			//trace("palteau s =", parent);

			//TextSucces
			addChild(textSucces);
			textSucces.x = (400/2);
			textSucces.y = (700/2);
			textSucces.mouseEnabled = false;
		}


		//?
		public function initeTuto():void{

			/* Inatialisation de la grille grid */
			addChild(gameSprite);

			//gameSprite.x += 75;
			//gameSprite.y += 75;
			gameSprite.addChild(cercle);

			initGrid.saisi.noircir_dernier_ligne_pion(grid);

			gameSprite.addChild( deplacement );

			/* Panneau d'affichage Objectif*/
			dico_objectif = objectifs.getDico();
			panneau_objectif_obj = new Panneau_objectif_obj( dico_objectif );
			panneau_objectif_obj.x = 30;
			panneau_objectif_obj.y = 0;

			if(dico_objectif != null)
				addChild(panneau_objectif_obj);
			
			if( objectifs.getDicoHandicap() != null && objectifs.getDicoHandicap()["glace"] != null ){
				tabGlace = new Array;
				var i:int;
				for(var row:int=0; row<12; row++){
					for( i=0; i<6; i++){
						if( grid[i][row] != null && grid[i][row].type == 6 ){
							grid[i][row].initRebour(4);
							tabGlace.push(grid[i][row]);
						}
					}
				}
			}

			square.graphics.beginFill(0x000000); 
			square.graphics.drawRect(-10, 0, stageWidth+20, stageHeight);
			square.alpha = 0.7;
			//trace("palteau s =", parent);

			//TextSucces
			addChild(textSucces);
			textSucces.x = (400/2);
			textSucces.y = (700/2);
			textSucces.mouseEnabled = false;
		}

		//?
		public function menuBeginApparition():void{
			var twenplac:Array = new Array();
			setChildIndex(menuBegin, numChildren-1);
			twenplac.push( new Tween(menuBegin, "y", Regular.easeOut, -menuBegin.height, (stageHeight/2), 0.5, true));
			addChild(square);
			setChildIndex(square, numChildren-2);
		}

		//?
		public function menuBeginDisparition():void{
			if( menuBegin.y == (stageHeight/2) ){
				var twenplac:Array = new Array();
				setChildIndex(menuBegin, numChildren-1);
				twenplac.push( new Tween(menuBegin, "y", Regular.easeIn, (stageHeight/2), -menuBegin.height, 0.5, true));
				
				removeChild(square);

				twenplac[twenplac.length-1].addEventListener(TweenEvent.MOTION_FINISH, onFinish);
				
				function onFinish(evt:TweenEvent):void{
					var dep_sound:s43 = new s43();

					if(sound.son_active)
						dep_sound.play();

					begin = true;
					timer_temps.start();
				}
				animation_inite_plateau_1(grid);
				setTimeout(beginSoundAterissage, 1000);
			}
		}

        /* Placement de pion aleatoire */
        private function place_pion_alea(grid:Array):void{
            var neg_pos:Number;

            for(var col:int=0;col<6;col++){
                for(var row:int=0; row<12; row++){
                    /* Position negatif ou positif (-1 ou 1) */
                    neg_pos = Math.round(Math.random()*1);

                    if(neg_pos != 0){
                        neg_pos=-1;
                    }
                    if(grid[col][row] != null){
                        grid[col][row].x = ((Math.random()*600)*neg_pos)+200;
                        grid[col][row].y = Math.random()*200*neg_pos;
                        grid[col][row].z = Math.random()*-500;
                    }
                }
            }
        }

        /* Annimation de Replacement des Elements a leur place selon le ofssetX et offsetY et spacing et ligne, colonne*/
        private function replace_pion( col, row:Number, spacing, offsetX, offsetY:int):void{
			
			var temp:Number = Math.random()*0.8 + 0.5;
			var twenn_placement:Array = new Array();

			if(grid[col][row] != null){
				twenn_placement.push( new Tween(grid[col][row], "x", Bounce.easeOut, grid[col][row].x, grid[col][row].col*spacing+deplacement.offsetX, temp, true));
				twenn_placement.push( new Tween(grid[col][row], "y", Bounce.easeOut, grid[col][row].y, grid[col][row].row*spacing+deplacement.offsetY, temp, true));
				twenn_placement.push( new Tween(grid[col][row], "z", Bounce.easeOut, grid[col][row].z, 0, temp, true));
			}
			
			setTimeout(replacePion ,temp+10);

			//s'assurer que les pion reprenne bien leur place
			function replacePion(){
				if(grid[col][row] != null){
					grid[col][row].x = grid[col][row].col*spacing+deplacement.offsetX;
					grid[col][row].y = grid[col][row].row*spacing+deplacement.offsetY;
					grid[col][row].z = 0;
				}
			}

			if(col == 5 && row == 11){
				twenn_placement[twenn_placement.length-1].addEventListener(TweenEvent.MOTION_FINISH, onFinish);
				
				function onFinish(e:TweenEvent):void{

					num_song_monde = Math.ceil(Math.random()*5);
					sound.joue_song_monde( num_song_monde );
					e.target.obj.z = 0;	
				}
			}
		}


		/* Lance l'animation des placement d'Elements */
		public function animation_inite_plateau_1(grid:Array):void{
			place_pion_alea(grid);
			for(var col:int=0; col<6; col++) {
				for(var row:int=0; row<13; row++) {
					replace_pion( col, row, deplacement.spacing, deplacement.offsetX, deplacement.offsetY);
				}
			}
			setTimeout(beginSoundAterissage, 1305);
		}

		private function beginSoundAterissage():void{
			for(var col:int=0; col<6; col++) {
				for(var row:int=0; row<13; row++) {
					deplacement.replacer(grid, col, row);
				}
			}

			sound.ok = true;
		}

		/*=======================================
		
						Souris

		=======================================*/
		

		private function suivre(pion_selectionner:MovieClip):void{
			if( pion_selectionner != null && pion_selectionner is Element && pion_selectionner.selectionner == true
			){
				//position de fin
				pion_selectionner.posXfin 	= Math.abs(Math.ceil( pion_selectionner.positionMouseX - gameSprite.mouseX) );
				//??
				pion_selectionner.posx 		= Math.abs(Math.ceil( pion_selectionner.positionMouseX - gameSprite.mouseX) );
				if(gameSprite.mouseX <= 0){
					pion_selectionner.posXfin = 0;
					pion_selectionner.posx    = 0;
				}
				pion_selectionner.movee 	= true;
				pion_selectionner.addEventListener(Event.ENTER_FRAME, mvt);
			}
		}

		public function moveThatMouse(evt:MouseEvent):void{
			suivre(pion_selectionner);
		}

		/* Deplacement fluid du pion */
		public function mvt(evt:Event):void {
			
			if(evt.target.posXfin > 255){
				evt.target.posXfin = 250;
			}
			//else if(evt.target.posXfin < 0){
			//	evt.target.posXfin = 0;
			//}

			evt.target.x -= (evt.target.x - evt.target.posXfin) * evt.target.vitesseDeplacement;
			
			/* Cercle de selection */
			if( cercle.isPetit() == false ){
				cercle.position_x = evt.target.x;
				cercle.position_y = evt.target.y;
				cercle.update();
			}
			
			deplacement.lignedeplace_2( grid, evt.target.col, evt.target.row);

			if(Math.abs(evt.target.x - evt.target.posXfin) < 2 || evt.target.movee == false
			){
				evt.target.x = evt.target.posXfin;
				evt.target.posXfin = evt.target.enr;
				evt.target.movee = false;
				evt.target.removeEventListener(Event.ENTER_FRAME, mvt);
			}

			
			if( grid[evt.target.col][evt.target.row+1] == null 
			|| deplacement.verf(grid, evt.target) == true 
			){
				/* Si la case d'en dessous est libre ou si le pion selectionner est aligner */
				evt.target.posXfin = evt.target.enr;
				evt.target.x = evt.target.posXfin;
				evt.target.movee = false;
				evt.target.selectionner = false;
				evt.target.alpha = 1;
				evt.target.removeEventListener(Event.ENTER_FRAME, mvt);
			
				cercle.position_x = pion_selectionner.x;
				cercle.position_y = pion_selectionner.y;
				cercle.retrecir_cercle();
			}
			else if((evt.target.x < evt.target.enr 
			&& (evt.target.col-1 >= 0 && grid[evt.target.col-1][evt.target.row] != null && deplacement.verf(grid, grid[evt.target.col-1][evt.target.row]) == true))
			|| (evt.target.x > evt.target.enr 
			&& (evt.target.col+1  < 6 && grid[evt.target.col+1][evt.target.row] != null && deplacement.verf(grid, grid[evt.target.col+1][evt.target.row]) == true))
			){
				/* Quand il y'a des pion aligner acoter du pion selectionner */
				evt.target.posXfin = evt.target.enr;
				evt.target.x = evt.target.posXfin;
				evt.target.movee = false;
				evt.target.removeEventListener(Event.ENTER_FRAME, mvt);
			}
		}

		/* Deselectionne le pion selectionner */
		private function apré_relache_souris():void{

			/* Cercle */
			if( !cercle.isPetit() && pion_selectionner != null){
				cercle.position_x = pion_selectionner.x;
				cercle.position_y = pion_selectionner.y;
				cercle.retrecir_cercle();
			}
			else if ( !cercle.isPetit() ){
				cercle.retrecir_cercle();
			}

			if(pion_selectionner != null && pion_selectionner.selectionner == true  ){
				pion_selectionner.selectionner = false;
				pion_selectionner.alpha = 1;
			}
		}


		/* Relachement du pion deplacement */
		public function fl_ReleaseToDrop(event:MouseEvent):void
		{
			apré_relache_souris();
		}

		/* Selection du pion à deplacement */
		public function Click_pion(pion:MouseEvent):void{
			
			if( pion.target != null && pion.target is Element && pion.target.aligne != true && pion.target.can_deplace && pion.target.can_select ){
				pion.target.selectionner = true;
				pion.target.enr = pion.target.col * deplacement.spacing + deplacement.offsetX;
				pion.target.positionMouseX = pion.target.mouseX*(pion.target.width/pion.target.tailleDorigine);
				//trace(" y = ",pion.target.positionMouseX);
				pion.target.alpha = 0.7;
				
				pion_selectionner = Element(pion.target);
				
				cercle.change_color(pion_selectionner.type);
				cercle.position_x = pion_selectionner.x;
				cercle.position_y = pion_selectionner.y;
				cercle.grossir_cercle();
				gameSprite.setChildIndex(cercle, gameSprite.numChildren-1);
				
				/* Place au 1er plan */
				if( gameSprite.contains( grid[pion_selectionner.col][pion_selectionner.row] ) ){
					gameSprite.setChildIndex(grid[pion_selectionner.col][pion_selectionner.row], gameSprite.numChildren-1);
				}
			}
		}



		/*=======================================

						Corespondance

		=======================================*/


		//A modifier ou revoir
		public function element_ateri(element:Object, deja_lu_son:Boolean):void{
			
			if( element != null && element.chute && !element.aligne && !deja_lu_son && element.y == element.row*deplacement.spacing+deplacement.offsetY ){
				sound.atterissage();
				element.chute = false;
				deja_lu_son = true;
			}

			if( element != null && ( ( !element.mur && !element.ateri && element.y == element.row*deplacement.spacing+deplacement.offsetY ) /*|| ( element.mur && !element.ateri )*/ ) ){
				element.ateri = true;
			}
		}

		public function pions_tousse_ateri( grid:Array ):void{
			var deja_lu_son:Boolean = false;

			for(var col:int=0; col<6; col++) {
				for(var row:int=0; row<13; row++) {
					element_ateri(grid[col][row], deja_lu_son);
				}
			}
		}

		function verif_pion_non_ater_exist(tab:Array):Boolean{
			for(var i:int=0; i<tab.length; i++) {
				for(var j:int=0; j<tab[i].length; j++) {
					if( tab[i][j] != null && tab[i][j].ateri == false ){
						return true;
					}
				}
			}
			return false;
		}

		/*Pour le grid (Tableau au complet)*/
		private function verif_pion_non_ater_exist_g(grid:Array):Boolean{
			for(var i:int=0; i<6; i++) {
				for(var j:int=0; j<12; j++) {
					if( grid[i][j] != null && grid[i][j].ateri == false ){
						return true;
					}
				}
			}
			return false;
		}

		protected function pion_aligne_exist(grid:Array):Boolean{
			for(var col:int=0; col<6; col++) {
				for(var row:int=0; row<12; row++) {
					if( grid[col][row] != null && grid[col][row].aligne == true ){
						return true;
					}
				}
			}
			return false;
		}

		

		/*Pour manipuler les correspondances trouvées*/
		public function fndAndRemoveMatches(gameSprite:Sprite, grid:Array):void {

			if( pion_selectionner != null && pion_selectionner.selectionner && deplacement.verf(grid, pion_selectionner) == true ){
				deplacement.replacer(grid, pion_selectionner.col, pion_selectionner.row);
				pion_selectionner.posXfin = pion_selectionner.enr;
				pion_selectionner.x = pion_selectionner.posXfin;
				pion_selectionner.movee = false;
				pion_selectionner.selectionner = false;
				pion_selectionner.alpha = 1;
				pion_selectionner.removeEventListener(Event.ENTER_FRAME, mvt);
			
				if( !cercle.isPetit() ){
					cercle.position_x = pion_selectionner.x;
					cercle.position_y = pion_selectionner.y;
					cercle.retrecir_cercle();
				}
			}

			/* Obtenir la liste des correspondances */
			var matches:Array = validiter.lookForMatches( grid );
			var tab:Array = new Array();
			
			var verif_ater:Boolean = verif_inite.verif_avec_y( grid, deplacement.offsetY );
			
			var comb_effet:int;
			var nb_boule_comb_effet:int;
			
			/* Pour l'effet tourbillon */
			if(nb_chain > 1 && nb_chain < 9){
				nb_boule_comb_effet = nb_chain;
			}
			else{
				nb_boule_comb_effet = 8;
			}
				
			/* Pour Chain */
			if( matches.length != 0 && verif_pion_non_ater_exist(matches) ){
				nb_chain++;
				PointBuster.PointBurst(gameSprite, nb_chain-2, matches[0][0].x, matches[0][0].y+50+25, 2);
				sound.joue_song( nb_chain-1 );
				
				effect.effet_combo(gameSprite, matches[0][0].x, matches[0][0].y, 30, nb_boule_comb_effet, 2, matches[0][0].type);
				effect.set_rayon((nb_chain-2)*30);
				
				effect.cibleEffect(gameSprite, matches[0][0].x, matches[0][0].y);
				
				/* Vibration */
				Vibrations.vibrationActiver = vibrationActiver;
				var time_vibration:int;

				if( nb_chain - 2 <= 7) {
					time_vibration = 200 + 100 * (nb_chain-2);
				}
				else {
					time_vibration = 900;
				}
				
				if( nb_chain <= 4 )
					Vibrations.vibration(time_vibration);
				else
					Vibrations.vibration(time_vibration);
			}
			else if( matches.length == 0 && verif_ater && !pion_aligne_exist(grid) ){
				
				if( sound.son_active ){
					if(nb_chain >= 4 && nb_chain < 6){
						sound.tada1.play();
						textSucces.setText("GREAT");
						textSucces.apparition();
					}
					else if(nb_chain >= 6 && nb_chain < 7){
						sound.tada2.play();
						textSucces.setText("NICE");
						textSucces.apparition();
					}
					else if(nb_chain >= 7){
						sound.tada3.play();
						textSucces.setText("AMAZING");
						textSucces.apparition();
					}
				}
				
				effect.set_rayon(20);/*rayon des effets*/
				nb_chain = 2;
			}

			if( verif_ater ){
				pions_tousse_ateri( grid );
			}	

			for(var i:int=0; i<matches.length; i++) {
				
				/* Combo */
				if( i == 0 && matches[i][0].aligne != true && matches[i].length*matches.length > 3 ){
					PointBuster.PointBurst(gameSprite, matches[i].length*matches.length, matches[i][0].x, matches[i][0].y+25, 1);
					sound.joue_song(Math.ceil(Math.random()*26)+1);
					if( nb_chain == 2 )
						effect.effet_combo(gameSprite, matches[i][0].x, matches[i][0].y, 30, nb_boule_comb_effet, 2, type_image_effect);
					
					Vibrations.vibrationActiver = vibrationActiver;

					Vibrations.doubleVibration();
				}
				
				for(var j:int=0; j<matches[i].length; j++) {
				
					if ( gameSprite.contains(matches[i][j]) && matches[i][j].aligne != true ){
						matches[i][j].aligne = true;
						tab.push(matches[i][j]);
					}
				}
			}

			if( matches.length != 0 )
				gestionDevoilementMur();
			if( tab.length != 0 )
				animation_clignote(gameSprite, tab);
		}

		var vitesse_clignotement :int = 100;
		var temp_attente_avt_supr:int = 300;/* temp d'attente avant l'animation de la supression des pion*/

		/* Fais clignoter les Pions */
		function animation_clignote( gameSprite:Sprite, tab:Array ):Array{
			var compt:int = 0;
			
			/* timer pour repetition*/
			var time_clignote:Timer = new Timer(vitesse_clignotement, 7);
			time_clignote.addEventListener(TimerEvent.TIMER, time);
			time_clignote.start();
			pause_timer_local(time_clignote);
			
			function time():void{
				pause_timer_local(time_clignote);
				change_style_image(gameSprite, tab);
				compt++;

				if(compt == 7){
					/* Attente avant disparition */
					setTimeout(animation_suppression_pion, temp_attente_avt_supr, gameSprite, tab);
				}
			}
			
			return tab;
		}


		/* Change l'image du pion */
		function change_style_image( gameSprite:Sprite, tab:Array ):void{
			
			for(var i:int=0; i<tab.length; i++) {
				if(tab[i] != null && gameSprite.contains(tab[i]) ){
					
					if( tab[i].color_change == true){
						tab[i].color_change = false;
						tab[i].gotoAndStop( tab[i].type );
					}
					else {
						tab[i].color_change = true;
						tab[i].gotoAndStop( tab[i].type + "a3r" );
					}
				}
			}
		}


		/**/
		function animation_suppression_pion(gameSprite:Sprite, tab:Array):void{
			var i:int = 0;
			var num_type_effect:int = Math.ceil(Math.random() * 4)-1;

			/*timer pour repetition*/
			var time_supression:Timer = new Timer(vitesse_anime_supresion, tab.length+1);
			time_supression.addEventListener(TimerEvent.TIMER, time);
			time_supression.start();
			
			pause_timer_local(time_supression);
			
			function time():void{
				pause_timer_local(time_supression);
				if( i < tab.length ) {
					if( tab[i] != null && gameSprite.contains(tab[i]) && tab[i].alpha != 0 ){
						tab[i].disparition();
						
						gameScore += 100 * (nb_chain - 1);
						PointBuster.pointGagner(gameSprite, (100 * (nb_chain - 1)), tab[i].x, tab[i].y);
						
						effect.effet_frame(gameSprite, tab[i].x, tab[i].y, tab[i], num_type_effect);

						sound.joue_song_plop(i+1, nb_chain-1);
						
						/* */
						reload_objectif(tab[i]);
					}
				}
				else{
					suppression_pion(gameSprite, tab);
					ajouterResteDuMur();
					//trace("anime supp")
				}
				i++;
			}
			
		}

		/* */
		function suppression_pion(gameSprite:Sprite, tab:Array):void{

			for(var i:int=0; i<tab.length; i++){
				if( tab[i] != null && gameSprite.contains(tab[i]) ){
					gameSprite.removeChild(tab[i]);

					//supprime l'element du tableau de glace si s'en est un
					if( tabGlace != null && tabGlace.indexOf(tab[i]) != -1 ){
						tabGlace[( tabGlace.indexOf(tab[i]) )] = null;
					}
					
					grid[ tab[i].col ][ tab[i].row ] = null;
					
					/*fais chuter les pions*/
					verif_inite.affectAbove_basic( grid, tab[i] );
				}
			}
		}


		/**
		 * Stop les timer en cas de pause et reprend aprés la pause
		 * @param timer le timer à gérer
		 * Stop le timer si var_pause est vrai et que le timer est en cours
		   Sinon relance le timer si var_pause est faux et que le timer n'est pas en cours
		 */
		private function pause_timer_local(timer:Timer):void{
			verif_pause();
			
			function verif_pause():void{
				if( var_pause && timer.running){
					timer.stop();
					addEventListener(Event.ENTER_FRAME, verif_pause);
				}
				else if( !var_pause && !timer.running ){
					timer.start();
					removeEventListener(Event.ENTER_FRAME, verif_pause);
				}
			}
		}


		/*=======================================

						Monter

		=======================================*/

		function couleur_origine( col:int, row:int ):void{
			var  c:Color = new Color(); 
			// définir la couleur de la teinte et de régler le multiplicateur / alpha 
			c.setTint( 000000, 0.1 ) ; 
			// appliquer la teinte à la propriété colorTransform du MovieClip / DisplayObject souhaité 
			grid[col][row].transform.colorTransform = c ;
		}


		/* Verfie si un objet touche le plafond */
		private function touche_plafond():Boolean{
			for(var col:int=0; col<6; col++){
				if( grid != null && grid[col][0] != null && grid[col][0].y <= 0 ){
					return true;
				}
			}
			return false;
		}


		/* Verfie si un objet touche le plafond */
		public function is_third_etage():Boolean{
			for(var col:int=0; col<6; col++){
				if( grid != null && grid[col][8] != null ){
					return false;
				}
			}
			return true;
		}


		/* Verfie si un objet atteigne une ligne donner */
		private function atteint_une_ligne( grid:Array, ligne:int ):Boolean{
			for(var col:int=0; col<6; col++){
				if( grid[col][ligne] != null && grid[col][ligne].y <= 0 ){
					return true;
				}
			}
			return false;
		}


		/* Monter des pions */
		private function monte( gameSprite:Sprite, grid:Array ):void{
			/* Monter le cadre */
			deplacement.offsetY -= vitesse_up;

			if(deplacement.offsetY < (-49) ){
				
				/* Descendre le cadre */
				deplacement.offsetY += 50;
				
				/* Monter les pions */
				for(var col:int=0; col<6; col++){
					for(var row:int=0; row<13; row++){
						
						/* Redonne leur couleur original au pion de la derniere ligne */
						if( row == 12 ){
							couleur_origine( col, row );
						}
						
						/* Monter les pion d'une ligne */
						if (grid[col][row] != null){
							grid[col][row].row--;
							grid[col][row-1] = grid[col][row];
							grid[col][row] = null;
						}
					}
				}
				
				/* Ajouter des noveau pion a la derniere ligne */
				for(col=0; col<6; col++){
					initGrid.saisi.addPiece(gameSprite, grid, col, 12, tabType[Math.ceil(Math.random() * numPieces)-1]);
				}
				
				/* Eviter qu'il y'est des corespondance dans la nouvel ligne */
				var tab_new_element:Array = validiter.alignement_une_ligne(grid, 12);
				
				for(col=0; col<tab_new_element.length; col++){
					if( tab_new_element[col].length != 0 ){
						tab_new_element[col][0].type = tabType[((tab_new_element[col][0].type + 1) % numPieces)];
						tab_new_element[col][0].gotoAndStop(tab_new_element[col][0].type);
					}
				}
				
				var tab_new_el:Array = validiter.tab_pion_similaire_au_dessu(grid, 12);
				
				for(col=0; col<tab_new_el.length; col++){
					tab_new_el[col].type = tabType[((tab_new_el[col].type + 1) % numPieces)];
					tab_new_el[col].gotoAndStop(tab_new_el[col].type);
				}
				
				initGrid.saisi.noircir_dernier_ligne_pion(grid);
			}
		}

		
		private var timer_monter:Timer = new Timer( vitesse_de_monter );

		public function timer_monter_start():void{
			timer_monter.addEventListener(TimerEvent.TIMER, time_monte);
			timer_monter.start();
		}

		public function timer_monter_stop():void{
			timer_monter.stop();
			timer_monter.removeEventListener(TimerEvent.TIMER, time_monte);
		}


		/*Sert as savoir combien de fois la monter rapide doit etre effectué*/
		private var nb_monte_utile:int = 0;
		private var compt:int = 0;

		function time_monte(inputEvent:TimerEvent):void{
			if( timer_monter.delay == 20 ){
				compt++;
				//trace("compt = "+compt+" nb monte "+nb_monte_utile);
				if( compt >= nb_monte_utile ){
					compt = 0;
					timer_monter.delay = vitesse_de_monter;
				}
			}

			if( !touche_plafond() && !pion_aligne_exist(grid) && verif_inite.verif_avec_y(grid, deplacement.offsetY) && begin ){
				monte(gameSprite, grid);

				if( is_third_etage() ){
					/*Deplacement Main*/
					main.deplaceX_or_Y(11);
				}//TODO attention mur verifier si cela marche quand il y'a les murs
				else{
					main.stop();
				}
			}
		}


		public function monte_vite():void{
			if( timer_monter.delay == vitesse_de_monter && compt == 0 && grid[0][12] != null && grid[0][12].y == grid[0][12].row*deplacement.spacing+deplacement.offsetY){
				//trace(timer_monter.delay)
				timer_monter.delay = 20;
				
				/*Calcul le nombre nécessaire de monté rapide qui devra etre effectuer*/
				nb_monte_utile = ( 50 - Math.abs( deplacement.offsetY ) )/ 10;
			}
		}

		/*=======================================

					Gestion Score

		=======================================*/

		public function set_gameScore(score:int):void{
			this.gameScore = score;
		}		

		public function get_gameScore(): int{
			return this.gameScore;
		}	


		/*=======================================

					Gestion des Menus

		=======================================*/

		/* Pause */
		public function get_var_pause():Boolean{
			return var_pause;
		}

		public function set_var_pause(var_pause:Boolean):void{
			 this.var_pause = var_pause;
		}

		var square:Shape = new Shape; 
		
		private function apparition_menu(menu:DisplayObject):void{
			var twenplac:Array = new Array();
			
			twenplac.push( new Tween(menu, "z", Bounce.easeOut, menu.z, 0, 0.5, true));
			twenplac.push( new Tween(menu, "alpha", Regular.easeOut, 0, 1, 0.4, true));
			addChild(square);
			setChildIndex(menu, numChildren-1);
			setChildIndex(square, numChildren-2);
		}

		private function disparition_menu(menu:DisplayObject):void{
			var twenplac:Array = new Array();

			twenplac.push( new Tween(menu, "z", Strong.easeOut, 0, -500, 0.5, true));
			twenplac.push( new Tween(menu, "alpha", Regular.easeOut, 1, 0, 0.3, true));
			removeChild(square);
		}

		public function pause(menu:int = 0):void{
			if(begin){
				if( !var_pause ){
					var_pause = true;
					if( menu == 0 ){
						apparition_menu(menu_paus);
						menu_paus.active();
					}
					//dep_sound.pause();
					timer_temps.stop();
					timer_monter.stop();
				}
				else{
					var_pause = false;
					if( menu == 0 ){
						disparition_menu(menu_paus);
						menu_paus.desactive();
					}
					//dep_sound.play();
					timer_temps.start();
					timer_monter.start();
				}
			}
		}


		/*=======================================

					Objectif et Plus

		=======================================*/

		public var fini:Boolean = false;


		/*Gestion du Panneau d'objectif*/
		public function reload_objectif(element:Object):void{
			for( var e:int=0; e<panneau_objectif_obj.getTabLength(); e++ ){
				if( element.type == panneau_objectif_obj.tab_piece_obj[e].type && panneau_objectif_obj.tab_piece_obj[e].nombre > 0 ){
					panneau_objectif_obj.tab_piece_obj[e].nombre--;
					panneau_objectif_obj.tab_piece_obj[e].ar.txt.text = String(panneau_objectif_obj.tab_piece_obj[e].nombre);
					anime(panneau_objectif_obj.tab_piece_obj[e]);
					
					/* Valide l'objectif */
					if( panneau_objectif_obj.tab_piece_obj[e].nombre == 0 && panneau_objectif_obj.tab_piece_obj[e].valide != true ){
						panneau_objectif_obj.tab_piece_obj[e].ar.txt.text = "";
						panneau_objectif_obj.valide(panneau_objectif_obj.tab_piece_obj[e]);
						if(panneau_objectif_obj.tab_piece_obj[e].type != 14)
							panneau_objectif_obj.tab_piece_obj[e].gotoAndStop(panneau_objectif_obj.tab_piece_obj[e].type+"a3r");
						anime(panneau_objectif_obj.tab_piece_obj[e]);
					}
				}
			}
			
			function anime(element:Object):void{
				var twenn_scaleX:Tween = new Tween(element, "scaleY", Bounce.easeOut, 0.3, 0.6, 1, true);
				var twenn_scaleY:Tween = new Tween(element, "scaleX", Bounce.easeOut, 0.3, 0.6, 1, true);
			}
		}


		private function objectifs_rempli():Boolean{
			
			/* Verifie si le score obtenu n'est pas superieur au score minimum attendue */
			if( objectifs.getScore() != null && objectifs.getScore() > gameScore ){
				return false;
			}
			
			/* Verifie si un des Objetif Objet n'a pas était atteints */
			for( var e:int=0; e<panneau_objectif_obj.tab_piece_obj.length; e++ ){
				if( ! panneau_objectif_obj.tab_piece_obj[e].valide ){
					return false;
				}
			}
			return true;
		}

		private function ecoute_mv():void{
			this.addEventListener(Event.ENTER_FRAME, mv);
		}

		private function stop_mv():void{
			this.removeEventListener(Event.ENTER_FRAME, mv);
		}

		var inf:int;
		var haut:int;
		var ancien_posy:Number;

		/**/
		private function rebond():void{
			ecoute_mv();
			inf = 0;
			haut = 30;
			ancien_posy = gameSprite.y;
		}

		/*  */
		private function mv(evt:Event):void{
			gameSprite.y = ancien_posy + Math.sin(inf) * haut;
			haut--;
			inf++;

			if( haut <= 0 ){
				stop_mv();
				gameSprite.y = ancien_posy;
			}
		}

		private function perd(raison:String):void{
			var perdu:Perdu = new Perdu();
			perdu.play();
			
			rebond();

			/* Vibration */
			Vibrations.vibrationActiver = vibrationActiver;
			Vibrations.vibration(2000);

			for(var col:int=0; col<6 ; col++){
				change_style_image(gameSprite, grid[col] );
			}

			var text_info:Text_info = new Text_info(raison);
			addChild(text_info);
			text_info.descente_monter();

			function suite():void{
				var menu_perd:Menu_perd  = new Menu_perd();
				menu_perd.x = stageWidth/2;
				menu_perd.y = -200;
				menu_perd.name = "menu_perd";
				menu_perd.raison.text = raison;
				menu_perd.alpha = 1;
				menu_perd.score.text = "Score : " + String(gameScore);
				menu_perd.width = stageWidth - 80;
				menu_perd.scaleY = menu_perd.scaleX;

				if( save != null && save.size != 0 ){
					if( save.data.nivo[nivau] != null && save.data.nivo[nivau].score != null ){
						menu_perd.higthScore.text = "Meilleur score : " + String( save.data.nivo[nivau].score );
					}
				}

				addChild(menu_perd);
				var tween_menu_perd:Tween = new Tween(menu_perd, "y", Regular.easeOut, -menu_perd.height, stageHeight/2, 0.2, true);
				var tween_alpha:Tween = new Tween(gameSprite, "alpha", Regular.easeOut, 1, 0, 0.7, true);
				
				for(var row:int=0; row<12 ; row++){
					for(var col:int=0; col<6 ; col++){
						if(grid[col][row] != null)
							gameSprite.removeChild(grid[col][row]);
					}
				}

				addChild(square);
				setChildIndex(square, numChildren-2);
			}
			setTimeout(suite, 2500*2);
		}

		/**/
		private function perdu_gagner(grid:Array):Boolean{
			if( nb_seconde_temps <= 0 && !verif_pion_non_ater_exist_g( grid ) && !pion_aligne_exist( grid ) && validiter.fndAndRemoveMatchesT(grid) ){
				if( !objectifs_rempli() ){
					//trace("Perdu");
					fini = true;
					pause(1);

					//perd("Dommage vous n'avez pas rempli tous les objectifs.");
					perd("Too bad you did not fulfill all the objectives.");
					return true;
				}
				else if( objectifs_rempli() ){

					if(save != null && save.size != 0 ){
						if( save.data.nivo[nivau].score == null || save.data.nivo[nivau].score < gameScore ){
							save.data.nivo[nivau].score = gameScore;
							save.flush();
						}

						if( save.data.nivo_map == undefined || save.data.nivo_map == null || save.data.nivo_map <= nivau ){
							save.data.nivo_map = nivau+1;
							//trace(save.data.nivo_map+" sd "+(nivau+1)+" n")
							save.flush();
						}
					}

					fini = true;
					//trace("Gagné");
					pause(1);

					//var text_info:Text_info = new Text_info("Felicitation.");
					var text_info:Text_info = new Text_info("Congratulations.");
					addChild(text_info);
					text_info.descente_monter();

					function suite():void{

						var menu_gagne:Menu_Gagner  = new Menu_Gagner();
						menu_gagne.x = stageWidth/2;
						menu_gagne.name = "menu_gagne";
						menu_gagne.width = stageWidth - 80;
						menu_gagne.scaleY = menu_gagne.scaleX;
						menu_gagne.score.text = "Score : " + gameScore;

						if(save != null && save.size != 0 ){
							if( save.data.nivo[nivau].score != null ){
								//menu_gagne.higthScore.text = "Meilleur Score : " + save.data.nivo[nivau].score;
								menu_gagne.higthScore.text = "Best Score : " + save.data.nivo[nivau].score;
							}
						}

						addChild(menu_gagne);

						var tween_menu_gagne:Tween = new Tween(menu_gagne, "y", Regular.easeOut, -menu_gagne.height, stageHeight/2, 0.2, true);
						//var tween_alpha = new Tween(gameSprite, "alpha", Regular.easeOut, 1, 0, 0.7, true);
						gameSprite.alpha = 1;
						var tab:Array = new Array();

						for( var col:int=0; col<6; col++ ){
							for( var e:int=0; e<13; e++ ){
								if( grid[col][e] ){
									tab.push(grid[col][e]);
								}
							}	
						}

						addChild(square);
						setChildIndex(square, numChildren-2);

						var i:int = 0;
						var vitesse_anime_supresion_ici:Number = 150;

						/*timer pour repetition*/
						var time_supression:Timer = new Timer(vitesse_anime_supresion_ici, tab.length+1);
						time_supression.addEventListener(TimerEvent.TIMER, time);
						time_supression.start();
						
						function time():void{
							if( i < tab.length ) {
								if( tab[i] != null && gameSprite.contains(tab[i]) && tab[i].alpha != 0 ){
									tab[i].disparition();
									effect.effet_frame(gameSprite, tab[i].x, tab[i].y, tab[i], type_image_effect);
									sound.joue_song_plop(i+1, nb_chain-1);
								}
							}
							else{
								suppression_pion(gameSprite, tab);
							}
							i++;
						}

						if(sound.son_active)
							sound.tada3.play();

						setTimeout(menu_gagne.init_etoile, 800, objectifs, gameScore);
					}
					setTimeout(suite, 2500*1.5);
					return true;
				}
			}
			return false;
		}

		/*======================================

						Temps

		=======================================*/

		private var nb_seconde_temps:int = 60*2;// 2 minutes
		private var minutes:int = int(nb_seconde_temps/60);
		private var secondes:int = int(nb_seconde_temps % 60);
		private var timer_temps:Timer = new Timer(1000, nb_seconde_temps);

		private var seconde_touche_plafond_autoriser:int = 4;
		private var timer_plafond:Timer = new Timer(1000, seconde_touche_plafond_autoriser);

		/**/
		public function set_seconde_touche_plafond_autoriser(seconde:int):void{
			seconde_touche_plafond_autoriser = seconde;
		}

		/**/
		public function get_nb_seconde_temps():int{
			return nb_seconde_temps;
		}

		/**/
		public function get_secondes():int{
			return secondes;
		}
		
		/**/
		public function get_minutes():int{
			return minutes;
		}


		/**/
		public function lauch_time():void{
			timer_temps.addEventListener(TimerEvent.TIMER, Time_inv);
			timer_temps.addEventListener(TimerEvent.TIMER_COMPLETE, Time_inv_fin);
			if(begin)
				timer_temps.start();
		}
		
		
		/*Compte à reboure*/
		private function Time_inv(event:TimerEvent):void{
			//trace(nb_seconde_temps + " nb_seconde_temps");
			if( begin ){
				nb_seconde_temps--;
				minutes  = int(nb_seconde_temps / 60);
				secondes = int(nb_seconde_temps % 60);

				//Augmente la vitesse toutes les 10s
				if(nb_seconde_temps % 10 == 0){
					vitesse_de_monter -= 100;
					timer_monter.delay = vitesse_de_monter;
				}

				//Glace
				if( objectifs.getDicoHandicap() != null && objectifs.getDicoHandicap()["glace"] != null && nb_seconde_temps % 2 == 0 ){
					//tabGlace = grid.filter(function t():void{return });
					for( var col:int=0; col<6; col++ ){
						for( var row:int=10; row<12; row++ ){
							//TODO glace mur ? 10 verifier se nombre
							if( grid[col][row] != null && grid[col][row].type == 6 && tabGlace.indexOf(grid[col][row]) == -1){
								tabGlace.push(grid[col][row]);
							}
						}
					}

					for(var i:int=0; i<tabGlace.length; i++){
						if(tabGlace[i] != null){
							tabGlace[i].decrementRebour();
							var element:Element;
							if( tabGlace[i].getCompte_A_Rebour() == -1 ){
								for(var e:int=0; e<3; e++){
									element = tabGlace[i].choisiElement();
									tabGlace[i].attaqueElement(element, deplacement);

									if( element )
										setTimeout(ajoutGlaceAfterAttaque, 560, element);
								}
								tabGlace[i].initRebour(1);
							}
						}
					}
				}

				function ajoutGlaceAfterAttaque(element:Element):void{
					if( element )
						element.addGlace(new Glace(element.tailleDorigine));
				}

				//Boue
				
			}
		} 

		/**/
		public function anime_texte():void{
			var temp:int = 2.5;
			var newFormat:TextFormat = new TextFormat();
			newFormat.size = 15;
			newFormat.bold = false;

			/*Text*/
			var texte:txt_anime = new txt_anime();
			texte.txt.text = "Temps ecoulé";
			texte.txt.textColor = 0xFF0000;
			texte.txt.setTextFormat(newFormat);
			texte.txt.autoSize = TextFieldAutoSize.LEFT;

			texte.height = 50;
			texte.width = 300;
			texte.x = 250/2;
			texte.y = 700/2;
			texte.mouseEnabled = false;
			texte.txt.mouseEnabled = false;
			
			gameSprite.addChild(texte);
			
			var tweenH:Tween = new Tween(texte, "scaleY", Regular.easeOut, 0, 3, temp-0.5, true);
			var tweenW:Tween = new Tween(texte, "scaleX", Regular.easeOut, 0, 3, temp-0.5, true);
			var tweenA:Tween = new Tween(texte, "alpha", Regular.easeOut, 1, 0, temp+0.7, true);
			//var tweenX:Tween = new Tween(texte.txt, "x", Regular.easeOut, texte.x, texte.x-200, temp, true);

			//icic
			tweenA.addEventListener(TweenEvent.MOTION_FINISH, onFinish);
				
			function onFinish(evt:TweenEvent):void{
				var obj = evt.target.obj;
				gameSprite.removeChild(obj);
				obj = null;
			}
		}

		/**/
		private function Time_fin(evt:Event):void{
			var bol:Boolean = perdu_gagner(grid);

			if( bol )
				this.removeEventListener(Event.ENTER_FRAME, Time_fin);

			if(save != null && save.size != 0 && save.data.nivo != undefined && save.data.nivo[nivau] != undefined && save.data.nivo[nivau].score != null && save.data.nivo[nivau].score != undefined && save.data.nivo[nivau].score < gameScore ){
				save.data.nivo[nivau].score = gameScore;
				save.flush();
			}
			else if( save != null && (save.size == 0 || save.data.nivo[nivau] == null) ){
				save.data.nivo[nivau] = new Object;
				save.data.nivo[nivau].score = gameScore;
				save.flush();
			}
		}

		/**/
		private function Time_inv_fin(event:TimerEvent):void{
			this.addEventListener(Event.ENTER_FRAME, Time_fin);
			anime_texte();
		}

		/**/
		public function lose_touche_plafond():void{
			//trace("timer_plafond.currentCount "+timer_plafond.currentCount);
			if( touche_plafond() && (!timer_plafond.running || timer_plafond.currentCount != 0)  && !fini && !var_pause){
				touche_plafond_time();
			}
		}
		
		/**/
		private function touche_plafond_time():void{
			timer_plafond.addEventListener(TimerEvent.TIMER, time_eval_plafond);
			timer_plafond.addEventListener(TimerEvent.TIMER_COMPLETE, time_eval_plafond_fin);
			timer_plafond.start();
		}

		/* */
		private function time_eval_plafond(evt:TimerEvent):void{
			var corespondance:Boolean = pion_aligne_exist(grid);
						
			if( !touche_plafond() ){
				timer_plafond.reset();
				timer_plafond.removeEventListener(TimerEvent.TIMER, time_eval_plafond);
				timer_plafond.removeEventListener(TimerEvent.TIMER_COMPLETE, time_eval_plafond_fin);
			}
			else if( corespondance && nb_chain > 2 && timer_plafond.running ){
				timer_plafond.reset();
				timer_plafond.removeEventListener(TimerEvent.TIMER, time_eval_plafond);
				timer_plafond.removeEventListener(TimerEvent.TIMER_COMPLETE, time_eval_plafond_fin);
			}
			else if( corespondance && timer_plafond.running ){
				timer_plafond.stop();
			}
			else if( !corespondance && !timer_plafond.running ){
				timer_plafond.start();
			}

			if( var_pause ){
				timer_plafond.stop();
			}
		}

		/**/
		private function time_eval_plafond_fin(evt:TimerEvent):void{
			var verif_ater:Boolean = verif_inite.verif_avec_y( grid, deplacement.offsetY );

			if(touche_plafond() && !pion_aligne_exist(grid) && verif_ater && !var_pause && ! verif_pion_non_ater_exist_g( grid ) ){
				fini = true;
				pause(1);
				//perd("Attention au plafond.");
				perd("Watch out for the ceiling.");
			}
		}


		/*===================================
	
						Mur	

		=====================================*/

		public function ajouterMur(nombreEtages:int, grid:Array):void{
			Murs.push( new Mur(nombreEtages, numPieces) );
			var element:Element;
			var mur:Mur = Murs[ Murs.length-1 ];
			var row:int;
			var nombreLigneVide:int;

			//trace("Murs ", Murs);
			if( !touche_plafond() && nbLigneVide() > 0 ){
				if( Murs.length < 2 || ( Murs[ Murs.length-2 ] != null && Murs[ Murs.length-2 ].isComplet() ) )
				{
					nombreLigneVide = nbLigneVide()-1;
					//trace("nombreLigneVide ", nombreLigneVide);
					
					for(var etage:int=0; etage<mur.getNombreEtages() && etage < nombreLigneVide + 1 && (nombreLigneVide - mur.getNombreEtagesAjouter()) >= 0 ; etage++)
					{
						for(var col:int=0; col<6; col++)
						{
							row = nombreLigneVide - mur.getNombreEtagesAjouter();
							if( grid[col][row] == null ){
								grid[col][row] = mur.elements[col][etage];
								element = grid[col][row];
								element.col = col;
								element.row = row;
								element.y = -(deplacement.spacing * Math.abs(nombreLigneVide-row) + deplacement.offsetY + 50);
								element.ateri = false;
								gameSprite.addChild( element );
							}
							else{
								col = 7;
								etage = mur.getNombreEtages()+2;
								break;
							}
							//trace(mur.getNombreEtagesAjouter() , " mur.getNombreEtagesAjouter()");
						}
						mur.setNombreEtagesAjouter(mur.getNombreEtagesAjouter()+1);
					}
					gameSprite.addChild(mur);
					mur.addSpriteEffet(gameSprite);
				}
				else{
					ajouterResteDuMur();
				}
			}
		}


		/* */
		public function ajouterResteDuMur():void{
			var mur:Mur;
			var row:int;
			var nombreLigneVide:int = nbLigneVide()-1;
			var element:Element;
			var nombreLigneVideCompleter:int = 0;

			//trace("Murs Rest ",Murs)
			for(var e:int=0; e<Murs.length; e++){
				if( Murs[e] != null && ! Murs[e].isComplet() ){
					mur = Murs[e];
					var NombreEtagesAjouter:int = mur.getNombreEtagesAjouter();
					//trace("Murs Rest ",Murs)
					for(var i:int=NombreEtagesAjouter; i<mur.getNombreEtages() && nombreLigneVide >= nombreLigneVideCompleter; i++){
						if( !touche_plafond() ){
							//trace("Mur n° ", e, " Etage i " ,i)
							for(var col:int=0; col<6; col++){
								row = nombreLigneVide - nombreLigneVideCompleter;
								if(grid[col][row] == null){
									grid[col][row] = mur.elements[col][i];
									element = grid[col][row];
									element.col = col;
									element.row = row;
									element.y = -(deplacement.spacing * Math.abs(nombreLigneVide-row) + deplacement.offsetY + 50 );
									gameSprite.addChild( element );
								}
							}
							nombreLigneVideCompleter++;
							mur.setNombreEtagesAjouter(mur.getNombreEtagesAjouter()+1);
						}
						else{
							if( !gameSprite.contains(mur)  ){
								gameSprite.addChild(mur);
								mur.addSpriteEffet(gameSprite);
							}
							return;
						}
					}

					if( nombreLigneVide >= 0 && !gameSprite.contains(mur)  ){
						gameSprite.addChild(mur);
						mur.addSpriteEffet(gameSprite);
					}
				}
			}
		}

		/**/
		private function nbLigneVide():int{
			var nombre:int = 0;

			for(var row:int=0; row<12; row++){
				for(var col:int=0; col<6; col++){
					if(grid[col][row] != null)
						return nombre;
				}
				nombre++;
			}

			return nombre;
		}

		public function murToucherParElementAligner(mur:Mur):Boolean{
			var elementBas:Element;
			var elementHaut:Element;

			elementBas = mur.elements[0][0];
			if( mur.isComplet() )
				elementHaut = mur.elements[0][mur.getNombreEtages()-1];

			for( var col:int=0; col<6; col++ ){
				if( grid[col][elementBas.row+1] != null && grid[col][elementBas.row+1].aligne ){
					return true;
				}
				else if( mur.isComplet() && elementHaut.row-1 >= 0 && grid[col][elementHaut.row-1] != null && grid[col][elementHaut.row-1].aligne ){
					return true;
				}
			}

			return false;
		}

		public function rechercheMur_A_Devoiler():Array{
			var mur:Mur;
			var elementHaut:Element;
			var elementBas:Element;
			var mursADevoiler:Array = new Array;

			/* Recherche des murs toucher par des elements aligner */
			for(var i:int=0; i<Murs.length; i++){
				if( Murs[i] != null && Murs[i].getNombreEtagesAjouter() != 0 && !Murs[i].getCeDevoile() ){
					if( murToucherParElementAligner( Murs[i] ) ){
						mursADevoiler.push( Murs[i] );
					}
				}
				else{
					break;
				}
			}

			var murToucherParElementsAligner:Array = new Array;
			for( i=0; i<mursADevoiler.length; i++){
				murToucherParElementsAligner.push(mursADevoiler[i]);
			}

			for( i=0; i<murToucherParElementsAligner.length; i++ ){
				if( murToucherParElementsAligner[i] != null ){

					/* Murs du Bas */
					elementBas = murToucherParElementsAligner[i].elements[0][0];
					while( grid[elementBas.col][elementBas.row+1] != null && grid[elementBas.col][elementBas.row+1].mur ){
						/*???*/
						if( mursADevoiler.indexOf( grid[elementBas.col][elementBas.row+1].getMurBlock() ) == -1 )
							mursADevoiler.push( grid[elementBas.col][elementBas.row+1].getMurBlock() );
						elementBas = grid[elementBas.col][elementBas.row+1].getMurBlock().elements[0][0];
					}

					/* Murs du Haut */
					if( murToucherParElementsAligner[i].isComplet() ){
						mur = murToucherParElementsAligner[i];
						elementHaut = mur.elements[0][mur.getNombreEtages()-1];
						while( mur.isComplet() && elementHaut.row-1 >= 0 && grid[elementHaut.col][elementHaut.row-1] != null && grid[elementHaut.col][elementHaut.row-1].mur ){
							/*???*/
							mur = grid[elementHaut.col][elementHaut.row-1].getMurBlock();
							if( mursADevoiler.indexOf( mur ) == -1 )
								mursADevoiler.push( mur );
							if( mur.isComplet() )
								elementHaut = mur.elements[0][mur.getNombreEtages()-1];
						}
					}
				}
			}

			return mursADevoiler;
		}

		/**
		 *
		 */
		public function gestionDevoilementMur():void{
			var mur:Mur;
			var element:Element;
			var mursADevoiler:Array = rechercheMur_A_Devoiler();

			//trace("gestion mur");

			if( mursADevoiler ){
				for( var i:int=0; i<mursADevoiler.length; i++)
					mursADevoiler[i].reduire();

				/* Apres le clignotement */
				setTimeout( devoiler, vitesse_clignotement*7+50, mursADevoiler);
			}		
		}

		private function devoiler(mursADevoiler:Array):void{
			if( mursADevoiler.length != 0 ){
				//trace("mursADevoiler.length ", mursADevoiler.length);
				mursADevoiler[0].devoilerElements();
				var indice:int = 1;
				var inter:* = setInterval(devoile, 100);
				
				mursADevoiler[0].setTypeSonPlop(nb_chain-1);

				function devoile():void{
					if( indice < mursADevoiler.length && mursADevoiler[indice-1].getEntierementDevoiler() ){
						mursADevoiler[indice].devoilerElements();
						indice++;
					}
					else if( indice >= mursADevoiler.length && mursADevoiler[mursADevoiler.length-1].getEntierementDevoiler() ){
						
						for(var a:int=0; a<mursADevoiler.length; a++){
							mursADevoiler[a].afterAnimeDevoilement();
						}

						for(var i:int=0; i<Murs.length; i++){
							if( Murs[i].isVide() && gameSprite.contains(Murs[i]) ){
								gameSprite.removeChild(Murs[i]);
								Murs[i] = null;

								if( i != Murs.length-1 ){
									/* Decalage Array */
									for(var t:int = i; t<Murs.length-1; t++){
										Murs[t] = Murs[t+1];
									}
									Murs.pop();
								}
								else{
									Murs.pop();
								}
								//trace("Murs--.length ", Murs.length);
							}
						}

						clearInterval(inter);
					}				
				}
			}
		}	

		//temporaire
		public var nb_etage_mur:int = 1;

		public function ajoutMur():void{
			ajouterMur(nb_etage_mur, grid);
		}

		public function nb_etage_plus():void{
			if(nb_etage_mur < 6)
				nb_etage_mur++;
		}

		public function nb_etage_moin():void{
			if( nb_etage_mur > 1 ){
				nb_etage_mur--;
			}
		}

	}
}
//C16217F3E5CFA62E147F2AE617
