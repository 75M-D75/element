package classe{
	import fl.transitions.easing.*;
	import fl.transitions.*;

	import flash.utils.*;
	import flash.utils.Timer;
	import flash.events.*;
	import fl.motion.Color;
	import flash.display.*;
	import flash.text.*;
	import flash.utils.Timer;
	import flash.display.MovieClip;
	import flash.utils.Timer;
	import flash.utils.Dictionary;
	import flash.net.URLRequest;
	import flash.geom.Matrix; 
	import flash.geom.*;

	import classe.InfoMultiJoueur;
	import classe.SocketExample;
	import classe.extension.Vibrations;


	public class PlateauMultiJoueur extends Plateau {

		private var _tabNumberMurEtage:Array = [0,0,0];
		private var _sockEx:SocketExample = new SocketExample();


		public function receptionInfo(info:InfoMultiJoueur):void {

			if ( info.finChain ) {
				ajouterMur(info.nombreChains, grid);
			}
	
			tabNumberMurEtage[info.idMur] = info.nombreChains;

			trace(_tabNumberMurEtage);
		}

		public function get sock():SocketExample{
			return _sockEx;
		}


		public function traitementInfoString(str:String):void {
			var tabEnter:Array;
			var tabVirgule:Array;
			
			tabEnter = str.split("\n");
			if(tabEnter.length > 0 && tabEnter[0].charAt(0) == 'm')
				trace("il y'a un mure "+tabEnter)

			for(var i=0; i<tabEnter.length; i++){

				tabVirgule = tabEnter[i].split(",");
				if( tabVirgule[0].charAt(0) == 'm' ){
					trace("tabEnter "+tabEnter)
					var ch = Number(tabVirgule[0].substr(2));
					var ch2 = Number(tabVirgule[1]);
					var info:InfoMultiJoueur;

					if( tabVirgule[2] == "f" ){
						info = new InfoMultiJoueur(ch2, ch);
						info.finChains();
					}
					else{
						info = new InfoMultiJoueur(ch2, ch);
					}

					receptionInfo(info);
					//trace("chhh " + ch + " " + ch2);
					
				}
			}
		}


		private function envoiInfo(str:String):void {
			trace("envoi "+str);
			_sockEx.socket.writeUTFBytes(str);
			sock.socket.flush();
		} 


		public function get tabNumberMurEtage():Array {
			return _tabNumberMurEtage;
		}

		private var _idMure:Number = 1;

		/*Pour manipuler les correspondances trouvées*/
		override public function fndAndRemoveMatches(gameSprite:Sprite, grid:Array):void {

			if( pion_selectionner != null && pion_selectionner.selectionner && deplacement.verf(grid, pion_selectionner) == true ){
				deplacement.replacer(grid, pion_selectionner.col, pion_selectionner.row);
				pion_selectionner.posXfin = pion_selectionner.enr;
				pion_selectionner.x = pion_selectionner.posXfin;
				pion_selectionner.movee = false;
				pion_selectionner.selectionner = false;
				pion_selectionner.alpha = 1;
				pion_selectionner.removeEventListener(Event.ENTER_FRAME, mvt);
			
				if( !cercle.isPetit() ){
					cercle.position_x = pion_selectionner.x;
					cercle.position_y = pion_selectionner.y;
					cercle.retrecir_cercle();
				}
			}

			/* Obtenir la liste des correspondances */
			var matches:Array = validiter.lookForMatches( grid );
			var tab:Array = new Array();
			
			var verif_ater:Boolean = super.verif_inite.verif_avec_y( grid, deplacement.offsetY );
			
			var comb_effet:int;
			var nb_boule_comb_effet:int;
			
			/* Pour l'effet tourbillon */
			if(super.nb_chain > 1 && super.nb_chain < 9){
				nb_boule_comb_effet = super.nb_chain;
			}
			else{
				nb_boule_comb_effet = 8;
			}
				
			/* Pour Chain */
			if( matches.length != 0 && verif_pion_non_ater_exist(matches) ){
				super.nb_chain++;
				PointBuster.PointBurst(gameSprite, super.nb_chain-2, matches[0][0].x, matches[0][0].y+50+25, 2);
				sound.joue_song( super.nb_chain-1 );
				
				effect.effet_combo(gameSprite, matches[0][0].x, matches[0][0].y, 30, nb_boule_comb_effet, 2, 4);
				effect.set_rayon((super.nb_chain-2)*30);
				
				effect.cibleEffect(gameSprite, matches[0][0].x, matches[0][0].y);
				
				envoiInfo("m:"+_idMure+","+(super.nb_chain)+",o\n");

				/* Vibration */
				Vibrations.vibrationActiver = vibrationActiver;
				var time_vibration:int;

				if( super.nb_chain - 2 <= 7) {
					time_vibration = 200 + 100 * (super.nb_chain-2);
				}
				else {
					time_vibration = 900;
				}
				
				if( super.nb_chain <= 4 )
					Vibrations.vibration(time_vibration);
				else
					Vibrations.vibration(time_vibration);

			}
			else if( matches.length == 0 && verif_ater && !pion_aligne_exist(grid) ){
				
				if( sound.son_active ){
					if(super.nb_chain >= 4 && super.nb_chain < 6){
						sound.tada1.play();
						textSucces.setText("GREAT");
						textSucces.apparition();
					}
					else if(super.nb_chain >= 6 && super.nb_chain < 7){
						sound.tada2.play();
						textSucces.setText("NICE");
						textSucces.apparition();
					}
					else if(super.nb_chain >= 7){
						sound.tada3.play();
						textSucces.setText("AMAZING");
						textSucces.apparition();
					}
				}

				//Mur 
				if( super.nb_chain > 2 ){
					envoiInfo("m:"+_idMure+","+(super.nb_chain)+",f\n");
					if(_idMure == 1){
						_idMure = 2;
					}
					else{
						_idMure = 1;
					}
				}
				
				effect.set_rayon(20);/*rayon des effets*/
				super.nb_chain = 2;
			}

			if( verif_ater ){
				pions_tousse_ateri( grid );
			}	

			for(var i:int=0; i<matches.length; i++) {
				
				/* Combo */
				if( i == 0 && matches[i][0].aligne != true && matches[i].length*matches.length > 3 ){
					PointBuster.PointBurst(gameSprite, matches[i].length*matches.length, matches[i][0].x, matches[i][0].y+25, 1);
					sound.joue_song(Math.ceil(Math.random()*26)+1);
					if( super.nb_chain == 2 )
						effect.effet_combo(gameSprite, matches[i][0].x, matches[i][0].y, 30, nb_boule_comb_effet, 2, type_image_effect);
					
					Vibrations.vibrationActiver = vibrationActiver;

					Vibrations.doubleVibration();
					envoiInfo("m:"+_idMure+","+super.nb_chain+"\n");
				}
				
				for(var j:int=0; j<matches[i].length; j++) {
				
					if ( gameSprite.contains(matches[i][j]) && matches[i][j].aligne != true ){
						matches[i][j].aligne = true;
						tab.push(matches[i][j]);
					}
				}
			}

			if( matches.length != 0 )
				gestionDevoilementMur();
			if( tab.length != 0 )
				animation_clignote(gameSprite, tab);
		}
	}

}