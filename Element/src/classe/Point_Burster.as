package  classe{
	
    import flash.events.Event;
    import flash.text.TextFormat;
    import flash.text.TextDisplayMode;
    import flash.utils.Timer;
    import flash.display.MovieClip;
    import fl.motion.Color;
    import fl.transitions.Tween;
    import fl.transitions.easing.*;
    import fl.transitions.TweenEvent;
    import flash.events.Event;
    import flash.text.engine.TypographicCase;
    import flash.utils.Dictionary;
    import flash.display.Sprite;
    import flash.events.TimerEvent;
    import flash.text.*;
    import flash.utils.*;
    
	
	public class Point_Burster extends Sprite {
        
        private const animSteps:int = 1;
        private const animStepTime:int = 1100;
        private var tweenholder:Array = new Array();
        
        
        public function PointBurst(mc:Sprite, pts:int, x,y:int, type:int) {
            // Créer le champ texte 
            var tField:combot_chain = new combot_chain();
            
            // Création du sprite
            var burstSprite:Sprite = new Sprite();
            burstSprite.addChild(tField);
            if(type == 1){
                if(pts > 7)
                    tField.gotoAndStop("ch"+(7));
                else
                    tField.gotoAndStop("ch"+(pts-3));
                
            }
            else{
                //avec les Fois
                if(pts > 9)
                    tField.gotoAndStop((9));
                else
                    tField.gotoAndStop((pts));
            }
            
            var parentMC = mc;
            parentMC.addChild(burstSprite);
            burstSprite.x = x;
            burstSprite.y = y-40;

            // Démarrer l'animation
            var animTimer:Timer = new Timer(animStepTime, animSteps);
            rescaleBurst();
            animTimer.addEventListener(TimerEvent.TIMER_COMPLETE, removeBurst);
            animTimer.start();
            
            // Terminé, se supprime
            function removeBurst(event:TimerEvent) {
                burstSprite.removeEventListener(Event.ENTER_FRAME, eventMonte);
                burstSprite.removeChild(tField);
                parentMC.removeChild(burstSprite);
                tField = null;
                burstSprite = null;
                tweenholder = new Array();
                delete this;
            }

            function rescaleBurst(){
                burstSprite.alpha = 0.8;
                tweenholder.push( new Tween(burstSprite, "width", Bounce.easeOut, 300, 50, 0.5, true));
                tweenholder.push( new Tween(burstSprite, "height", Bounce.easeOut, 300, 50, 0.5, true));

                tweenholder[tweenholder.length-1].addEventListener(TweenEvent.MOTION_FINISH, onFinish);

                function onFinish(evt:TweenEvent){
                    monte(burstSprite);
                }
            }
        }

        private function monte(sprite:Sprite):void{
            sprite.addEventListener(Event.ENTER_FRAME, eventMonte);
        }

        private function eventMonte(evt:Event):void{
            evt.target.y -= 1;
        }

        //...................
        public function pointGagner(mc:Sprite, pts:int, x:int,y:int):void{
            //TODO trouver la bonne classe pour le tField
            // Créer le champ texte 
            var tField:TextPoint = new TextPoint();
            //tField.texte.autoSize = TextFieldAutoSize.CENTER;
            tField.mouseEnabled = false;
            tField.texte.mouseEnabled = false;
            tField.texte.text = "+"+pts;

            // Création du sprite
            var burstSprite:Sprite = new Sprite();
            burstSprite.addChild(tField);
            //noclick
            tField.mouseEnabled = false;
            
            var parentMC = mc;
            parentMC.addChild(burstSprite);
            //noclick
            burstSprite.mouseEnabled = false;
            burstSprite.x = x;
            burstSprite.y = y;

            // Démarrer l'animation
            var animTimer:Timer = new Timer(animStepTime, animSteps);
            rescaleBurst();
            animTimer.addEventListener(TimerEvent.TIMER_COMPLETE, removeBurst);
            animTimer.start();
            
            // Terminé, se supprime
            function removeBurst(event:TimerEvent) {
                burstSprite.removeEventListener(Event.ENTER_FRAME, eventMonte);
                burstSprite.removeChild(tField);
                parentMC.removeChild(burstSprite);
                tField = null;
                burstSprite = null;
                tweenholder = new Array();
                delete this;
            }

            function rescaleBurst(){
                burstSprite.alpha = 0.8;
                tweenholder.push( new Tween(burstSprite, "alpha", Bounce.easeOut, 1, 0.8, 0.1, true));
                //tweenholder.push( new Tween(burstSprite, "width", Bounce.easeOut, 300, 50, 0.5, true));
                //tweenholder.push( new Tween(burstSprite, "height", Bounce.easeOut, 300, 50, 0.5, true));

                tweenholder[tweenholder.length-1].addEventListener(TweenEvent.MOTION_FINISH, onFinish);

                function onFinish(evt:TweenEvent){
                    monte(burstSprite);
                }
            }
        }
	}
}