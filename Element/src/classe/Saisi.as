package  classe{
	
    import flash.utils.Timer;
    import fl.motion.Color;
    import flash.display.Sprite;
    import flash.events.*;
    
    import classe.Element;
    
	public class Saisi extends Sprite {
        
        /* Variable de placement Position Pion */
        public const spacing:Number = 50;// espace entre les element coresspond a la taille d'un element
        public var offsetX:Number = 0;//
        public var offsetY:Number = 0;//
        public var tabType:Array = new Array();//?
        
        /**
         * Ajouter des piece au sprite et à la grille! 
         * @model_pion est le type du pion (eau, feu ect...)
         */
        public function addPiece(gameSprite:Sprite, grid:Array, col,row:Number, model_pion:int) : Element{
    
            var newPiece:Element = new Element(col, row, model_pion);
            newPiece.x = newPiece.col*spacing+offsetX;
            newPiece.y = newPiece.row*spacing+offsetY;
            newPiece.gotoAndStop(newPiece.type);
            newPiece.addSprite(gameSprite);
            newPiece.addGrid(grid);
            gameSprite.addChild(newPiece);
            grid[col][row] = newPiece;
            
            return newPiece;
        }

        /**
         * pour les pion inactive de la derniere ligne 
         * @grid tableau contenant les elements du jeu
         */
        public function noircir_dernier_ligne_pion(grid:Array) : Array{
            var row = 12;
            
            for(var col=0; col<6; col++){
                if( grid[col][row] != null ){
                    // définir la couleur de la teinte et de régler le multiplicateur / alpha 
                    var  c:Color = new Color();
                    // appliquer la teinte à la propriété colorTransform du MovieClip / DisplayObject souhaité 
                    c.setTint(000000, 0.7) ;
                    // noirci l'element
                    grid[col][row].transform.colorTransform = c ;
                    grid[col][row].alpha = 0.5;
                }
            }

            return grid;
        }


        public function initTabType(tab:Array):void{
            if(tab != null){
                tabType = tab;
                //trace(tabType," tabType OK")
            }
        }

        public function getTabType():Array{
            return tabType;
        }
        
	}
}