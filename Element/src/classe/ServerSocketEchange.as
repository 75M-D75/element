package classe
{ 
    import flash.display.Sprite; 
    import flash.events.Event; 
    import flash.events.IOErrorEvent; 
    import flash.events.ProgressEvent; 
    import flash.events.ServerSocketConnectEvent; 
    import flash.net.ServerSocket; 
    import flash.net.Socket;
    import flash.display.MovieClip;

    import classe.GestionPartieServer;
    import classe.InfoMultiJoueur;
     
    public class ServerSocketEchange
    { 
        private var serverSocket:ServerSocket; 
        private var clientSockets:Array = new Array(); 
		private var port:int;
		private var ip:String;
        public  var tex:String = "KO";
        public  var texNombreConnexion:String = "KO";
        public  var tabGestion:Array = new Array();
		
		public function init_port(port:int, ip:String){
			this.port = port;
			this.ip = ip;
		}
        
        public function ServerSockete()
        { 
            try 
            { 
                // Create the server socket 
                serverSocket = new ServerSocket(); 
                 
                // Add the event listener 
                serverSocket.addEventListener( Event.CONNECT, connectHandler ); 
                serverSocket.addEventListener( Event.CLOSE, onClose ); 
                 
                // Bind to local port 8087 
                serverSocket.bind( port, String(ip) );
                 
                // Listen for connections 
                serverSocket.listen(); 
                trace( "Listening on " + serverSocket.localPort );
                tex = "Listening on " + serverSocket.localPort;
 
            }
            catch(e:SecurityError) 
            { 
                trace(e); 
            } 
        } 
 
        public function connectHandler(event:ServerSocketConnectEvent):void 
        { 
            //The socket is provided by the event object 
            var socket:Socket = event.socket as Socket; 
            clientSockets.push( socket ); 
             
            socket.addEventListener( ProgressEvent.SOCKET_DATA, socketDataHandler); 
            socket.addEventListener( Event.CLOSE, onClientClose ); 
            socket.addEventListener( IOErrorEvent.IO_ERROR, onIOError ); 
            var str:String = "";

            //Send a connect message 
            if( clientSockets.length > 0 && clientSockets.length % 2 != 0){
                creePartie( socket );
                str = "Create ";
            }
            else if( clientSockets.length > 0 ){
                joindrePartie( socket );
                str = "Join ";
            }
            
            for(var i=0; i<clientSockets.length; i++){
                if( clientSockets[i] != null ){
                    clientSockets[i].writeUTFBytes("Nouveau Connecté au serveur " +str);
                    clientSockets[i].flush();
                }
            }
             
            trace( "Sending connect message " + str);
            tex = "Sending connect message " + str;
            udapteNombreClient();


        } 
         
        /**
         * Evenement Donnée recu
         */
        public function socketDataHandler(event:ProgressEvent):void 
        { 
            var socket:Socket = event.target as Socket 
                 
            //Read the message from the socket 
            var message:String = socket.readUTFBytes( socket.bytesAvailable ); 
            //var message:String = "";
            var info:Object;
            //socket.position = 0;
            if(socket.bytesAvailable > 0){
                trace("socket.bytesAvailable "+socket.bytesAvailable + " " +socket.bytesPending )
            }
            
            if(socket.bytesAvailable > 7){
                trace("socket.bytesAvailable "+socket.bytesAvailable+ " " +socket.bytesPending )
                info = socket.readObject();
            }
            else{
                info = new MovieClip;
                info.chains = 1;
                info.id = 2;
            }
            info = new MovieClip;
            info.chains = 1;
            info.id = 2;
            //var info:* = socket.readObject();
            

            trace( "Received : " + message);
            //trace( "Onjeeeee : " + info.chains + " " + info.id);

            //message = "Message : " + message; 
            message = message; 
 
            // Echo the received message back to the sender 
            //message = "Message : " + message + info.chains + " " + info.id; 
			
			/*Echange donné*/ /*A gerer*/
            tex = " " + message;

            for(var i=0; i<tabGestion.length; i++){
                if( tabGestion[i] != null && tabGestion[i].isComplet() ){
                    if( tabGestion[i].joueur[0] == socket ){
                        tabGestion[i].joueur[1].writeUTFBytes( message );
                        tabGestion[i].joueur[1].flush();
                        tex = (" " + tex + "Sending: " + message + " à " + socket.localAddress + " au client n° " + String(i+2) );
                    }
                    else if(tabGestion[i].joueur[1] == socket){
                        tabGestion[i].joueur[0].writeUTFBytes( message );
                        tabGestion[i].joueur[0].flush();
                        tex = (" " + tex + "Sending: " + message + " à " + socket.localAddress + " au client n° " + String(i+1) );
                    }
                }
            }
            
        } 
        
        /**
         */
        private function onClientClose( event:Event ):void 
        { 
            trace( " Connection to client closed." ); 
            tex = ( " Connection to client closed." );
			
			/*Spression du client*/
			var client = event.target;
			for(var i:int=0; i<clientSockets.length; i++){
                
                if( clientSockets[i] != null && client == clientSockets[i] ){
                    for(var e:int=0; e<tabGestion.length; e++){
                        
                        if( ( client == tabGestion[e].joueur[0] )  ){
                            
                            tabGestion[e].joueur[0].close();
                            
                            /* Efface dans le tableau client le joeur 2 */
                            for(var f:int=0; f<clientSockets.length && (tabGestion[e].joueur[1] != null); f++){
                                if( tabGestion[e].joueur[1] == clientSockets[f] ){
                                    tabGestion[e].joueur[1].close();
                                    clientSockets.splice(f, 1);
                                }
                            }

                            tabGestion.splice(e, 1);
                        }
                        else if ( ( tabGestion[e].joueur[1] != null && client == tabGestion[e].joueur[1] ) ){
                            tabGestion[e].joueur[1].close();
                            tabGestion[e].joueur[1] = null;
                        }
                    }
					clientSockets.splice(i, 1);
                }
            }
			
			trace(" Nb_client " + clientSockets.length + " tabGestion " + tabGestion.length);
			udapteNombreClient();
        } 
 

        private function onIOError( errorEvent:IOErrorEvent ):void 
        { 
            trace( "IOError: " + errorEvent.text ); 
            tex = ( "IOError: " + errorEvent.text );
        } 
 
        private function onClose( event:Event ):void 
        { 
            trace( "Server socket closed by OS." ); 
            tex = ( "Server socket closed by OS." );
        } 


        public function creePartie(player:Object){
            var i:int;
            tabGestion.push( new GestionPartieServer(player) );
            i = (tabGestion.length - 1);
            tabGestion[ i ].setIdGestion( i );
            tabGestion[ i ].setNumTabServeur( i );
        }


        public function joindrePartie(player:Object){
            for(var i=0; i<tabGestion.length; i++){
                if( tabGestion[i] != null && !tabGestion[i].isComplet() ){
                    tabGestion[i].addPlayer(player);
                    tabGestion[i].setIdGestion( i );
                    tabGestion[i].setNumTabServeur( i );

                    /* Si les 2 joueurs son connecter enssemble envoyer un message
                     pour leur signalé que la connection est effectué */
                    if( tabGestion[i].isComplet() ){
                        tabGestion[i].joueur[0].writeUTFBytes( "connecter\n" );
                        tabGestion[i].joueur[0].flush();
                        tabGestion[i].joueur[1].writeUTFBytes( "connecter\n" );
                        tabGestion[i].joueur[1].flush();
                    }
                }
            }
        }

        public function udapteNombreClient(){
            texNombreConnexion = "Nombre de clients : " + clientSockets.length;
        }
    }
}