package classe{
    import flash.display.Sprite;

    public class SocketExample {
        public var socket:CustomSocket;
        
        public function SocketExample() {
            socket = new CustomSocket("127.0.0.1", 8002);
        }
    }
}

import flash.errors.*;
import flash.events.*;
import flash.net.Socket;

class CustomSocket extends Socket {
    public var response:String;
    public var nb:int = 3;
    public var bufferReponse:Array = new Array();
    
    public function CustomSocket(host:String = null, port:uint = 0) {
        super();
        configureListeners();
        if (host && port){
            super.connect(host, port);
        }
    }

    private function configureListeners():void {
        addEventListener(Event.CLOSE, closeHandler);
        addEventListener(Event.CONNECT, connectHandler);
        addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
        addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
        addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
    }

    public function writeln(str:String):void {
        str += "\n";
        try {
            writeUTFBytes(str);
        }
        catch(e:IOError) {
            trace(e);
        }
    }

    private function sendRequest():void {
        trace("sendRequest");
        response = " null ";
        writeln("GETer /");
        flush();
    }

    private function readResponse():void {
        var str:String = readUTFBytes(bytesAvailable);
        response = str;
        bufferReponse.push(str);
    }

    private function closeHandler(event:Event):void {
        trace("closeHandler: " + event);
        trace(response.toString());
    }

    private function connectHandler(event:Event):void {
        trace("connectHandler: " + event);
        sendRequest();
    }

    /*Error*/
    private function ioErrorHandler(event:IOErrorEvent):void {
        trace("ioErrorHandler: " + event);
    }
    
    /*Error*/
    private function securityErrorHandler(event:SecurityErrorEvent):void {
        trace("securityErrorHandler: " + event);
    }
    
    /*Repoonse*/
    private function socketDataHandler(event:ProgressEvent):void {
        trace("socketDataHandler: = " + event);
        
        readResponse();
        trace("reponse : -- " + response);
    }
}

