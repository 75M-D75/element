package  classe{

    import flash.display.Sprite;
    import flash.events.*;
    import flash.media.*; 
	
	public class Sounds extends Sprite{
        
        /*Son de Felicitation*/
		public var tada1:s1 = new s1;
		public var tada2:s2 = new s2;
		public var tada3:s3 = new s3;
		public var ok:Boolean = false;
        
		public var switch_droit:s10 = new s10
		public var switch_gauche:s11 = new s11;

		public var son_active:Boolean = true;
		public var effet_son_active:Boolean = true;
        
        /*Son Correspondant au Cri*/
		var A:sA = new sA;
		var B:sB = new sB;
		var C:sC = new sC;
		var D:sD = new sD;
		var E:sE = new sE;
		var F:sF = new sF;
		var G:sG = new sG;
		var H:sH = new sH;
		var I:sI = new sI;
		var J:sJ = new sJ;
		var K:sK = new sK;
        var L:sL = new sL;
        var M:sM = new sM;
        var N:sN = new sN;
        var O:sO = new sO;
        var P:sP = new sP;
        var Q:sQ = new sQ;
        var R:sR = new sR;
        var S:sS = new sS;
        var T:sT = new sT;
        var U:sU = new sU;
        var V:sV = new sV;
        var W:sW = new sW;
        var X:sX = new sX;
        var Y:sY = new sY;
        var Z:sZ = new sZ;
        var Z1:sZ1 = new sZ1;
        var Z2:sZ2 = new sZ2;
        
		public function joue_song(nbr:Number){
			if(son_active && effet_son_active){
	            switch (nbr)
	            {
	                case 1:{
	                    A.play()
	                    break;
	                }
	                case 2:{
	                    B.play()
	                    break;
	                }
	                case 3:{
	                    C.play()
	                    break;
	                }
	                case 4:{
	                    D.play()
	                    break;
	                }
	                case 5:{
	                    E.play()
	                    break;
	                }
	                case 6:{
	                    F.play()
	                    break;
	                }
	                case 7:{
	                    G.play()
	                    break;
	                }
	                case 8:{
	                    H.play()
	                    break;
	                }
	                case 9:{
	                    I.play()
	                    break;
	                }
	                case 10:{
	                    J.play()
	                    break;
	                }
	                case 11:{
	                    K.play()
	                    break;
	                }
	                case 12:{
	                    L.play()
	                    break;
	                }
	                case 13:{
	                    M.play()
	                    break;
	                }
	                case 14:{
	                    O.play()
	                    break;
	                }
	                case 15:{
	                    P.play()
	                    break;
	                }
	                case 16:{
	                    Q.play()
	                    break;
	                }
	                case 17:{
	                    R.play()
	                    break;
	                }
	                case 18:{
	                    S.play()
	                    break;
	                }
	                case 19:{
	                    T.play()
	                    break;
	                }
	                case 20:{
	                    U.play()
	                    break;
	                }
	                case 21:{
	                    V.play()
	                    break;
	                }
	                case 22:{
	                    W.play()
	                    break;
	                }
	                case 23:{
	                    X.play()
	                    break;
	                }
	                case 24:{
	                    Y.play()
	                    break;
	                }
	                case 25:{
	                    Z.play()
	                    break;
	                }
	                case 26:{
	                    Z1.play()
	                    break;
	                }
	                case 27:{
	                    N.play()
	                    break;
	                }
	                default:{
	                    Z2.play()
	                    break;
	                }
	            }
            }
		}
        
        /*Son plop pour eclatement*/
		var p_n0_1:plop_n0_1 = new plop_n0_1;
		var p_n0_2:plop_n0_2 = new plop_n0_2;
		var p_n0_3:plop_n0_3 = new plop_n0_3;
		var p_n0_4:plop_n0_4 = new plop_n0_4;
		
		var p_n1_1:plop_n1_1 = new plop_n1_1;
		var p_n1_2:plop_n1_2 = new plop_n1_2;
		var p_n1_3:plop_n1_3 = new plop_n1_3;
		var p_n1_4:plop_n1_4 = new plop_n1_4;
		var p_n1_5:plop_n1_5 = new plop_n1_5;
		var p_n1_6:plop_n1_6 = new plop_n1_6;
		//n2
		var p_n2_1:plop_n2_1 = new plop_n2_1;
		var p_n2_2:plop_n2_2 = new plop_n2_2;
		var p_n2_3:plop_n2_3 = new plop_n2_3;
		var p_n2_4:plop_n2_4 = new plop_n2_4;
		var p_n2_5:plop_n2_5 = new plop_n2_5;
		var p_n2_6:plop_n2_6 = new plop_n2_6;
		var p_n2_7:plop_n2_7 = new plop_n2_7;
		var p_n2_8:plop_n2_8 = new plop_n2_8;
		var p_n2_9:plop_n2_9 = new plop_n2_9;
		var p_n2_10:plop_n2_10 = new plop_n2_10;
		
		var p_n3_1:plop_n3_1 = new plop_n3_1;
		var p_n3_2:plop_n3_2 = new plop_n3_2;
		var p_n3_3:plop_n3_3 = new plop_n3_3;
		var p_n3_4:plop_n3_4 = new plop_n3_4;
		var p_n3_5:plop_n3_5 = new plop_n3_5;
		var p_n3_6:plop_n3_6 = new plop_n3_6;
		var p_n3_7:plop_n3_7 = new plop_n3_7;
		var p_n3_8:plop_n3_8 = new plop_n3_8;
		var p_n3_9:plop_n3_9 = new plop_n3_9;
		var p_n3_10:plop_n3_10 = new plop_n3_10;
		
		var p_n4_1:plop_n4_1 = new plop_n4_1;
		var p_n4_2:plop_n4_2 = new plop_n4_2;
		var p_n4_3:plop_n4_3 = new plop_n4_3;
		var p_n4_4:plop_n4_4 = new plop_n4_4;
		var p_n4_5:plop_n4_5 = new plop_n4_5;
		var p_n4_6:plop_n4_6 = new plop_n4_6;
		var p_n4_7:plop_n4_7 = new plop_n4_7;
		var p_n4_8:plop_n4_8 = new plop_n4_8;
		var p_n4_9:plop_n4_9 = new plop_n4_9;
		var p_n4_10:plop_n4_10 = new plop_n4_10;

		public function joue_song_plop(nbr,niv:Number){
			if(son_active){
				if(niv==1){
					if(nbr==1){
						p_n0_1.play();
					}
					if(nbr==2){
						p_n0_2.play();
					}
					if(nbr==3){
						p_n0_3.play();
					}
					if(nbr>=4){
						p_n0_4.play();
					}
				}
				if(niv==2){
					if(nbr==1){
						p_n1_1.play();
					}
					if(nbr==2){
						p_n1_2.play();
					}
					if(nbr==3){
						p_n1_3.play();
					}
					if(nbr==4){
						p_n1_4.play();
					}
					if(nbr==5){
						p_n1_5.play();
					}
					if(nbr>=6){
						p_n1_6.play();
					}
				}
				if(niv==3){
					if(nbr==1){
						p_n2_1.play()
					}
					if(nbr==2){
						p_n2_2.play()
					}
					if(nbr==3){
						p_n2_3.play()
					}
					if(nbr==4){
						p_n2_4.play()
					}
					if(nbr==5){
						p_n2_5.play()
					}
					if(nbr==6){
						p_n2_6.play()
					}
					if(nbr==7){
						p_n2_7.play()
					}
					if(nbr==8){
						p_n2_8.play()
					}
					if(nbr==9){
						p_n2_9.play()
					}
					if(nbr>=10){
						p_n2_10.play()
					}
				}
				if(niv==4){
					if(nbr==1){
						p_n3_1.play()
					}
					if(nbr==2){
						p_n3_2.play()
					}
					if(nbr==3){
						p_n3_3.play()
					}
					if(nbr==4){
						p_n3_4.play()
					}
					if(nbr==5){
						p_n3_5.play()
					}
					if(nbr==6){
						p_n3_6.play()
					}
					if(nbr==7){
						p_n3_7.play()
					}
					if(nbr==8){
						p_n3_8.play()
					}
					if(nbr==9){
						p_n3_9.play()
					}
					if(nbr>=10){
						p_n3_10.play()
					}
				}
				if(niv>=5){
					if(nbr==1){
						p_n4_1.play()
					}
					if(nbr==2){
						p_n4_2.play()
					}
					if(nbr==3){
						p_n4_3.play()
					}
					if(nbr==4){
						p_n4_4.play()
					}
					if(nbr==5){
						p_n4_5.play()
					}
					if(nbr==6){
						p_n4_6.play()
					}
					if(nbr==7){
						p_n4_7.play()
					}
					if(nbr==8){
						p_n4_8.play()
					}
					if(nbr==9){
						p_n4_9.play()
					}
					if(nbr>=10){
						p_n4_10.play()
					}
				}
			}
		}
		
        /*Son pour boutons*/
		var sn_btn_continu:sn_btn_continue = new sn_btn_continue();
		var sn_btn_back:sn_btn_retour = new sn_btn_retour();
		
		public function joue_song_btn(nbr:Number){
			if(son_active){
				if(nbr==1){
					sn_btn_continu.play();
				}
				if(nbr==2){
					sn_btn_back.play();
				}
			}
		}
		
		var monde_water:mnd_water;
		var monde_moon:mnd_moon;
		var monde_forest:mnd_forest;
		var monde_ice:mnd_ice;
		var monde_flower:mnd_flower;
		var channel_monde:SoundChannel;
		var sonMondeChoisi;

		public var instru_monde_actif:Boolean;
		
		public function joue_song_monde(nbr:Number){
			if(son_active){
				instru_monde_actif = true;

				if( channel_monde != null ){
					channel_monde.stop();
					channel_monde = null;
				}

				switch(nbr){
					case 1:
						monde_water = new mnd_water;
						channel_monde = monde_water.play();
						sonMondeChoisi = monde_water;
						break;
					case 2:
						monde_moon = new mnd_moon;
						channel_monde = monde_moon.play();
						sonMondeChoisi = monde_moon;
						break;
					case 3:
						monde_forest = new mnd_forest;
						channel_monde = monde_forest.play();
						sonMondeChoisi = monde_forest;
						break;
					case 4:
						monde_ice = new mnd_ice;
						channel_monde = monde_ice.play();
						sonMondeChoisi = monde_ice;
						break;
					default:
						monde_flower = new mnd_flower;
						channel_monde = monde_flower.play();
						sonMondeChoisi = monde_flower;
				}
				channel_monde.addEventListener(Event.SOUND_COMPLETE, onPlaybackCompleteMonde);
			}
		}

		public function stop_song_monde(){
			
			instru_monde_actif = false;

			if(channel_monde != null){
				channel_monde.stop();
				channel_monde = null;
			}
		}

		/* Repete le song lorsqu'il est terminé */
		function onPlaybackCompleteMonde(event:Event) 
		{ 
		    //trace("The sound has finished playing.");
		    instru_monde_actif = false;
		    var num:int = Math.random()*5;
		    joue_song_monde(num);
		}


		private var sn12:s12 = new s12;
		private var channel:SoundChannel;
		private var son_s12_non_jouer:Boolean = false;


		public function atterissage(){
			if( son_active && !son_s12_non_jouer && ok ){
				channel = sn12.play();
				channel.soundTransform.volume = 1;
				channel.addEventListener(Event.SOUND_COMPLETE, onPlaybackComplete); 
				son_s12_non_jouer = true;
			}
		}

		/* Repete le song lorsqu'il est terminé */
		private function onPlaybackComplete(event:Event){ 
			son_s12_non_jouer = false;
		}

	}
}

