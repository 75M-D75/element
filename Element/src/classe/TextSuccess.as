package  classe{

    import flash.display.Sprite;
    import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import flash.display.MovieClip;

	import flash.text.*;
	import flash.utils.*;
	
	public class TextSuccess extends MovieClip{

		public var temp:Number = 0.5;
		public var descendu:Boolean = false;

		private var tweenY:Tween;
		private var textSucces:TextSucces;
		
		private var scale;

		public function TextSuccess(chain_text:String, largeur:Number=400, color_fond:uint = 0xFFFFFF, color_text:uint = 0x555555){
			//Format
			var newFormat:TextFormat = new TextFormat();
			newFormat.size = 100;
			newFormat.bold = false;

			/*Text*/
			textSucces = new TextSucces();
			textSucces.txt.text = chain_text;
			textSucces.txt.textColor = color_text;
			textSucces.txt.setTextFormat(newFormat);
			textSucces.txt.textColor = 0xFF3333;
			textSucces.height = 250;
			textSucces.width = 450;
			
			textSucces.txt.autoSize = TextFieldAutoSize.CENTER;
			textSucces.mouseEnabled = false;
			textSucces.txt.mouseEnabled = false;
			scale = textSucces.scaleX;

			textSucces.background = false;
			textSucces.alpha = 0;

			addChild(textSucces);
			
        }

        public function setText(texte:String):void{
        	textSucces.txt.text = texte;
        }

        public function getText():String{
        	return textSucces.txt.text;
        }

		
		public function apparition():void{
			textSucces.alpha = 1;
			var tweenH:Tween = new Tween(textSucces, "scaleY", Bounce.easeOut, 0, scale, 0.5, true);
			var tweenH2:Tween = new Tween(textSucces, "scaleX", Bounce.easeOut, 0, scale, 0.5, true);

			setTimeout(disparition , 1500);
		}
		

		public function disparition():void{
			//var tweenH:Tween = new Tween(textSucces, "alpha", Regular.easeIn, 1, 0, 0.2, true);
			textSucces.alpha = 0;
			//var tweenH2:Tween = new Tween(textSucces, "scaleX", Bounce.easeIn, scale, 0, 0.2, true);
		}


		
	}
}