
package  classe{
	
    import flash.display.Sprite;
		
	public class TextTuto extends Sprite {
				
		public function getTextTuto( num:int , language:String="En"):String{
			if(language == "Fr"){
				switch( num ){
					case 1:
						return "Commençons par un petit tutoriel, déplacez les éléments vers la droite ou vers la gauche pour les rassembler, à la verticale";
					case 2:
						return "Mais aussi à l'horizontale";
					case 3:
						return "Rassemblez plus de 3 éléments pour faire un 'Combo'";
					case 4:
						return "Vous pouvez aussi faire tomber les éléments pour les rassembler";
					case 5:
						return "Après la disparition des éléments, si les éléments tombants forment à nouveau une nouvelle combinaison vous réalisez une 'Chain'";
					case 6:
						return "Réaliser plusieurs Combos.";
					case 7:
						return "Mélanger 'Combos' et 'Chains'.";
					case 8:
						return "Plus il y a de 'Chains' plus il y a de points.";
					case 9:
						return "N'attendez pas que les éléments tombent, enchainez les déplacements pour réaliser une 'Chain'.";
					case 10:
						return "Avant que l'élément ne tombe, vous pouvez déplacer un élément juste en dessous de celui-ci.";
					case 16:
						return "Seuls les éléments du type feu peuvent faire fondre la glace, assemblez des éléments du type feu proche de la glace pour qu'elle disparaisse.";
					default:
						return "Felicitations ! Vous êtes prêts pour l'aventure.\nUne dernière chose si vous souhaitez monter les éléments plus rapidement, faite glissé votre doigt vers le haut.";
				}
			}
			else if(language == "En"){
				switch( num ){
					case 1:
						return "Let's start with a small tutorial, move the elements to the right or to the left to collect them, vertically";
					case 2:
						return "But also horizontally";
					case 3:
						return "Gather more than 3 elements to make a 'Combo'";
					case 4:
						return "You can also drop the elements to collect them";
					case 5:
						return "After the disappearance of the elements, if the falling elements form again a new combination you realize a 'Chain'";
					case 6:
						return "Réaliser plusieurs Combos.";
					case 7:
						return "Make several Combos.";
					case 8:
						return "The more Chains there are, the more points there are.";
					case 9:
						return "Do not wait for the elements to fall, chase the moves to achieve a 'Chain'.";
					case 10:
						return "Before the item falls, you can move an item just below it.";
					case 16:
						return "Only fire-type elements can melt the ice, assemble fire-type elements close to the ice so that it disappears.";
					default:
						return "Congratulations! You're ready for the adventure, one last thing if you want to mount the items faster, slid your finger up.";
				}
			}
			return "";
		
		}
		
		
	}
}