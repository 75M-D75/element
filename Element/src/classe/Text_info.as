package  classe{

    import flash.display.Sprite;
    import fl.transitions.Tween;
	import fl.transitions.easing.*;

	import flash.text.*;
	import flash.utils.*;
	
	public class Text_info extends Sprite{

		public var temp:Number = 0.5;
		public var rect:Sprite;
		public var descendu:Boolean = false;
		
		private var tweenY:Tween;
		private var largeurRect:Number = 0;
		
		public function Text_info(chain_text:String, largeur:Number=400, color_fond:uint = 0xFFFFFF, color_text:uint = 0x555555){
			/*Fond text*/
			rect = new Sprite();
			rect.graphics.beginFill(color_fond); 
			
			//Format
			var newFormat:TextFormat = new TextFormat();
			newFormat.size = 15;
			newFormat.bold = false;

			/*Text*/
			var texte:txt_anime = new txt_anime();
			texte.txt.text = chain_text;
			texte.txt.textColor = color_text;
			texte.txt.setTextFormat(newFormat);
			texte.txt.height = 50;
			texte.txt.width = 400-10;
			texte.txt.autoSize = TextFieldAutoSize.LEFT;

			texte.background = true;

			rect.graphics.drawRoundRect(0, 0, largeur+300, texte.txt.textHeight+70, 1);
			largeurRect = rect.width;
			texte.x = (100/2)+((rect.width)/2)-(texte.width/2);
			texte.y = (40/2)+(rect.height/2)-(texte.height/2);

			rect.graphics.endFill();

			rect.y = -rect.height;
			rect.addChild(texte);
			rect.x = (400/2)-((rect.width)/2);


			addChild(rect);
			
        }


        public function placeRectAtCenter(largeurTerrain:Number):void{
        	var placementX:Number;

        	if( largeurRect != 0 ){
        		placementX = largeurTerrain - largeurRect;
        		rect.x = (placementX/2);
        	}
        	
        }

		
		public function descente_monter():void{
			tweenY = new Tween(rect, "y", Regular.easeOut, 0-rect.height, 300, temp, true);
			setTimeout(remonte, 2500);
		}
		

		public function remonte():void{
			tweenY = new Tween(rect, "y", Regular.easeOut, (stage.stageHeight/2)-rect.height/2, -rect.height, temp, true);
			descendu = false;
		}
		

		public function descente():void{
			tweenY = new Tween(rect, "y", Regular.easeOut, -rect.height, (stage.stageHeight/2)-rect.height/2, temp, true);
			descendu = true;
		}
		
	}
}