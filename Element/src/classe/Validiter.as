package classe{
	    
	public class Validiter {
    
        /*? verifie si il y a des piece nul en dessous d'une piece donné */
        public function piece_nul(grid:Array, piece:Element):Boolean{
            
            //verifie si il ny pas de piece en desous
            for(var row=piece.row+1; row<12; row++){

                if( grid[piece.col][row] != null && grid[piece.col][row].mur == true){
                    return true;
                }
                else if( grid[piece.col][row] == null ){
                    return false;
                }
            }
            return true;
        }
        
        /* Rechercher des correspondances horizontales à partir de ce point! */
        private function getMatchHoriz(grid:Array, col:int, row:int):Array {
            var match:Array = new Array(grid[col][row]);
            
            for(var i:int=1;col+i<6;i++) {
                if(grid[col][row] != null){
                    var bol:Boolean = piece_nul(grid, grid[col][row]);
                    
                    if(bol==true){
                        if (grid[col+i][row] != null && grid[col][row] != null && grid[col][row].type == grid[col+i][row].type
                            && grid[col][row].y == grid[col+i][row].y && grid[col][row].x == grid[col+i][row].x-i*50
                            && grid[col+i][row].aligne != true && grid[col][row].aligne != true && grid[col][row+1] != null && grid[col+i][row+1] != null 
							&& grid[col][row].can_elimine && grid[col+i][row].can_elimine 
                        ){
                            match.push(grid[col+i][row]);
                        } 
                        else{
                            return match;
                        }
                    }
                }
            }
            return match;
        }
        
        
        /* Rechercher des correspondances verticales à partir de ce point! */
        private function getMatchVert(grid:Array, col:int, row:int):Array{
            var match:Array = new Array(grid[col][row]);
            
            for(var i:int=1; row+i<12; i++){
                if(grid[col][row] != null && grid[col][row].can_elimine != false){
                    var bol:Boolean = piece_nul(grid, grid[col][row]);
                    
                    if(bol==true){
                        var h = i+1
                        if(grid[col][row+i]!=null && grid[col][row]!=null && grid[col][row].type == grid[col][row+i].type
                            && grid[col][row].y == grid[col][row+i].y-i*50 && grid[col][row].x == grid[col][row+i].x && grid[col][row+h] != null
                            && grid[col][row+i].aligne != true && grid[col][row].aligne != true 
							&& grid[col][row].can_elimine && grid[col][row+i].can_elimine 
                        ){
                            match.push(grid[col][row+i]);
                        } else {
                            return match;
                        }
                    }
                }
            }
            return match;
        }
        
        
        /* enregistrement des correspondance */
        public function lookForMatches( grid:Array ):Array {
            var row:int
            var col:int
            var matchList:Array = new Array();
            
            // Rechercher les correspondances vertical
            for(col=0;col<6;col++) {
                for (row=0;row<10;row++) {

                    var match:Array = getMatchVert(grid, col, row);
                    if (match.length > 2) {
                        matchList.push(match);
                        row += match.length-1;
                    }
                }
            }
            
            // Rechercher les correspondances horizontales
            for (row=0; row<12; row++) {
                for(col=0;col<4;col++) {
                    match = getMatchHoriz(grid, col, row);
                    if (match.length > 2) {
                        matchList.push(match);
                        col += match.length-1;
                    }
                }
            }

            return matchList;
        }
		
       
        /**
         *Verifie si il y'a pas des correspondance dans grid (tableau deux dimension);
         *@param grid tableau a verifier;
         **/
        public function fndAndRemoveMatchesT( grid:Array ):Boolean {
            // Obtenir la liste des correspondances !
            var matches:Array = lookForMatches(grid);
            
            if( matches.length == 0 )
            {
                //trace("le tableau est vide");                
                return true;
            }
            else
            {
                //trace("il y a du contenu");
                return false;
            }
        }
		
		
		/*? Rechercher des correspondances horizontales à partir de ce point! */
        private function getMatchHoriz_une_ligne(grid:Array, col:int, row:int):Array {
            var match:Array = new Array(grid[col][row]);
            
            for(var i:int=1;col+i<6;i++) {
                if(grid[col][row] != null){
                    var bol:Boolean = piece_nul(grid, grid[col][row]);
                    
                    if(bol==true){
                        if (grid[col+i][row] != null && grid[col][row] != null && grid[col][row].type == grid[col+i][row].type
                            && grid[col][row].y == grid[col+i][row].y && grid[col][row].x == grid[col+i][row].x-i*50
                            && grid[col+i][row].aligne != true && grid[col][row].aligne != true 
                        ){
                            match.push(grid[col+i][row]);
                        } 
                        else{
                            return match;
                        }
                    }
                }
            }
            return match;
        }
		
		
        //?
		public function alignement_une_ligne(grid:Array, row:int):Array{
            var col:int
            var matchList:Array = new Array();
			var match:Array;
			
			for(col=0; col<4; col++){
				match = getMatchHoriz_une_ligne(grid, col, row);
				if (match.length > 2) {
					matchList.push(match);
					col += match.length-1;
				}
			}
			
			return matchList;
		}
		
		
		/*?  Pour la generation de pion aleatoir a la dernier ligne*/
		
		private function getMatchVert_une_ligne(grid:Array, col:int, row:int):Array {
            var match:Array = new Array(grid[col][row]);
            
            for(var i:int=1; col+i<6; i++) {
                if(grid[col][row] != null){
                    var bol:Boolean = piece_nul(grid, grid[col][row]);
                    
                    if(bol == true){
                        if (grid[col+i][row] != null && grid[col][row] != null && grid[col][row].type == grid[col+i][row].type
                            && grid[col][row].y == grid[col+i][row].y && grid[col][row].x == grid[col+i][row].x-i*50
                            && grid[col+i][row].aligne != true && grid[col][row].aligne != true 
                        ){
                            match.push(grid[col+i][row]);
                        } 
                        else{
                            return match;
                        }
                    }
                }
            }
            return match;
        }
		
        //?
		public function pion_similaire_au_dessu(grid:Array, col:int, row:int){

			if( grid[col][row] != null && grid[col][row-1] != null && grid[col][row-2] != null 
				&& grid[col][row].type == grid[col][row-1].type && grid[col][row-1].type == grid[col][row-2].type
				){
				return grid[col][row];
			}
			return null;
		}
		
		//?
		public function tab_pion_similaire_au_dessu(grid:Array, row:int):Array{
			var col:int;
			var pion;
			var match:Array = new Array();
			
			for(col=0; col<6; col++){
				pion = pion_similaire_au_dessu(grid, col,row);
				if( pion != null ){
					match.push(pion);
				}
			}
			
			return match;
		}

	}
}