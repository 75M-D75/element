﻿package classe.classe{
	
    import flash.utils.*;
    import flash.display.Sprite;
    import flash.display.MovieClip;
    
    import classe.Glace;
    import classe.Mur;
    import classe.Effect;

    import fl.transitions.Tween;
    import fl.transitions.easing.*;

	public class Element extends MovieClip {
		private var _col:int;// colonne 
		private var _row:int;// ligne 
        public var type:int;
        public var glace:Glace;//-?
        
        public var can_elimine:Boolean = true;//can = pouvoire
        public var can_deplace:Boolean = true;
        public var can_tombe:Boolean = true; 
        public var can_select:Boolean = true;
        public var glace_delete:Boolean;
        public var mur:Boolean = false;
        public var aligne:Boolean = false;//indique si l'element et aligner a au moin 2 autre element
        public var ateri:Boolean;
        public var selectionner:Boolean;
        public var movee:Boolean = false;//element en movement
        public var color_change:Boolean = false;
        public var chute:Boolean = false;//element en chute

        public var posx:Object = null;
        public var enr:Number;//?
        public var positionMouseX:Number = -1;
        public var posXfin:Number = -1;

		
        private var murBlock:Mur = null;
        private var gameSprite:Sprite;//?
        private var grid:Array;//tableau dans lequel l'element ce trouve
        private var effet:Effect = new Effect;//?
        private var compte_A_Rebour:int = 5;//avant l'attaque
        

        /* Constructeur */
        
        /**
         * Cree un Element (Constructeur)
         * @param col Colonne de l'element
         * @param row Ligne
         * @param type Son type numero (image ou element)
         * @param typePion Son type Element ou animal
         */

        public function Element(col:int=1, row:int=1, type:int=1){
            this.col = col;
            this.row = row;
            assert(type > 0);
            this.type = type;
            textE.mouseEnabled = false;

        }

        /******
            Mur
        *******/

        public function removeMurBlock():void{
            this.murBlock = null;
        }


        public function initeMurBlock(mur:Mur):void{
            this.murBlock = mur;
        }


        /**
         * Indique si l'objet courant a le meme type que l'objet passé en parametre
         * @param obj Object a comparé
         * @return Boolean true si c'est de meme type sinon false
         */
        public function sameType(obj:Object):Boolean{
            return obj.type == this.type && glace == null && obj.glace == null;
        }
        
        /**
         * Met l'alpha de l'objet à 0
         */
        public function disparition():void{
            this.alpha = 0;
        }

        //?
        private function apparition(obj:Object):void{
            if(obj != null){
                obj.alpha = 1;
                obj.scaleX = obj.scaleY = 1;
            }
        }

        //?
        private function animationApparitionGlace():void{
            var tweenScaleY = new Tween(this.glace, "scaleY", Regular.easeOut, 0, 1, 1, true);
            var tweenScaleX = new Tween(this.glace, "scaleX", Regular.easeOut, 0, 1, 1, true);
            setTimeout(apparition, 1100, this.glace);
        }

        /*? Ajouter Glace*/
        public function addGlace( glace:Glace ):void{
            if( this.type != 3 && this.glace == null && !aligne ){
                this.glace = glace;
                can_elimine  = false;
                can_select  = false;
                glace_delete = false;
                addChild( glace );

                animationApparitionGlace();
            }
        }

        //?
        public function addSprite(gameSprite:Sprite):void{
            this.gameSprite = gameSprite;
        }

        //?
        public function attaqueElement(element:Element):void{
            if( element != null && element != this && !aligne )
                effet.effet_roue(gameSprite, this.x, element.x, this.y, element.y);
        }

		//?
        public function afficherText(num:int):void{
            textE.text = String(num);
        }

        //?
        public function initRebour(num:int):void{
            compte_A_Rebour = num;
            afficheRebour();
        }

		//?
        public function decrementRebour():void{
            if( compte_A_Rebour >= 0 && glace == null )
                compte_A_Rebour--;
            afficheRebour();
        }

        //?
        public function afficheRebour():void{
            textE.text = String(compte_A_Rebour);
        }

        //?
        public function getCompte_A_Rebour():int{
            return compte_A_Rebour;
        }

        //?
        public function addGrid(grid:Array):void{
            this.grid = grid;
        }

        //?
        public function choisiElement():Element{
            var col:int = Math.random() * 5;
            var row:int = Math.random() * 11;
            
            if( grid[col][row] != null && !grid[col][row].mur ){
                return grid[col][row];
            }

            return null;
        }

        //?
        public function assert(truth:*): void
        {
            if ( !truth ){
                throw new Error("Assertion failed!");
			}
		}

        /**
         * retourne la colonne
         */
		public function get col() : int {
			return _col;
		}
		
        /**
         * retourne la ligne
         */
		public function get row() : int {
			return _row;
		}

        /**
         * modifie la colonne
         */
		public function set col(col : int) : void {
			this._col = col;
		}

        /**
         * modifie la ligne
         */
		public function set row(row : int) : void {
			_row = row;
		}

	}
}