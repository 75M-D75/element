package classe.classe{
	
	import flash.display.Sprite;

	import classe.Saisi;
	import classe.Validiter;
	import classe.VerifInit;
	
	public class InitGrid extends Sprite {
		private static const nbColonne:int = 6;//nombre de colonne
		private static const nbLigne:int = 13;//nombre de ligne

		private var validiter:Validiter = new Validiter();//?
		private var verif_inite:VerifInit = new VerifInit();//?
		private var gameSprite:Sprite;
		private var grid:Array;

		public var nbPions:int = 5;
		public var saisi:Saisi = new Saisi();


		public function InitGrid(tab:Array = null){
			saisi.initTabType(tab);
		}


		/**
		 * Initialisation de la grille grid Aleatoirement 
		 */
		public function setUpGrid_aleatoir():void {
			var aléa:Number;
			var col:int;
			var row:int;
			var valide:Boolean = false;
			var affecte_pion_ou_pas:Number;
			var gameSprite:Sprite;
			var grid:Array;
			
			// Boucler jusqu'à ce que la grille de départ soit valide !
			while( valide == false ){
				
				// Créer sprite symbole 3
				
				gameSprite = new Sprite();
					
				grid = [[]
				,[]
				,[]
				,[]
				,[]
				,[]
				,[]];
					
				aléa = Math.ceil(Math.random()*8);

				// Ajouter des pièces aléatoires!
				for(col=0; col<nbColonne; col++) {
					for(row=aléa; row<nbLigne; row++) {
						affecte_pion_ou_pas = Math.ceil(Math.random()*5);
						if( affecte_pion_ou_pas != 1 && row != 12 && row != 11 ){
							if( saisi.getTabType().length >= nbPions )
								saisi.addPiece( gameSprite, grid, col, row, saisi.tabType[Math.ceil(Math.random()*nbPions)-1]);
							else
								saisi.addPiece( gameSprite, grid, col, row, Math.ceil(Math.random()*nbPions)-1);
						}
						else if( row == 12 || row == 11 ){
							if( saisi.getTabType().length >= nbPions )
								saisi.addPiece( gameSprite, grid, col, row, saisi.tabType[Math.ceil(Math.random()*nbPions)-1]);
							else
								saisi.addPiece( gameSprite, grid, col, row, Math.ceil(Math.random()*nbPions)-1);
						}
						else{
							grid[col][row] = null;
						}
					}

					for(row=0; row<aléa; row++) {
						grid[col][row] = null;
					}
				}
				
				/**
				 * Verifi si il y a des place vide en dessou des pion
				   et fait descendre les pion si c'est le cas
				 *
				 */
				while( verif_inite.verfif(grid) == true ){
					verif_inite.nul(grid);
					
					for(var e:int=0; e<nbLigne; e++){
						for(var i:int=0; i<nbColonne; i++){
							verif_inite.placeinit(grid, i, e);
						}
					}
				}
				 
				/*Verifie si il y'a des corespondance*/
				valide = validiter.fndAndRemoveMatchesT(grid);

			};
			
			this.grid = grid;
			this.gameSprite = gameSprite;			
		};
		
		/**
		 *Donne un tableau d'entier (tab_ini), et remplie le tableau (grid) de Piece
		 */
		public function setUpGrid(tab_ini:Array):void {
			var col:int;
			var row:int;
			var gameSprite:Sprite;
			var grid:Array;
				
			gameSprite = new Sprite();
				
			grid = [[]
			,[]
			,[]
			,[]
			,[]
			,[]
			,[]];
					
			row = 0;
			
			for (col = 0; col<tab_ini.length; col++){
			
				if( (col % 6) == 0){
					row++;
				}
				
				/**/
				if( tab_ini[col] != 0 ){
					/**/
					saisi.addPiece(gameSprite, grid, (col % 6), row, tab_ini[col]);
					if(tab_ini[col] == 6){
						grid[(col % 6)][row].can_elimine = false;
						grid[(col % 6)][row].gotoAndStop("cage");
					}
				}
			
			}
			
			this.grid = grid;
			this.gameSprite = gameSprite;
		};

		public function getGameSprite():Sprite{
			return gameSprite;
		}

		public function getGrid():Array{
			return grid;
		}

	}
}