package classe.classe{
	
    import flash.utils.Timer;
    import flash.display.MovieClip;
    import fl.motion.Color;
    import flash.events.Event;
    import flash.utils.*;
    import flash.display.Sprite;
        
	public class VerifInit {
            
        public const spacing:Number = 50;//meme chose que dans la classe Validiter
        public var offsetX:Number = 0;
        //public var offsetY:Number = 0;
        
        /* Verifie si il y'a des pions non atterit */
        public function verfif( grid:Array ):Boolean{
            var bol:Boolean = false;
            
            for(var col=0;col<6;col++){
                for(var row=0;row<12;row++){
                    if( grid[col][row] != null && grid[col][row+1] == null ){
                        return true;
                    }
                }
            }
            return bol
        }
        
        /**?
         * Verifie si il y'a des pions non atterit
         * @param grid tableau de pion
         * @param offsetY 
         * @return boolean False si il y'a un element qui n'a pas ateri sinon True
         */
        public function verif_avec_y( grid:Array, offsetY:Number ):Boolean{
            
            for(var col=0; col<6; col++){
                for(var row=0; row<12; row++){
                    /*Warning*/
                    if( ( grid[col][row] != null && !grid[col][row].mur ) && ( grid[col][row+1] == null || grid[col][row].y < grid[col][row].row*spacing+offsetY ) 
                       ){
                        return false;
                    }
                    else if( ( grid[col][row] != null ) && grid[col][row].mur && !elementEnDessousDuMur(grid, grid[col][row]) ){
                        return false;
                    }

                }
            }
            return true;
        }
        
        /*? Inviter toutes les pièces au-dessus de celle-ci à descendre d'un cran!*/
        public function affectAbove_basic(grid:Array, piece:Element){	  
            for(var row:int = piece.row-1; row>=0; row--) {
                if ( grid[piece.col][row] != null && grid[piece.col][row].aligne != true && !grid[piece.col][row].mur ) {
                    
                    if( row == piece.row-1 )
                        grid[piece.col][row].chute = true;
                    
                    grid[piece.col][row].row++;
                    grid[piece.col][row].ateri = false;
                    
                    grid[piece.col][row+1] = grid[piece.col][row];
                    delete grid[piece.col][row];
                    grid[piece.col][row] = null;
                }
                else{
                    nulMur(grid);
                    return;}
                }
            return;
        }
        
        /*? Descendre les pion sur les place vide */
        public function affectAbove(grid:Array, piece:Element, nb_place_vide:int){
            var firstElement:Boolean = true;
            var ine:Boolean = false;
            for(var row:int=piece.row; row>=-5; row--){

                if ( grid[piece.col][row] != null && grid[piece.col][row].aligne != true && grid[piece.col][row+nb_place_vide] == null && row+nb_place_vide < 12 && ! grid[piece.col][row].mur ) {

                    if( firstElement ){
                        grid[piece.col][row].chute = true;
                        firstElement = false;
                    }
                    
                    grid[piece.col][row].row += nb_place_vide;
                    grid[piece.col][row + nb_place_vide] = grid[piece.col][row];
                    delete grid[piece.col][row];
                    grid[piece.col][row] = null;
                    ine = true;
                }
                else{
                    if(ine){
                        nulMur( grid);
                    }
                    return;
                }
            }
            return;
        }

        /**?
         * Renvoi un tableau avec des pions et le nombre de place vide en dessous de ces pion
         */
        private function tab( grid:Array ):Array{
            var list:Array = new Array();
            
            for(var col=0; col<6; col++){
                for(var row:int=-8; row<11; row++) {
                    var arc:Array = mar(grid, col, row);
                    
                    if(arc.length == 2){
                        /*arc[0]=Element, arc[1]=nb_place_vide en dessou de piece*/
                        list.push(arc[0], arc[1]);			
                    }
                }	
            }
            return list;
        }
        
        /**
         * Renvoi un tableau avec un pion et le nombre de place vide en dessous de celle ci
         */
        private function mar(grid:Array, col,row:int):Array{
            var ar:Array = new Array();
            var nb_place_vide:Number = 0;
            
            for(var i:int=1; row+i<12; i++) {
                if ( grid[col][row] != null && grid[col][row+i] == null && grid[col][row].can_tombe && !grid[col][row].mur )
                {
                    if( i == 1 ){
                        ar.push(grid[col][row]);
                    }
                    nb_place_vide++;
                }
                else
                {   
                    ar.push(nb_place_vide);
                    return ar;
                }
            }
            ar.push(nb_place_vide);
            return ar;
        }

        /*? Faire decendre objet ou il ya rien en dessous */
        public function nul( grid:Array ){
            var A:Array = tab(grid);
            var ine:Boolean = false;
            
            for(var d=0; d<A.length; d+=2){
                if( A[d] != null && ! A[d].aligne && ! A[d].ateri  && grid[A[d].col][A[d].row+1] == null ){
                    /*Descendre*/
                    affectAbove(grid, A[d], A[d+1]);
                    ine = true;
                }
            }
            if(ine){
                nulMur(grid);
            }
        }
        
        
        /*? Faire decendre objet ou il ya rien en dessous*/
        public function nul_avc_temp(gameSprite:Sprite, grid:Array, temp:int = 200):Boolean{
            var A:Array = tab(grid);
            var descenteElementsNonAteri:Boolean = false;

            for(var d=0; d<A.length; d+=2){
                if( A[d] != null && !A[d].aligne && !A[d].ateri && grid[A[d].col][A[d].row+1] == null && gameSprite.contains(A[d]) ){
                    /*Descendre*/
                    affectAbove(grid, A[d], A[d+1]);
                    descenteElementsNonAteri = true;
                }
            }

            if( !descenteElementsNonAteri )
                setTimeout(after_time, temp);
            
            function after_time(){
                var ine:Boolean = false;
                for(var d=0; d<A.length; d+=2){
                    if( A[d] != null && !A[d].aligne && A[d].ateri && grid[A[d].col][A[d].row+1] == null && gameSprite.contains(A[d]) ){
                        /*Descendre*/
                        affectAbove(grid, A[d], A[d+1]);
                        ine = true;
                    }
                }
                if(ine){
                    nulMur( grid);
                }
            }

            if(A.length != 0)
                return true;
            return false;
        }
        
        /* replace les pion a leur emplacement */
        public function placeinit(grid:Array, col:int, row:int, offsetY:int=0){
            // Déplacement vers le bas
            if(grid[col][row]!=null && grid[col][row].y != grid[col][row].row*spacing+offsetY){
                grid[col][row].y = grid[col][row].row*spacing+offsetY;
                // Déplacement sur l'axe Y
            } 
            if (grid[col][row]!=null && grid[col][row].x != grid[col][row].col*spacing+offsetX){
                grid[col][row].x = grid[col][row].col*spacing+offsetX;
                // Déplacement sur l'axee X
            } 
        }
        
        //mur a ne pas faire
        /*====================================
    
                        Mur 

        =====================================*/

        /* Faire decendre objet ou il ya rien en dessous */
        public function nulMur( grid:Array ){
            var A:Array = tabMur(grid);
            var ine:Boolean = false;

            for(var d=0; d<A.length; d+=2){
                if( A[d] != null && !A[d].ateri ){
                    
                    /* Descendre */
                    affectAboveMur(grid, A[d], A[d+1]);
                    ine = true;
                }
            }
            if(ine)
                nul(grid);
        }

        /* Descendre les pion sur les place vide */
        public function affectAboveMur(grid:Array, piece:Element, nb_place_vide:int){
            for(var row:int=piece.row; row>=-5; row--) {
                if ( grid[piece.col][row] != null && !grid[piece.col][row].aligne && grid[piece.col][row+nb_place_vide] == null && grid[piece.col][row].mur ) {
                    grid[piece.col][row].row += nb_place_vide;
                    grid[piece.col][row + nb_place_vide] = grid[piece.col][row];
                    delete grid[piece.col][row];
                    grid[piece.col][row] = null;
                }
                else if( grid[piece.col][row] != null && !grid[piece.col][row].aligne && grid[piece.col][row+nb_place_vide] == null && !grid[piece.col][row].mur ){
                    grid[piece.col][row].ateri = false;
                }
                else{
                    return;
                }
            }
        }

        /**
         * Renvoi un tableau avec des pions et le nombre de place vide en dessous de ces pion
         */
        private function tabMur( grid:Array ):Array{
            var list:Array = new Array();
            
            for(var col=0; col<6; col++){
                for(var row:int=-8; row<11; row++) {
                    var arc:Array = marMur(grid, col, row);
                    
                    if(arc.length == 2){
                        /* arc[0]=Element, arc[1]=nb_place_vide en dessou de piece */
                        list.push(arc[0], arc[1]);          
                    }
                }   
            }
            return list;
        }


        /**
         * Renvoi un tableau avec un pion et le nombre de place vide en dessous de celle ci
         */
        private function marMur(grid:Array, col,row:int):Array{
            var ar:Array = new Array();
            var nb_place_vide:Number = 0;

            if ( grid[col][row] != null && !elementEnDessousDuMur(grid, grid[col][row]) && grid[col][row].mur && grid[col][row].can_tombe ){
                ar.push(grid[col][row]);
                grid[col][row].ateri = false;
                nb_place_vide = nombreDePlaceDessousMur(grid, grid[col][row]);
            }
            else{   
                ar.push(nb_place_vide);
                return ar;
            }

            ar.push(nb_place_vide);
            return ar;
        }

        /**
         * Verifie si il y'a des element sur la ligne dans dessous l'element fourni en parametre
         */
        private function elementEnDessousDuMur(grid:Array, element:Element):Boolean{
            var row:int = element.row;

            for(var col=0; col<6; col++){
                if( grid[col][row+1] != null ){
                    return true;
                }
            }

            return false;

        }

        private function nombreDePlaceDessousMur(grid:Array, element:Element):int{
            var nombre:int = 0;

            for(var row=element.row; row<12; row++){
                if( grid[0][row] != null && grid[0][row].mur && !elementEnDessousDuMur(grid, grid[0][row]) ){
                    nombre++;
                }
                else{
                    return nombre;
                }
                    
                //trace("nombre ", nombre);
            }

            return nombre;
        }
	
	}
}