package  classe.extension{
	
    import flash.utils.*;

    import com.adobe.nativeExtensions.Vibration;

    
	public class Vibrations{

        public static var  vibrationActiver:Boolean = true;

        public static function vibration(time:int):void{
            try{
                if( vibrationActiver && Vibration.isSupported ){
                    var vibre:Vibration = new Vibration;
                    vibre.vibrate(time);
                }
            }
            catch(e:Error){
                trace("Oh oh");
            }
        }

        public static function doubleVibration():void{
            try{
                if( vibrationActiver && Vibration.isSupported ){
                    var vibre:Vibration = new Vibration;
                    vibre.vibrate(100);

                    setTimeout(suit ,150);
                    
                    function suit():void{
                        vibre.vibrate(100);
                    }
                }
            }
            catch(e:Error){
                trace("Oh oh");
            }
        }
    }
}