package classe.gestionAttaque{


	public class Boue {

		private var _typeElement:int = 6; 
		private var _typeElementEau:int = 2;//supprime la boue
		private var _tab:Array;

		public function get tab():Array{
			return _tab;
		}

		public function Boue( grid:Array ){
			for( var col:int=0; col<6; col++ ){
				for( var row:int=0; row<12; row++ ){
					//TODO glace mur ? 10 verifier se nombre
					if( grid[col][row] != null && grid[col][row].type == _typeElement && _tab.indexOf(grid[col][row]) == -1){
						this.tab.push(grid[col][row]);
					}
				}
			}
		}

		/*Ajoute un element de boue au tableau de gestion */
		public function ajoutBoue( element:Element ):void{
			if( element != null ){
				this.tab.push(element);
			}
		}

		/*
			Ajoute les element de type boue au tableau qui apparaisse d'en bas
		*/
		public function ajoutBoueDansGrid( grid:Array ):void{
			for( var col:int=0; col<6; col++ ){
				for( var row:int=10; row<12; row++ ){
					//TODO glace mur ? 10 verifier se nombre
					if( grid[col][row] != null && grid[col][row].type == _typeElement && _tab.indexOf(grid[col][row]) == -1){
						this.tab.push(grid[col][row]);
					}
				}
			}
		}

		public function supressionElement(element:Element):void{
			//supprime l'element du tableau de boue si s'en est un
			if( _tab != null && _tab.indexOf(element) != -1 ){
				_tab[( _tab.indexOf(element) )] = null;
			}
		}
			
		public function compteARebour():void{
			for(var i:int=0; i<_tab.length; i++){
				if(_tab[i] != null){
					_tab[i].decrementRebour();
					var element:Element;
					if( _tab[i].getCompte_A_Rebour() == -1 ){
						for(var e:int=0; e<3; e++){
							element = _tab[i].choisiElement();
							_tab[i].attaqueElement(element);

							if( element )
								setTimeout(ajoutBoueAfterAttaque, 560, element);
						}
						_tab[i].initRebour(9);
					}
				}
			}
		}

		public function ajoutBoueAfterAttaque(element:Element):void{
			if( element ){
				element.boue = true;
				element.vitesseDeplacement( 0.1 );
			}
		}

		public function conditionDeSuppresion(grid:Array, col:Number,row:Number):Boolean{
			if( grid[col][row] != null && grid[col][row].boue ){
				if( col+1<=5 && grid[col+1][row] != null && grid[col+1][row].type == _typeElementEau && grid[col+1][row].aligne == true
					||col-1>=0 && grid[col-1][row] != null  && grid[col-1][row].type == _typeElementEau && grid[col-1][row].aligne == true
					||row+1<=11 && grid[col][row+1] != null && grid[col][row+1].type == _typeElementEau && grid[col][row+1].aligne == true 
					||row-1>=0 && grid[col][row-1] != null  && grid[col][row-1].type == _typeElementEau && grid[col][row-1].aligne == true 
				){
					return true;
				}
			}
			return false;
		}


		public function suppresion(element:Element):void{
			element.boue = false;
			element.vitesseDeplacement(0.3);
		}
		
	}

}