package classe.gestionAttaque{

	import classe.Glace;

	public class GlaceGestion {

		private var _typeElement:int = 6; 
		private var _typeElementFeu:int = 2;//supprime la boue
		private var _tab:Array;

		public function get tab():Array{
			return _tab;
		}

		public function GlaceGestion( grid:Array ){
			for( var col:int=0; col<6; col++ ){
				for( var row:int=0; row<12; row++ ){
					//TODO glace mur ? 10 verifier se nombre
					if( grid[col][row] != null && grid[col][row].type == _typeElement && _tab.indexOf(grid[col][row]) == -1){
						grid[col][row].initRebour(4);
						this.tab.push(grid[col][row]);
					}
				}
			}
		}

		/*Ajoute un element de boue au tableau de gestion */
		public function ajoutGlace( element:Element ):void{
			if( element != null ){
				this.tab.push(element);
			}
		}

		/*
			Ajoute les element de type boue au tableau qui apparaisse d'en bas
		*/
		public function ajoutGlaceDansGrid( grid:Array ):void{
			for( var col:int=0; col<6; col++ ){
				for( var row:int=10; row<12; row++ ){
					//TODO glace mur ? 10 verifier se nombre
					if( grid[col][row] != null && grid[col][row].type == _typeElement && _tab.indexOf(grid[col][row]) == -1){
						this.tab.push(grid[col][row]);
					}
				}
			}
		}

		public function supressionElement(element:Element):void{
			//supprime l'element du tableau de boue si s'en est un
			if( _tab != null && _tab.indexOf(element) != -1 ){
				_tab[( _tab.indexOf(element) )] = null;
			}
		}
			
		public function compteARebour():void{
			for(var i:int=0; i<_tab.length; i++){
				if(_tab[i] != null){
					_tab[i].decrementRebour();
					var element:Element;
					if( _tab[i].getCompte_A_Rebour() == -1 ){
						for(var e:int=0; e<3; e++){
							element = _tab[i].choisiElement();
							_tab[i].attaqueElement(element);

							if( element )
								setTimeout(ajoutGlaceAfterAttaque, 560, element);
						}
						_tab[i].initRebour(9);
					}
				}
			}
		}

		public function ajoutGlaceAfterAttaque(element:Element):void{
			if( element )
				element.addGlace(new Glace(element.tailleDorigine));
		}

		public function conditionDeSuppresion(grid:Array, col:Number,row:Number):Boolean{
			if( grid[col][row] != null && grid[col][row].boue ){
				if( col+1<=5 && grid[col+1][row] != null && grid[col+1][row].type == _typeElementFeu && grid[col+1][row].aligne == true
					||col-1>=0 && grid[col-1][row] != null  && grid[col-1][row].type == _typeElementFeu && grid[col-1][row].aligne == true
					||row+1<=11 && grid[col][row+1] != null && grid[col][row+1].type == _typeElementFeu && grid[col][row+1].aligne == true 
					||row-1>=0 && grid[col][row-1] != null  && grid[col][row-1].type == _typeElementFeu && grid[col][row-1].aligne == true 
				){
					return true;
				}
			}
			return false;
		}


		public function suppresion(element:Element):void{
			if( element != null && element.glace != null && element.glace_delete != true ){
				element.glace_delete = true;
				var obj = element;
				
				//var tweenAlpha = new Tween(element.glace, "alpha", Regular.easeOut, 1, 0.1, 2, true);
				var tweenScaleY = new Tween(element.glace, "scaleY", Regular.easeOut, 1, 0, 1, true);
				var tweenScaleX = new Tween(element.glace, "scaleX", Regular.easeOut, 1, 0, 1, true);
				setTimeout(supressionElement, 1100, obj);
				
				tweenScaleY.addEventListener(TweenEvent.MOTION_FINISH, onFinish2);
				
				function onFinish2(evt:TweenEvent):void{
					obj.can_elimine = true;
					obj.can_deplace = true;
					obj.can_select = true;
					obj.removeChild( obj.glace );
					obj.glace = null;
				}
			}
		}


		public function addGer(nb_seconde_temps:Number): void{
			nb_seconde_temps--;
			minutes  = int(nb_seconde_temps / 60);
			secondes = int(nb_seconde_temps % 60);

			//Glace
			for( var col:int=0; col<6; col++ ){
				for( var row:int=10; row<12; row++ ){
					//TODO glace mur ? 10 verifier se nombre
					if( grid[col][row] != null && grid[col][row].type == 6 && _tab.indexOf(grid[col][row]) == -1){
						_tab.push(grid[col][row]);
					}
				}
			}

			for(var i:int=0; i<_tab.length; i++){
				if(_tab[i] != null){
					_tab[i].decrementRebour();
					var element:Element;
					if( _tab[i].getCompte_A_Rebour() == -1 ){
						for(var e:int=0; e<3; e++){
							element = _tab[i].choisiElement();
							_tab[i].attaqueElement(element);

							if( element )
								setTimeout(ajoutGlaceAfterAttaque, 560, element);
						}
						_tab[i].initRebour(9);
					}
				}
			}
		}
		
		
	}

}