﻿package classe.main{
	
    import flash.ui.MultitouchInputMode;
	import flash.ui.Multitouch;
	import flash.display.Loader;
	import flash.net.URLRequest;
    import flash.display.Stage;
    import flash.display.MovieClip; 
	import flash.display.StageAlign; 
	import flash.display.StageScaleMode; 
	import flash.events.Event; 
	import flash.events.*;
	import flash.utils.*;
	import flash.display.Sprite;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.media.Sound; 
	import flash.net.SharedObject;
 	import fl.transitions.*;
	import flash.display.*;
	import flash.geom.*;
	import flash.ui.*;

	import fl.motion.Color;
	import flash.utils.Timer;

	import classe.Effect;
	import classe.nivo_menu;
	import classe.Objectifs;
	import flash.net.SharedObject;
	
	public class CarteMain extends MainMain {

		private var sceneWidth:Number = 400;
		private var sceneHeight:Number = 700;

		public function CarteMain(varSav:SharedObject){

			var objectifs:Objectifs = new Objectifs();

			var sound:plop_n0_2 = new plop_n0_2;
			var son_active:Boolean = true;

			var effet:Effect = new Effect();
			effet.set_rayon(50);

			var mapSprite:Sprite = new Sprite();

			var nivo_selectionner:Object;

			/*Cree le Menu nivo*/
			var menu_nivo:MovieClip = new nivo_menu();
			menu_nivo.width = sceneWidth-80;
			menu_nivo.scaleY = menu_nivo.scaleX ;
			menu_nivo.y = -(menu_nivo.height+20);
			menu_nivo.x = (sceneWidth/2)-(menu_nivo.width/2);

			/*Tween pour animation menu*/
			var twendep:Tween;
			var twenn_alpha:Tween;

			var mouseDown = false;
			var num_niv:int;

			var posi_mouse_map:Number = 0;

			var tab_map:Array = new Array();

			var square:Shape = new Shape; 
			square.graphics.beginFill(0x000000); 
			square.graphics.drawRect(0, 0, sceneWidth, sceneHeight);
			square.alpha = 0.7;

			/*
				Menu
			*/
			function descente_menu(){
				var posi_new_y = (stage.stageHeight/2)-(menu_nivo.height/2);
				twendep = new Tween(menu_nivo, "y", Strong.easeOut, menu_nivo.y, posi_new_y, 0.5, true);
				menu_nivo.descendu = true;
				menu_nivo.tuto.alpha = 0;
				
				fond_Sprite.addChild(square);
				square.width = stage.stageWidth;
				fond_Sprite.setChildIndex(square, fond_Sprite.numChildren-1);
				fond_Sprite.setChildIndex(menu_nivo, fond_Sprite.numChildren-1);
			}

			function monter_menu(){
				var posi_new_y = -(menu_nivo.height+20);
				twendep = new Tween(menu_nivo, "y", Strong.easeIn, menu_nivo.y, posi_new_y, 0.5, true);
				menu_nivo.descendu = false;
				fond_Sprite.removeChild(square);
			}
			/*
				*/

			/*Fond de la map*/
			var fond_Sprite:Sprite = new Sprite();
			fond_Sprite.graphics.lineStyle(5, 0x3393A6, 1); 
			fond_Sprite.graphics.beginFill(0x3399CC); 
			fond_Sprite.graphics.drawRect(0, 0, sceneWidth, sceneHeight); 
			fond_Sprite.graphics.endFill();
			//fond_Sprite.blendMode = "overlay";
			fond_Sprite.x = fond_Sprite.y = 0;
			addChild(fond_Sprite);

			color = 0x2F5C80;

			var request:URLRequest = new URLRequest("ImageA/fond_2.png"); 
			var loader:Loader = new Loader();

			loader.load(request); 
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, pict_is_loaded)

			var fond;

			function pict_is_loaded (e:Event) {
				fond = loader.content;
				fond.x = fond.y = 0;
				fond.width = sceneWidth+80;
				fond.scaleY = fond.scaleX;
				fond_Sprite.addChild(fond);
				fond_Sprite.setChildIndex(fond, 0);

				if( stage != null ){
					stage.align = StageAlign.TOP_LEFT;
					//stage.scaleMode = "exactFit";
					stage.scaleMode = StageScaleMode.SHOW_ALL;
				}
				else{
					addEventListener(Event.ADDED_TO_STAGE, init);
				}
			}

			function init(e:Event):void{
				removeEventListener(Event.ADDED_TO_STAGE, init);
				stage.align = StageAlign.TOP_LEFT;
				//stage.scaleMode = "exactFit";
				stage.scaleMode = StageScaleMode.SHOW_ALL;
				//inite();
			}


			/*Ajouter un nivo au Sprite*/
			function add_mapSprite(mapSprite:Sprite, x,y:Number, numero_nivo:int):nivo{
				var choix_nivo:nivo = new nivo();
				choix_nivo.x = x;
				choix_nivo.y = y;
				choix_nivo.num_nivo = numero_nivo;
				
				num_niv:int;

				var VarSave:SharedObject = varSav;
				
				//trace(VarSave.data.nivo_map+" savee");
				if(VarSave != null && VarSave.data.nivo_map != null)
					num_niv = VarSave.data.nivo_map;
				else
					num_niv = 1;

				if( choix_nivo.num_nivo <= num_niv ){
					choix_nivo.acceder = true;
				}
				else
					choix_nivo.acceder = true;
				
				//trace(choix_nivo.txnnt);
				if( !choix_nivo.acceder )
					choix_nivo.alpha = 0.3;
				
				choix_nivo.txt_num_niv.text = String(numero_nivo);
				
				if(numero_nivo == 0)
					choix_nivo.txt_num_niv.text = "Tuto";
				
				choix_nivo.width = 50;
				choix_nivo.scaleY = choix_nivo.scaleX;
				choix_nivo.inf_nivo = 0;
				choix_nivo.txt_num_niv.mouseEnabled = false;
				
				choix_nivo.addChild(choix_nivo.nb_etoile);
				choix_nivo.nb_etoile.mouseEnabled = false;
				
				objectifs.objectifs(numero_nivo);
				
				if(VarSave != null && VarSave.data.nivo != null && VarSave.data.nivo[numero_nivo] != null && VarSave.data.nivo[numero_nivo].score != null){
					if( VarSave.data.nivo[numero_nivo].score >= 1.7*objectifs.getScore()){
						choix_nivo.nb_etoile.gotoAndStop(3);
						choix_nivo.nb_etoiles = 3;
					}
					else if( VarSave.data.nivo[numero_nivo].score >= 1.3*objectifs.getScore()){	
						choix_nivo.nb_etoile.gotoAndStop(2);
						choix_nivo.nb_etoiles = 2;
					}
					else if( VarSave.data.nivo[numero_nivo].score >= objectifs.getScore()){	
						choix_nivo.nb_etoile.gotoAndStop(1);
						choix_nivo.nb_etoiles = 1;
					}
					else{
						choix_nivo.nb_etoile.gotoAndStop(4);
						choix_nivo.nb_etoiles = 0;
					}
					//trace(VarSave.data.nivo[1].score+" savee");
				}
				else
					choix_nivo.nb_etoile.gotoAndStop(4);
				
				mapSprite.addChild(choix_nivo);
				
				choix_nivo.addEventListener(Event.ENTER_FRAME, deforme);

				return choix_nivo;
			}


			function deforme(evt:Event){
				if(fond_Sprite.contains(mapSprite) && evt.target.alpha != 0 &&  evt.target.y+(mapSprite.y) > 545 && evt.target.y+(mapSprite.y) < stage.stageHeight){
					//trace(" evt.target.y "+evt.target.y);
					evt.target.inf_nivo += 0.4;
					evt.target.height = 50+Math.sin(evt.target.inf_nivo)*2;
					evt.target.width = 50+Math.cos(evt.target.inf_nivo)*2;
				}
			}

			/*Faire bouger la map*/
			function move_map_sprite(mapSprite:Sprite){
				if(mapSprite.y < 100 && mouseDown == true && mapSprite.y >= -(mapSprite.height-600+40) ){
					mapSprite.y -= (mapSprite.y - (mouseY-posi_mouse_map))*0.6;
					
					//posi_mouse_map = mapSprite.mouseY;
					//trace("mapSprite.y xxx "+mapSprite.y)
				}
				if(mapSprite.y >= 50 && mouseDown == false){
					mapSprite.y -= Math.ceil(Math.abs(49-mapSprite.y)/10);
					posi_mouse_map = mapSprite.mouseY;
					//trace("mapSprite.y kkk "+mapSprite.y);
				}
				else if( mapSprite.y <= -(mapSprite.height-600) && mouseDown == false ){
					mapSprite.y += Math.abs(((mapSprite.height-600)-1)+mapSprite.y)/10;
					posi_mouse_map = mapSprite.mouseY;
					//trace("mapSprite.y ooo "+mapSprite.y);
				}
			}

			function mouse_down_map(evt:MouseEvent){
				//trace("mouse_down_map "+evt.target.name);
				posi_mouse_map = mapSprite.mouseY;
				mouseDown = true;
				//trace("evt.target " + evt.target.parent.parent + evt.target.name);

				
				//trace(mapSprite.height)
				//trace("posi_mouse_map "+posi_mouse_map);
			}

			function click(evt:MouseEvent){
				
				if( evt.target is nivo || evt.target.parent is nivo || evt.target.parent.parent is nivo){
					/*Pour les objet enfant qui son en 1er plan de la bulle*/
					var target;
					if( evt.target is nivo ){
						target = evt.target;
					}
					else if(evt.target.parent is nivo){
						target = evt.target.parent;
					}
					else{
						target = evt.target.parent.parent;
					}
					if( target.num_nivo != 0 && target.acceder ){
						if( twendep == null || (!twendep.isPlaying && menu_nivo.descendu != true) ){
							
							/*Initialise les Objectif a null*/
							menu_nivo.del_object_dic();
							
							/*Initialise les Objectif selon le numero du nivo*/
							objectifs.objectifs(target.num_nivo);
							
							/*Affichage Score a atteindre*/
							if(objectifs.getScore() != null){
								menu_nivo.objectif_text.text = "Objectif: " + String(objectifs.getScore());
							}
							else{
								menu_nivo.objectif_text.text = "";
							}
							
							/*Initialise les Objectif*/
							menu_nivo.inite_object_dic(objectifs);
							
							if(target.nb_etoiles >= 1 && target.nb_etoiles <= 3)
								menu_nivo.gotoAndStop(target.nb_etoiles+1);
							else{
								menu_nivo.gotoAndStop(1);
							}
							
							/**/
							descente_menu();
							
							/*Disparition de la bulle de selection
							  Et effet, plus affichage du numero du nivo
							 */
							target.alpha = 0;
							nivo_selectionner = target;
							
							if(son_active)
								sound.play();
							
							effet.effet3_frame(mapSprite, nivo_selectionner.x, nivo_selectionner.y, Math.ceil(Math.random()*5+4), 2, Math.random()*10+7 );
							menu_nivo.num_niv.text = "Niveau " + String(target.num_nivo);
						}
						
						//trace(evt.target.num_nivo);
						num_niv = target.num_nivo;
					}
					else if(target.num_nivo == 0 && target.acceder){
						maVar = "tuto";
						fond_Sprite.removeChild(Sprite_Eau);
					}
					if( target.num_nivo == 5 ){
						menu_nivo.tuto.alpha = 1;
					}
				}
				
				
				//
				if( evt.target.name == "tuto" ){
					varNvxTuto = 16;
					maVar = "tuto";

					fond_Sprite.removeChild(Sprite_Eau);
				}
				if( evt.target.name != "jouer" && twendep != null  && !twendep.isPlaying && menu_nivo.descendu == true ){
					monter_menu();
					
					/*Animation de la Bulle selectionner precedement
					  Apparition
					 */
					var twenn_height = new Tween(nivo_selectionner, "height", Bounce.easeOut, 0, 50, 0.5, true);
					var twenn_width = new Tween(nivo_selectionner, "width", Bounce.easeOut, 0, 50, 0.5, true);
					twenn_alpha = new Tween(nivo_selectionner, "alpha", Regular.easeOut, 0, 1, 0.5, true);
					nivo_selectionner = null;
				}
				else if( evt.target.name == "jouer" ){
					/*Supression des ecouteur pour liberer la memoire*/
					fond_Sprite.removeEventListener(MouseEvent.MOUSE_DOWN, mouse_down_map);
					fond_Sprite.removeEventListener(MouseEvent.MOUSE_UP, mouse_up_map);
					fond_Sprite.removeEventListener(Event.ENTER_FRAME, moveThatMouse_map);
					removeEventListener(Event.ENTER_FRAME, tracee);
					
					/*Changement de la variable du SWF Parent
					  Communication avec le Parent
					 */
					//trace("jouer "+thise+" "+thise.parent.parent);
					maVar = "jouer_map";
					maVarNivo = int(num_niv);
					clearInterval(Inter);

					for(var i=0; i<tab_map.length; i++){
						tab_map[i].removeEventListener(Event.ENTER_FRAME, deforme);
						mapSprite.removeChild(tab_map[i]);
						tab_map[i] = null;
					}
					
					fond_Sprite.removeChild(Sprite_Eau);
					fond_Sprite.removeChild(menu_nivo);
					fond_Sprite.removeChild(mapSprite);
					removeChild(fond_Sprite);
					
					Sprite_Eau = null;
					fond_Sprite = null;
					menu_nivo = null;
					mapSprite = null;
					
				}
				else if( evt.target.name == "btn_exit" && twendep != null  && !twendep.isPlaying && menu_nivo.descendu == true ){
					monter_menu();
				}
				
				
				/*Mettre le menu au premier Plan*/
				if(fond_Sprite != null )
					fond_Sprite.setChildIndex(menu_nivo, fond_Sprite.numChildren-1);
			}

			var maskeTrajet:Sprite = new Sprite;

			/*Principale*/
			function main(){
				
				if( thise != null && thise.parent != null && thise.parent.parent != null ){
					son_active = thise.parent.parent["son_active"];
				}
				
				var trajetSprite:Sprite = new Sprite;
				/*Ajouts des nivaux*/
				var posi_x = 0;
				for(var i=10; i>=0; i--){
					posi_x = Math.cos(((Math.PI)/2)*i);
					var ps = Math.cos(((Math.PI)/2)*(i-1));
					
					var ligne:Shape = new Shape();
					
					trajetSprite.addChild(ligne);
					
					tab_map.push( add_mapSprite(mapSprite, 200 + posi_x * 60, 60*i, 10-i) );
					
					if( 10-i+1 <= num_niv ){
						//ligne.graphics.lineStyle(6, 0xDD6666, .9);
						ligne.graphics.lineStyle(2, 0x00CCCC, .5);
					}
					else
						ligne.graphics.lineStyle(2, 0x00CCCC, .5);	

					ligne.graphics.moveTo((200+posi_x*60), (60*i));
					ligne.graphics.lineTo((200+ps*60), (60*(i-1)));
					
				}
				
				maskeTrajet.graphics.beginFill(0x000000);
				maskeTrajet.graphics.drawRect(0, stage.stageHeight, stage.stageWidth, -100+(-60 * num_niv));
				mapSprite.addChild(maskeTrajet);
				mapSprite.addChild(trajetSprite);
				mapSprite.setChildIndex(trajetSprite, 0);
				
				trajetSprite.mask = maskeTrajet;
				var tweenTrajet:Tween = new Tween(maskeTrajet, "scaleY", Bounce.easeOut, 0, 1, 3, true);
				
				//mapSprite.mask = maskeTrajet;
				/*Evenement Click souris*/
				fond_Sprite.addEventListener(MouseEvent.MOUSE_DOWN, mouse_down_map);
				
				fond_Sprite.addEventListener(MouseEvent.CLICK, click);
				
				/*Releachement souris*/
				fond_Sprite.addEventListener(MouseEvent.MOUSE_UP, mouse_up_map);

				fond_Sprite.addEventListener(Event.ENTER_FRAME, moveThatMouse_map);
				
				mapSprite.y = -(mapSprite.height-600);
				//trace(mapSprite.y);
				//trace(" mapSprite.height "+mapSprite.height);
				fond_Sprite.addChild(menu_nivo);
				fond_Sprite.addChild(mapSprite);
				
			}

			/*Evenement Frame*/
			function moveThatMouse_map(evt:Event){
				move_map_sprite(mapSprite);
			}

			function mouse_up_map(event:MouseEvent):void
			{
				mouseDown = false;
				if( Math.abs( mapSprite.y - (mouseY-posi_mouse_map) ) > 2 ){
					
					//trace("posi_mouse_map " + posi_mouse_map);
					posi_mouse_map += 3;
					//trace("posi_mouse_map " + posi_mouse_map);
					
					var posi_mousey = mouseY;
					addEventListener(Event.ENTER_FRAME, avance);
					function avance(){
						if( Math.abs( mapSprite.y - (posi_mousey-posi_mouse_map) ) < 2 || mouseDown ){
							removeEventListener(Event.ENTER_FRAME, avance);
						}
						else{
							mapSprite.y -= (mapSprite.y - (posi_mousey-posi_mouse_map))*0.2;
						}
					}
				}
			}

			//
			var thise;

			/*Cree le this*/
			if( stage ){
				main();
			}
			else{
				addEventListener(Event.ADDED_TO_STAGE, inite);
			}

			function inite(evt:Event){
				//trace(this+" "+fond_Sprite);
				thise = this;
				removeEventListener(Event.ADDED_TO_STAGE, inite);
				main();
			};


			/****************************

						Eau

			*****************************/

			import flash.events.MouseEvent;
			import flash.display.Sprite;
			import flash.display.Shape;
			import flash.display.MovieClip;

			var inf = 0;
			var inf2 = 0;

			var tab:Array = new Array();
			var tab2:Array = new Array();
			var Sprite_Eau:Sprite = new Sprite();
			Sprite_Eau.mouseEnabled = false;

			var hauteur_vague = 1;
			var haut_teur_eau = 1;

			var color:uint;

			var posy_eau = sceneHeight;
			var vitesse_dessin_eau = 0;
			var rect_mask:Shape = new Shape();

			rect_mask.graphics.beginFill(color);
			rect_mask.graphics.drawRect(0, posy_eau, sceneWidth, -600);

			addEventListener(Event.ENTER_FRAME, tracee);

			function tracee(evt:Event){
				var type:String = GradientType.LINEAR;
				var colors:Array = [0x00CCFF, 0x001188]; 
				var alphas:Array = [1, 1]; 
				var ratios:Array = [0, 255]; 
				var spreadMethod:String = SpreadMethod.PAD; 
				var interp:String = InterpolationMethod.LINEAR_RGB; 
				var focalPtRatio:Number = 0; 
				 
				if( vitesse_dessin_eau % 2 == 0 ){
					var ligne:Shape = new Shape(); 
					
					var ligneLimiteEau:Shape  = new Shape();
					var arrayPoint:Array = new Array();
					var colRo = 0x33DDCC;
					
					ligne.graphics.lineStyle(3, colRo, 1, false, "normal", CapsStyle.SQUARE);	
					
					/* Placement Point */
					for(var e=0; e<205/6; e++){
						var point:Point = new Point;
						point.x = e*12;
						point.y = posy_eau - 300 + haut_teur_eau + Math.sin((e + inf * 30)/10) * hauteur_vague;
						
						arrayPoint.push(point);
					}
					
					/* Traçage */
					for(e=0; e<arrayPoint.length-1; e++){
						var matrix:Matrix = new Matrix(); 
						var boxWidth:Number = arrayPoint[e+1].x - arrayPoint[e].x; 
						var boxHeight:Number = posy_eau - 430 - haut_teur_eau; 
						var boxRotation:Number = Math.PI/2; // 90° 
						var tx:Number = 0; 
						var ty:Number = arrayPoint[e].y; 
						matrix.createGradientBox(boxWidth, boxHeight, boxRotation, tx, ty); 

						/* Eau */
						ligneLimiteEau.graphics.beginGradientFill(type,  
			                            colors, 
			                            alphas, 
			                            ratios,  
			                            matrix,  
			                            spreadMethod,  
			                            interp,  
			                            focalPtRatio);
						ligneLimiteEau.graphics.moveTo(arrayPoint[e].x  , arrayPoint[e].y);
						ligneLimiteEau.graphics.lineTo(arrayPoint[e].x  , posy_eau);
						ligneLimiteEau.graphics.lineTo(arrayPoint[e+1].x  , posy_eau);
						ligneLimiteEau.graphics.lineTo(arrayPoint[e+1].x  , arrayPoint[e+1].y);
						ligneLimiteEau.graphics.lineTo(arrayPoint[e].x  , arrayPoint[e].y);
						ligneLimiteEau.graphics.endFill();
						
						/* Ligne de bord */
						ligne.graphics.moveTo(arrayPoint[e].x   , arrayPoint[e].y);
						ligne.graphics.lineTo(arrayPoint[e+1].x , arrayPoint[e+1].y);
					}
					
					ligneLimiteEau.alpha = 0.5;
					//ligne.blendMode = "hardlight";
					
					tab.push(ligne);
					tab2.push(ligneLimiteEau);
					
					Sprite_Eau.addChild(tab[tab.length-1]);
					Sprite_Eau.addChild(tab2[tab2.length-1]);
					
					for( e=0; e<tab.length-1; e++){
						if(tab[e] != null){
							Sprite_Eau.removeChild(tab[e]);
							tab[e] = null;
						}
					}

					/* ligne limite Eau */
					for( e=0; e<tab2.length-1; e++){
						if(tab2[e] != null){
							Sprite_Eau.removeChild(tab2[e]);
							tab2[e] = null;
						}
					}
					
					fond_Sprite.addChild(Sprite_Eau);
					fond_Sprite.setChildIndex(Sprite_Eau, fond_Sprite.numChildren-1);
					
					if(tab.length-1 == 2){
						tab = [ tab[tab.length-1] ];
					}
					
					if(tab2.length-1 == 2){
						tab2 = [ tab2[tab2.length-1] ];
					}

					hauteur_vague = Math.sin(10)*18;
					haut_teur_eau = Math.sin(inf2)*160;
						
					inf  += 0.1;
					inf2 += 0.05;
				}
				vitesse_dessin_eau++;
			}

			fond_Sprite.addChild(Sprite_Eau);
			Sprite_Eau.mouseEnabled = false;
			Sprite_Eau.mask = rect_mask;
			addChild(rect_mask);

			/******************************

						Bulles

			*******************************/

			import classe.Bulles;

			generer_bulle(Math.random()*3+2);
			var Inter = setInterval(generer_bulle, 10000, Math.ceil(Math.random()*3+2));

			function generer_bulle(nb_bulle:int){
				for(var i=0; i<nb_bulle; i++){
					var bul:Bulles = new Bulles();
					bul.Bulle(Math.random()*300+10, 750, Math.random()*15+10, 1, posy_eau - 250, 70);
					fond_Sprite.addChild(bul);
					fond_Sprite.setChildIndex(bul, 1);
				}
			}
		}
	}
}