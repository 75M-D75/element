package classe.main {
	import flash.events.Event;
	import fl.transitions.TweenEvent;
	import fl.transitions.Tween;
	import flash.text.TextField;
	import flash.display.SimpleButton;
	import flash.display.MovieClip;
	import fl.transitions.easing.*;


	import classe.*;
	
	public class IntroMain extends MainMain {
		
		public function IntroMain(){
		
			var twenn:Array = new Array();
			var fini:Boolean = false;
			var introAnime:IntroAnime = new IntroAnime();
			var infinity:MovieClip = introAnime.infinity;
			var infinity_atom:SimpleButton = introAnime.infinity_atom;
			var presente:TextField = introAnime.presente;
			var element:SimpleButton = introAnime.Element;
			addChild(introAnime);

			infinity.y = -100;
			presente.x = 500;
			infinity_atom.x = -200;
			element.alpha = 0;

			infinity.mouseEnabled = false;
			infinity_atom.mouseEnabled = false;
			presente.mouseEnabled = false;
			element.mouseEnabled = false;

			function anime_intro():void{
				twenn.push( new Tween(infinity, "y", Bounce.easeOut, -100, 180, 0.6, true));
				
				twenn[twenn.length-1].addEventListener(TweenEvent.MOTION_FINISH, suit_1);
			}

			function suit_1(evt:TweenEvent):void{
				twenn.push( new Tween(infinity_atom, "x", Bounce.easeOut, -200, 135, 0.5, true));
				twenn.push( new Tween(presente, "x", Bounce.easeOut, 500, 138, 0.5, true));
				
				twenn[twenn.length-1].addEventListener(TweenEvent.MOTION_FINISH, suit_2);
			}

			function suit_2(evt:TweenEvent){
				twenn.push( new Tween(element, "alpha", Regular.easeIn, 0, 1, 1, true));
				
				twenn[twenn.length-1].addEventListener(TweenEvent.MOTION_FINISH, is_fini);
			}

			function is_fini(evt:TweenEvent):void{
				fini = true;
			}

			this.addEventListener(Event.ENTER_FRAME, lance_intro);

			function lance_intro(evt:Event):void{
				if(contains(infinity)){
					removeEventListener(Event.ENTER_FRAME, lance_intro);
					anime_intro();
				}
			}

		}
	}
}