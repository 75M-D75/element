package classe.main{
	
	import flash.display.Sprite;
	import flash.net.SharedObject;

	public class MainMain extends Sprite {
		var maVar:String;
		var maVarNivo:int = -1;
		var varSave:SharedObject;
		var varNvxTuto:int;

		public function MainMain(maVar:String=null, varSave:SharedObject=null){
			this.maVar   = maVar;
			this.varSave = varSave;
		}

		public function getMaVar() :String{
			return maVar;
		}

		public function getMaVarNivo():int {
			return maVarNivo;
		}

		public function getVarSave():SharedObject{
			return varSave;
		}
	}
}