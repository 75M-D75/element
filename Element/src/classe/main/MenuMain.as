package classe.main{
	
    import flash.display.DisplayObject;
    import flash.ui.MultitouchInputMode;

	import flash.ui.Multitouch;
	import flash.display.Loader;
	import flash.net.URLRequest;
    import flash.display.Stage;
    import flash.display.MovieClip; 
	import flash.display.StageAlign; 
	import flash.display.StageScaleMode; 
	import flash.events.Event; 
	import flash.events.*;
	import flash.utils.*;
	import flash.display.Sprite;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.media.Sound; 
	import flash.net.SharedObject;
	
	import classe.Neige;
	
	public class MenuMain extends MainMain {

		private var sceneWidth:Number = 400;
		private var sceneHeight:Number = 700;

		public function MenuMain(varSave:SharedObject = null){
			//Initialisation Menu Info
			var text_info:MovieClip = new Info();
			text_info.height = sceneHeight;
			text_info.width = sceneWidth;
			text_info.non_visible = true;

			//Initialisation Menu Reglage (Parametre)
			var reglage:Reglage = new Reglage();
			reglage.non_visible = true;
			reglage.height = sceneHeight;
			reglage.width  = sceneWidth;
			reglage.x = sceneWidth/2;
			reglage.y = sceneHeight/2;

			var menuSprite:Sprite = new Sprite();
			var menuPrincipale:MenuPrincipale = new MenuPrincipale();
			var jouer = menuPrincipale.jouer;
			var logo = menuPrincipale.logo;
			var parametre = menuPrincipale.parametre;
			var btn_info = menuPrincipale.btn_info;

			var neige:Neige;
			var save:SharedObject = varSave;
			var twenn_X:Tween;
			var neig_interval:uint = setInterval(ajout_neige, 2000);
			var neig_Sprite:Sprite = new Sprite()
			addChild(neig_Sprite)

			var tab_neig:Array = new Array();

			//fond ecran
			var request:URLRequest = new URLRequest("ImageA/fond_3.jpg"); 
			var loader:Loader = new Loader() 

			//lancement d'affichage de l'image
			setTimeout(launch, 1000);
			menuSprite.x = (sceneWidth/2)-(menuPrincipale.width/2);
			menuSprite.y = sceneHeight * 0.2;

			// Manage the event that picture is loaded
			function launch(){
				loader.load(request); 
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, pict_is_loaded)
			}

			var pict:DisplayObject;
			// charge le fond ecran ...
			function pict_is_loaded (e:Event) :void{
				pict = loader.content;
				pict.width = sceneWidth+80;
				pict.scaleY = pict.scaleX;
				pict.x = 0;
				pict.y = 0;
			 	addChild(pict);

				/*initialisation stage*/
				if( stage != null ){
					inite();
				}
				else{
					addEventListener(Event.ADDED_TO_STAGE, init);
				}
				setChildIndex(pict, 0);
				//trace("load")
			}

			function init(e:Event):void{
				removeEventListener(Event.ADDED_TO_STAGE, init);
				inite();
			}

			//initialisation du menu, en fonction des donné sauvegarder
			function inite():void{
				save = varSave;
				if(save.size != 0 && save.data.reglage != undefined && save.data.reglage.song != undefined && save.data.reglage.song == true){
					reglage.btn_reglage_son.active();
				}
				else if(save.size != 0 && save.data.reglage != undefined && save.data.reglage.song != undefined && !save.data.reglage.song ){
					reglage.btn_reglage_son.desactive();
				}
				else{
					reglage.btn_reglage_son.active();
				}
				
				if( save.size != 0 && save.data.reglage != undefined && save.data.reglage.vibration != undefined && save.data.reglage.vibration == true ){
					reglage.btn_reglage_vibration.active();
				}
				else if( save.size != 0 && save.data.reglage != undefined && save.data.reglage.vibration != undefined && !save.data.reglage.vibration ){
					reglage.btn_reglage_vibration.desactive();
				}
				else{
					reglage.btn_reglage_vibration.active();
				}
				
				if( save.size != 0 && save.data.reglage != undefined && save.data.reglage.effet_son != undefined && save.data.reglage.effet_son == true ){
					reglage.btn_reglage_effetSon.active();
				}
				else if( save.size != 0 && save.data.reglage != undefined && save.data.reglage.effet_son != undefined && !save.data.reglage.effet_son ){
					reglage.btn_reglage_effetSon.desactive();
				}
				else{
					reglage.btn_reglage_effetSon.active();
				}
				
				stage.align = StageAlign.TOP_LEFT;
				//stage.scaleMode = "exactFit";
				stage.scaleMode = StageScaleMode.SHOW_ALL;
				setChildIndex(neig_Sprite, numChildren-1);
				pict.height = stage.stageHeight;
				pict.scaleX = pict.scaleY;
				//
				stage.addEventListener(MouseEvent.CLICK, itemButtons);
			}

			

			menuSprite.addChild(jouer);
			menuSprite.addChild(logo);
			menuSprite.addChild(parametre);
			menuSprite.addChild(btn_info);
			addChild(menuSprite);

			if( Math.ceil(Math.random( )*3) == 1 ) {
				Neige.choisirSense(1);
				trace("Sense neige 1");
			}
			else{
				Neige.choisirSense(-1);
				trace("Sense neige -1");
			}

			function ajout_neige():void{
				var colore:uint;	
				colore = 0xFFFFFF;
				
				for( var i:int=0; i<2+Math.random()*4; i++){
					neige = new Neige(-100 + Math.random() * 600, -20);
					neige.graphics.beginFill(colore); 
					neige.graphics.drawCircle(0, 0, 10);
					neige.graphics.endFill();
					tab_neig.push(neige);
					neig_Sprite.addChild(neige);
				}
				
				while( tab_neig.length > 50 ){
					
					neige = tab_neig.shift();
					
					if( neige.y > sceneHeight ){
						neig_Sprite.removeChild(neige);
						neige.kill();
						neige = null;
					}
				}
			}

			//Animation de l'apparition des menus (Reglage et Info)
			function apparitionMenu(obj:MovieClip, x_dep,x_fin:int):void{
				obj.alpha = 1;
				obj.x = x_dep;
				obj.y = sceneHeight/2;
				obj.non_visible = false;
				addChild(obj);
				setChildIndex(obj, numChildren-1);
				
				twenn_X = new Tween(obj, "x", Bounce.easeOut, x_dep, x_fin, 1, true);
			}

			//Animation de la sortie des menus (Reglage et Info)
			function sortiMenu(obj:MovieClip, x_dep,x_fin:int):void{
				twenn_X = new Tween(obj, "x", Regular.easeOut, 200, -reglage.width, 0.4, true);
				
				twenn_X.addEventListener(TweenEvent.MOTION_FINISH, end);
				
				function end(){
					obj.non_visible = true;
					removeChild(obj);
				}
			}


			//Action à effectuer lors d'un click sur un bouton
			function itemButtons(evt:MouseEvent):void {
				//trace(evt.target is SimpleButton)
				var bip:sn_btn_bip = new sn_btn_bip;
				switch (evt.target.name) {
					case "jouer" :
						bip.play();
					
						maVar = "jouer";
						stage.removeEventListener(MouseEvent.CLICK, itemButtons);
						clearInterval(neig_interval);
						break ;
					case "btn_info" :
						
						bip.play();
					
						/*Code*/
						if( twenn_X == null || (text_info.non_visible && !twenn_X.isPlaying) ){
							apparitionMenu(text_info, -420, sceneWidth/2);
							
						}
						else if( !reglage.non_visible && twenn_X != null && !twenn_X.isPlaying ){
							sortiMenu(text_info, sceneWidth/2, -420);
						}
						break;
					case "parametre" :
						bip.play();
					
						if( twenn_X == null || (reglage.non_visible && !twenn_X.isPlaying) ){
							apparitionMenu(reglage, -420, sceneWidth/2);
							trace("reglage ", reglage.x, " wid ", reglage.width)
							trace(stage.stageWidth,"  stage.stageWidth")
						}
						else if( !reglage.non_visible && twenn_X != null && !twenn_X.isPlaying ){
							sortiMenu(reglage, sceneWidth/2, -420);
							trace("reglage ", reglage.x, " wid ", reglage.width)
						}
						break;
					case "btn_reglage_son" :
						if( evt.target.getActiver() ){
							evt.target.desactive();
							if(save.data.reglage == null || save.data.reglage == undefined)
								save.data.reglage = new Object;
							save.data.reglage.song = false;
							save.flush();
						}
						else if( !evt.target.getActiver() ){
							evt.target.active();
							if(save.data.reglage == null || save.data.reglage == undefined)
								save.data.reglage = new Object;
							save.data.reglage.song = true;
							save.flush();
						}
						break;
					case "btn_reglage_vibration" :
						if( evt.target.getActiver() ){
							evt.target.desactive();
							if(save.data.reglage == null || save.data.reglage == undefined)
								save.data.reglage = new Object;
							save.data.reglage.vibration = false;
							save.flush();
						}
						else if( !evt.target.getActiver() ){
							evt.target.active();
							if(save.data.reglage == null || save.data.reglage == undefined)
								save.data.reglage = new Object;
							save.data.reglage.vibration = true;
							save.flush();
						}
						break;
					case "btn_reglage_effetSon" :
						if( evt.target.getActiver() ){
							evt.target.desactive();
							if(save.data.reglage == null || save.data.reglage == undefined)
								save.data.reglage = new Object;
							save.data.reglage.effet_son = false;
							save.flush();
						}
						else if( !evt.target.getActiver() ){
							evt.target.active();
							if(save.data.reglage == null || save.data.reglage == undefined)
								save.data.reglage = new Object;
							save.data.reglage.effet_son = true;
							save.flush();
						}
						break;
					default :
						if( text_info.non_visible != null && !text_info.non_visible && !twenn_X.isPlaying){
							sortiMenu(text_info, sceneWidth/2, -440);
						}
						if( !reglage.non_visible && !twenn_X.isPlaying){
							sortiMenu(reglage, sceneWidth/2, -440);
							trace("reglage ", reglage.x, " wid ", reglage.width)
						}
				}
			}
		}
	}
}

//6d5bdf80cd27bf0ae82712b6215513c4ba80803bdaca03dc5fbfb26a601d2020
//http://www.msftconnecttest.com/redirect