﻿package classe.main{
	
    import flash.ui.MultitouchInputMode;
	import flash.ui.Multitouch;
	import flash.display.Loader;
	import flash.net.URLRequest;
    import flash.display.Stage;
    import flash.display.MovieClip; 
	import flash.display.StageAlign; 
	import flash.display.StageScaleMode; 
	import flash.events.Event; 
	import flash.events.*;
	import flash.utils.*;
	import flash.display.Sprite;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.media.Sound; 
	import flash.net.SharedObject;
 	import fl.transitions.*;
 	import fl.motion.Color;
	import flash.text.TextFormat;
	import fl.motion.Color;
	import flash.utils.Dictionary;
	import flash.ui.*;

	/*Importation des classe*/
	import classe.Plateau;
	import classe.Nivo;
	import classe.Neige;
	
	public class SaisiMain extends MainMain {

		public function SaisiMain(numNivo:int = 5, varSave:SharedObject=null, sonActive:Boolean = true){
		
			/*Declaration des class*/
			var nivo:Nivo = new Nivo;
			var num_nivo:int = numNivo;
			var vibrationActiver:Boolean = true;
			var son_active:Boolean;
			var effet_son_active:Boolean;

			var change_pause:Boolean = false;
			var fini:Boolean = false;

			var cache_haut:Cache_haut = new Cache_haut();
			cache_haut.gotoAndStop(2);
			cache_haut.point.mouseEnabled = false;
			cache_haut.gotoAndStop(1);
			
			var cache_bas:Cache_bas = new Cache_bas();
			cache_bas.mouseEnabled = false;
			cache_bas.gotoAndStop(2);
			addChild(cache_bas);

			var VarSave:SharedObject;
			var temps = cache_bas.temps;

			temps.text = String((nivo.plateau.get_nb_seconde_temps()/60)+":"+"00");

			var request:URLRequest = new URLRequest("ImageA/fond_2.png"); 
			var loader:Loader = new Loader();

			var pict;

			cache_bas.gotoAndStop(1);
			temps.mouseEnabled = false;
			cache_haut.mouseEnabled = false;

			nivo.plateau.effect.mouseEnabled = false;
			nivo.plateau.PointBuster.mouseEnabled = false;

			addChild(nivo);

			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			
			/*initialisation stage*/
			if( stage ){
				loader.load(request); 
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, pict_is_loaded)
			}
			else{
				addEventListener(Event.ADDED_TO_STAGE, init);
			}

			function init(e:Event):void{
				removeEventListener(Event.ADDED_TO_STAGE, init);
				loader.load(request); 
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, pict_is_loaded)
			}

			function pict_is_loaded (e:Event) {
				pict = loader.content;
			 	addChild(pict);

				launch();

			}
			
			function launch(){		
				pict.width = stage.stageWidth+80;
				pict.scaleY = pict.scaleX;
				pict.x = -30;
				pict.y = 0;
				
				inite();
					
				setChildIndex(pict, 0);
			}

			addEventListener(Event.DEACTIVATE, t);

			function t(evt:Event){
				nivo.plateau.sound.stop_song_monde();
			}

			function t2(evt:Event){
				if( !nivo.plateau.sound.instru_monde_actif )
					nivo.plateau.sound.joue_song_monde(nivo.plateau.get_num_song_monde());
			}

			function enable_events_mouse_move(){
				addEventListener(MouseEvent.MOUSE_DOWN, nivo.plateau.Click_pion);
				addEventListener(MouseEvent.MOUSE_MOVE, nivo.plateau.moveThatMouse);
				addEventListener(MouseEvent.MOUSE_UP, nivo.plateau.fl_ReleaseToDrop);
			}

			function disable_events_mouse_move(){
				removeEventListener(MouseEvent.MOUSE_DOWN, nivo.plateau.Click_pion);
				removeEventListener(MouseEvent.MOUSE_MOVE, nivo.plateau.moveThatMouse);
				removeEventListener(MouseEvent.MOUSE_UP, nivo.plateau.fl_ReleaseToDrop);
			}

			function inite(){
				nivo.plateau.stageWidth = stage.stageWidth;
				nivo.plateau.stageHeight = stage.stageHeight;
				VarSave = varSave;
				
				if( VarSave.data.size == 0 || VarSave.data.nivo[1].score == undefined){
					VarSave.data.nivo = new Array();
					VarSave.data.nivo[1] = new Object;
				}
				else if(VarSave.data.nivo[num_nivo] == undefined){
					VarSave.data.nivo[num_nivo] = new Object;
				}
				
				son_active = sonActive;
				nivo.plateau.sound.son_active = son_active;
				
				vibrationActiver = true;//this.parent["vibrationActiver"];	
				nivo.plateau.vibrationActiver = vibrationActiver;
				
				effet_son_active = true;//this.parent["effet_son_active"];
				nivo.plateau.sound.effet_son_active = effet_son_active;
				
				nivo.plateau.objectifs.objectifs( num_nivo );
				nivo.plateau.save = VarSave;
				nivo.lauch_nivo( num_nivo );

				stage.align = StageAlign.TOP;
				//stage.scaleMode = "exactFit";
				stage.scaleMode = StageScaleMode.SHOW_ALL;
						
				after_init();
			}

			function after_init(){
				
				enable_events_mouse_move();
				addEventListener (TransformGestureEvent.GESTURE_SWIPE, fl_SwipeHandler);
				stage.addEventListener(KeyboardEvent.KEY_DOWN, fl_SetKeyPressed);
				stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDowne1);
				addEventListener(KeyboardEvent.KEY_UP  , fl_OptionsMenuHandler2);
				
				if( !contains(cache_haut) ){
					cache_haut.x = 0;
					cache_haut.y = 0;
					cache_haut.width = stage.stageWidth;
					cache_haut.scaleY = cache_haut.scaleX;
					cache_haut.gotoAndStop(2);
					cache_haut.point.score.text = "0";
				}
				
				cache_bas.x = 0;
				cache_bas.y = stage.stageHeight-50;
				cache_bas.width = stage.stageWidth;
				cache_bas.height = 50;
				
				addChild(cache_bas);
				addChild(cache_haut);
				
				addChild(nivo.jauge);
				addChild(nivo.plateau.panneau_objectif_obj);
				setChildIndex(cache_bas, numChildren-1);
				setChildIndex(cache_haut, numChildren-1);
				
				if(nivo.plateau.objectifs.getScore() != null)
					cache_bas.gotoAndStop(2);
				
				temps = cache_bas.temps;
				
				addEventListener(Event.ACTIVATE, t2);
				addEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler);
			}


			/* Fais tourner le jeu */
			function fl_EnterFrameHandler(event:Event):void{
				anime_score(nivo.plateau.get_gameScore());
				fini = nivo.plateau.fini;

				/* Pause */
				if( nivo.plateau.get_var_pause() && !change_pause ){
					nivo.stop_gestion_nivo( num_nivo );
					disable_events_mouse_move();
					cache_bas.btn_pause.gotoAndStop(2);
					if( !fini )
						nivo.plateau.gameSprite.alpha = 0.1;
					change_pause = true;
				}
				else if( change_pause && !nivo.plateau.get_var_pause() ){
					nivo.start_gestion_nivo( num_nivo );
					enable_events_mouse_move();
					cache_bas.btn_pause.gotoAndStop(1);
					nivo.plateau.gameSprite.alpha = 1;
					change_pause = false;
				}
				
				/* Time */
				if( nivo.plateau.get_secondes() < 10 ){
					temps.text = String(nivo.plateau.get_minutes()+":0"+nivo.plateau.get_secondes());
				}
				else{
					temps.text = String(nivo.plateau.get_minutes()+":" +nivo.plateau.get_secondes());
				}
				
				setChildIndex(nivo.jauge, numChildren-1);
				setChildIndex(cache_bas, numChildren-1);
				setChildIndex(cache_haut, numChildren-1);
				setChildIndex(nivo.plateau.panneau_objectif_obj, numChildren-1);
				nivo.plateau.gameSprite.setChildIndex(nivo.plateau.deplacement, nivo.plateau.gameSprite.numChildren-1);

				if( fini ){
					VarSave = nivo.plateau.save;
					varSave = VarSave;//???
				}
			}

			function anime_score(score:Number){
				if( Number(cache_haut.point.score.text) < score ){
					cache_haut.point.score.text = String(Number(cache_haut.point.score.text) + 20);
				}
			}

			function baisser_score(gameScore, vitesse:Number){ 
				if( gameScore < 0 ){
					gameScore -= vitesse;
				}
			}

			/*Souris Menu*/
			addEventListener(MouseEvent.CLICK, Click);
			        
			function Click(evt:MouseEvent){
				var bip:sn_btn_bip = new sn_btn_bip;
				
				switch(evt.target.name)
				{
					case "continuer":
					{
						bip.play();
						nivo.plateau.pause();
						break;
					}
					case "btn_pause":
					{
						bip.play();
						if(!fini)
							nivo.plateau.pause();
						break;
					}
					case "retour":
					{
						bip.play();
						quit_this();
						maVar = "retour_menu";
						
						break;
					}
					case "replay":
					{
						bip.play();
						quit_this();
						maVar = "jouer_map";

						break;
					}
					case "continu":
					{
						
						bip.play();
						quit_this();
						maVar = "jouer";
						break;
					}
					case "menuBegin":
					{
						nivo.plateau.menuBeginDisparition();
						break;
					}
				}
			}

			function quit_this(){
				removeEventListener(MouseEvent.MOUSE_DOWN, Click);
				removeEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler);
				stage.removeEventListener(KeyboardEvent.KEY_DOWN, fl_SetKeyPressed);			
				removeEventListener (TransformGestureEvent.GESTURE_SWIPE, fl_SwipeHandler);
				stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDowne1);
				removeEventListener(KeyboardEvent.KEY_UP, fl_OptionsMenuHandler2);
				removeEventListener(Event.ACTIVATE, t2);
				removeEventListener(Event.DEACTIVATE, t);

				removeChild(nivo.jauge);
				removeChild(nivo.plateau.panneau_objectif_obj);
				removeChild(nivo);
				removeChild(cache_haut);
				removeChild(cache_bas);
				removeChild(pict);
				
				nivo.plateau.sound.stop_song_monde();
				nivo.plateau.timer_monter_stop();
				nivo.plateau.variable_null();
				disable_events_mouse_move();
				nivo.stop_gestion_nivo(num_nivo);
				
				cache_haut = null;
				cache_bas = null;
				nivo.jauge = null;
				nivo.plateau.panneau_objectif_obj = null;
				nivo = null; 
			}

			/* Clavier */
			function fl_SetKeyPressed(event:KeyboardEvent):void{
				
				switch (event.keyCode)
				{
					case Keyboard.Q :
					{
						nivo.plateau.monte_vite();
						break;
					}
					case Keyboard.ENTER :
					{
						if(!fini)
							nivo.plateau.pause();
						break;
					}
				}
			}


			/* Tactile Glissement de doigt */
			function fl_SwipeHandler(event:TransformGestureEvent):void{
				switch(event.offsetX)
				{
					// glissement vers la droite
					case 1:
					{
						break;
					}
					// glissement vers la gauche
					case -1:
					{
						break;
					}
				}

				switch(event.offsetY)
				{
					// glissement vers le bas
					case 1:
					{		
						// code personnalisé
						break;
					}
					// glissement vers le haut
					case -1:
					{
						nivo.plateau.monte_vite();
						break;
					}
				}
			}



			function onKeyDowne1(event:KeyboardEvent):void{
			    if( event.keyCode == Keyboard.BACK )
			    {
					event.preventDefault();
			    }
			}


			function fl_OptionsMenuHandler2(event:KeyboardEvent):void{
				if( event.keyCode == Keyboard.BACK )
			    {
					event.preventDefault();
					
					/* Pause */
					if( !nivo.plateau.get_var_pause() && !change_pause ){
						nivo.plateau.pause();
					}
					else if( change_pause ){
						nivo.plateau.pause();
					}
			    }
			}

			

		}
	}
}