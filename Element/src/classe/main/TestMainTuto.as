package classe.main {
	import flash.ui.MultitouchInputMode;
	import flash.ui.Multitouch;
	import flash.ui.*;
	import flash.display.Stage;
	import flash.display.*;
import flash.events.*;
import flash.text.TextFormat;
import flash.text.TextDisplayMode;
import flash.utils.Timer;
import flash.display.MovieClip;
import fl.motion.Color;
import fl.transitions.Tween;
import fl.transitions.easing.*;
import fl.transitions.TweenEvent;
import flash.utils.Timer;
import flash.events.Event;
import flash.text.engine.TypographicCase;
import flash.utils.Dictionary;
import flash.display.Sprite;
import flash.display.Loader;
import flash.net.URLRequest;
 

/*Importation des classe*/
import classe.Saisi;
import classe.Validiter;
import classe.VerifInit;
import classe.init_grid;
import classe.Effect;
import classe.Sounds;
import classe.Fond_grid;
import classe.Jauge;
import classe.Cercle;
import classe.Panneau_objectif_obj;
import classe.Objectifs;
import classe.Text_info;
import classe.Glace;
import classe.Fct_Glace;
import classe.Tab_tuto;
import classe.Maske;
import classe.MaskTuto;
import classe.TextTuto;
import classe.Element;
import classe.MainTuto;



	
    import flash.utils.*;
    import fl.motion.Color;
    import flash.display.Sprite;
    import flash.display.MovieClip;
    import flash.events.*;
    import flash.geom.*;
    
    import classe.Glace;
    import classe.Mur;
    import classe.Effect;

    import fl.transitions.Tween;
    import fl.transitions.easing.*;
    import fl.transitions.TweenEvent;

	public class TestMainTuto extends Sprite{
		Multitouch.inputMode = MultitouchInputMode.GESTURE;
		public function TestMainTuto(){
			/*******************************

					Initialisation

			********************************/

			/*Declaration des class*/
			var validiter:Validiter = new Validiter();
			var saisi:Saisi = new Saisi();
			var verif_inite:VerifInit = new VerifInit();
			var inite_grid:init_grid = new init_grid;
			var effe:Effect = new Effect();
			var sound:Sounds = new Sounds();
			var fond_gride:Fond_grid = new Fond_grid;
			var jauge:Jauge = new Jauge();
			var cercle:Cercle = new Cercle(40);
			var fct_Glace:Fct_Glace = new Fct_Glace;
			var tab_tuto:Tab_tuto = new Tab_tuto();
			var maske:Maske;
			var maskeTuto:MaskTuto = new MaskTuto;
			var textTuto:TextTuto = new TextTuto;
			var main:MainTuto = new MainTuto();


			var grid:Array; 

			var gameSprite:Sprite;
			var frstPiece:Element;
			var isDropping,isSwapping,isMonte:Boolean;
			var gameScore:int;
			var madeMove:Boolean=false;

			const numPieces:uint = 5;
			const spacing:Number = 50;
			const offsetX:Number = 0;
			var offsetY:Number = 0;



			//txt_tuto.mouseEnabled = false;

			//initialisation 
			var nvx:Number = 1;

			sound.joue_song_monde( Math.ceil(Math.random()*5 ) );
			//addChild(txt_tuto);

			function inite(){
				
				if(this.parent != null && this.parent.parent != null && this.parent.parent["varNvxTuto"] != 0 ){
					nvx = this.parent.parent["varNvxTuto"]
					this.parent.parent["varNvxTuto"] = 0;
				}

				var tab_init:Array = inite_grid.setUpGrid(gameSprite, grid, tab_tuto.array(nvx));
				gameSprite = tab_init[0];
				grid = tab_init[1];
				
				/*Placement gameSprite*/
				gameSprite.x = 25+(stage.stageWidth/2)-(gameSprite.width/2);
				gameSprite.y = 25+50;
				addChild(gameSprite);
				gameSprite.addChild(cercle);
				maske = new Maske;
				gameSprite.addChild(maske);
				gameSprite.addChild(main);
				
				if( maskeTuto.maske_position(nvx) != false ){
					maske.make_maske(gameSprite, maskeTuto.maske_position(nvx));
					maske.alpha = 0;
				}
				
				/*Texte*/
				/*txt_tuto.text = textTuto.getTextTuto(nvx);
				setChildIndex(txt_tuto, numChildren-1);
				
				txt_tuto.alpha = 0;*/
				
				/*Effet alpha texte*/
				/*appartition_text_tuto();
				function appartition_text_tuto(){
					if( txt_tuto.alpha < 1 ){
						txt_tuto.alpha += 0.1;
						setTimeout( appartition_text_tuto, 50);
					}
				}*/
				
				if( nvx == 16 ){
					fct_Glace.addGlace(grid[1][9], new Glace(50, 0, 0), grid);
				}
				
				/*Placement Pion*/
				animation_init_pion(grid);
				main.deplaceX(nvx);
				stage.align = StageAlign.TOP;
				stage.scaleMode = StageScaleMode.NO_SCALE;
			}

			/*Placement de pion aleatoire*/
			function place_pion_alea(grid:Array){
				var neg_pos:Number;
				for(var col=0;col<6;col++){
					for(var row=0; row<12; row++){
						/*Position negatif ou positif (-1 ou 1)*/
						neg_pos = Math.round(Math.random()*1);
						//trace(neg_pos);
						if(neg_pos != 0){
							neg_pos=-1;
						}
						if(grid[col][row]!=null){
							grid[col][row].x=((Math.random()*600)*neg_pos)+200;
							grid[col][row].y=Math.random()*200*neg_pos;
							grid[col][row].z=Math.random()*-500;
						}
						
					}
				}
			}



			var water_world_sound:mnd_water = new mnd_water();
			var myCha;

			function replace_pion(grid:Array, col, row:Number, spacing, offsetX, offsetY:int){
				
				var temp:Number = Math.random()*0.8+0.5;
				var twenplac:Array = new Array();
				
				if(grid[col][row] != null){
					twenplac.push( new Tween(grid[col][row], "x", Bounce.easeOut, grid[col][row].x, grid[col][row].col*spacing+offsetX, temp, true));
					twenplac.push( new Tween(grid[col][row], "y", Bounce.easeOut, grid[col][row].y, grid[col][row].row*spacing+offsetY, temp, true));
					twenplac.push( new Tween(grid[col][row], "z", Bounce.easeOut, grid[col][row].z, 0, temp, true));

				}
				setTimeout(replace, temp*1000+100);
				
				/*en cas de bug des Tweens*/
				function replace(){			
					/*Replacement des Pions*/
					for(var col=0; col<6; col++) {
						for(var row=0; row<13; row++) {
							deplacement.replacer(grid, col, row);
						}
					}
				}
				if(col == 5 && row == 11){
					twenplac[twenplac.length-1].addEventListener(TweenEvent.MOTION_FINISH, onFinish);
					
					function onFinish(e:TweenEvent):void{
						setTimeout(temp_aten, 100);
						
						/*Demarrage du jeu*/
						function temp_aten(){
							stage.addEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler_2);
							stage.addEventListener(MouseEvent.MOUSE_MOVE, moveThatMouse);
							stage.addEventListener(MouseEvent.MOUSE_UP, fl_ReleaseToDrop);
							stage.addEventListener(MouseEvent.MOUSE_DOWN, Click_pion);
							stage.addEventListener(KeyboardEvent.KEY_DOWN, fl_SetKeyPressed);
							stage.addEventListener (TransformGestureEvent.GESTURE_SWIPE, fl_SwipeHandler);

							/**/
							if(maske.alpha == 0)
								var tweenA:Tween = new Tween(maske, "alpha", Regular.easeInOut, 0, 1, 1, true);
						}
					}
				}
				
			}



			/*******************************

						Deplacement

			********************************/

			import classe.Deplacement;

			var deplacement:Deplacement = new Deplacement();
			deplacement.gameSprite = gameSprite;

			function animation_init_pion(grid:Array){
				place_pion_alea(grid);
				for(var col=0; col<6; col++) {
					for(var row=0; row<13; row++) {
						replace_pion(grid, col, row, deplacement.spacing, deplacement.offsetX, deplacement.offsetY);
					}
				}
			}


			var pion_selectionner:Object = null;

			//stage.addEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler_2);

			/*Fais tourner le jeu*/
			function fl_EnterFrameHandler_2(event:Event):void{
				/**/
				if( nvx == 9 ){
					if( grid[2][11]!= null && grid[2][11].type != 2 && grid[2][11-1].type == 2 ){
						grid[2][11-1].can_tombe = false;
						grid[2][11-2].can_tombe = false;
						maske.del_rect(2, 11);
						maske.del_rect(1, 11);
						maske.del_rect(0, 11);
						maske.add_rect(2, 10);
						maske.add_rect(3, 10);
						maske.add_rect(4, 10);
					}
					else if(grid[2][11]!= null && grid[2][11].type == 2 && grid[2][11-1].type == 2 ){
						grid[2][11-1].can_tombe = true;
						grid[2][11-2].can_tombe = true;
						
					}
					
				}
				else if( nvx == 10 ){
					if( grid[3][9] != null && grid[3][9].type != 5 && (grid[2][9] == null || grid[2][9].type != 4) && grid[2][10] != null && grid[2][10].type == 4){
						grid[2][10].can_tombe = false;
						maske.del_rect(2, 9);
						maske.del_rect(1, 9);
						maske.add_rect(3, 9);
					}
					else if( grid[3][9] != null && grid[3][9].type != 5 && grid[2][9] != null && grid[2][9].type == 4 && grid[2][8] != null && grid[2][8].can_tombe != true){
						grid[2][8].can_tombe = true;
						
					}
				}
				
				
				/*Replacement des Pions*/
				for(var col=0; col<6; col++) {
					for(var row=0; row<13; row++) {
						deplacement.replacer(grid, col, row);
						if( fct_Glace.condition_del_glace(grid, col, row) ){
							fct_Glace.delGlace( grid[col][row] );
						}
					}
				}

				/* Glace */
				fct_Glace.fn_glace( fct_Glace.lookForMatches_glace( grid ), grid );

				deplacement.deplace_pion(grid, pion_selectionner );
				if( pion_selectionner != null && pion_selectionner.selectionner )
					deplacement.lignedeplace_2( grid, pion_selectionner.col, pion_selectionner.row);
				
				/*cercle de selection*/
				if( cercle.isPetit() == false && pion_selectionner != null ){
					cercle.position_y = pion_selectionner.y;
					cercle.update();
				}
				
				/*Traite les correspondance*/
				fndAndRemoveMatches(gameSprite, grid);
				
				
				
				verif_inite.nul_avc_temp(gameSprite, grid);/*Descente de pion en cas de place*/
				gameSprite.setChildIndex(maske, gameSprite.numChildren-1);

			}


			/****************************
						Souris
			****************************/


			function suivre(pion_selectionner:Object){
				if( pion_selectionner != null && pion_selectionner is Element && pion_selectionner.selectionner == true
				){
					pion_selectionner.posXfin = Math.abs(Math.ceil( pion_selectionner.positionMouseX-gameSprite.mouseX) );
					pion_selectionner.posx = Math.abs(Math.ceil( pion_selectionner.positionMouseX-gameSprite.mouseX) );
					pion_selectionner.movee = true;
					pion_selectionner.addEventListener(Event.ENTER_FRAME, mvt);
				}
			}

			//stage.addEventListener(MouseEvent.MOUSE_MOVE, moveThatMouse);

			function moveThatMouse(evt:MouseEvent){
				suivre(pion_selectionner);
			}

			/*Deplacement fluid du pion*/
			function mvt(evt:Event) {
				if(evt.target.posXfin > 255){
					evt.target.posXfin = 250;
				}
				else if(evt.target.posXfin < 0){
					evt.target.posXfin = 0;
				}
				//trace(" moov "+evt.target.x);
				evt.target.x -= (evt.target.x - evt.target.posXfin)*0.3;
				//trace(" moov uuu "+evt.target.x);
				
				/*cercle de selection*/
				if( cercle.isPetit() == false ){
					cercle.position_x = evt.target.x;
					cercle.position_y = evt.target.y;
					cercle.update();
				}
				
				deplacement.lignedeplace_2( grid, evt.target.col, evt.target.row);
				if(Math.abs(evt.target.x - evt.target.posXfin) < 2 || evt.target.movee == false
				){
					//trace("ici "+evt.target.posXfin)
					evt.target.x = evt.target.posXfin;
					evt.target.posXfin = evt.target.enr;
					evt.target.movee = false;
					evt.target.removeEventListener(Event.ENTER_FRAME, mvt);
				
				}
				if( grid[evt.target.col][evt.target.row+1] == null 
				|| deplacement.verf(grid, evt.target) == true 
				){
					//trace("ici jjj "+evt.target.posXfin)
					evt.target.posXfin = evt.target.enr;
					evt.target.x = evt.target.posXfin;
					evt.target.movee = false;
					evt.target.selectionner = false;
					evt.target.alpha = 1;
					evt.target.removeEventListener(Event.ENTER_FRAME, mvt);
				
					cercle.position_x = pion_selectionner.x;
					cercle.position_y = pion_selectionner.y;
					cercle.retrecir_cercle();
				}
				else if((evt.target.x < evt.target.enr 
				&& (evt.target.col-1 >= 0 && grid[evt.target.col-1][evt.target.row] != null && deplacement.verf(grid, grid[evt.target.col-1][evt.target.row]) == true))
				|| (evt.target.x > evt.target.enr 
				&& (evt.target.col+1 < 6 && grid[evt.target.col+1][evt.target.row] != null && deplacement.verf(grid, grid[evt.target.col+1][evt.target.row]) == true))
				){
					//trace("ici set_rayon() y");
					evt.target.posXfin = evt.target.enr;
					evt.target.x = evt.target.posXfin;
					evt.target.movee = false;
					evt.target.removeEventListener(Event.ENTER_FRAME, mvt);
				
					
				}
			}

			/*deselection le pion aprés relachement du clic*/
			function apré_relache_souris(){
				if(pion_selectionner != null && pion_selectionner.selectionner == true  ){
					pion_selectionner.selectionner = false;
					pion_selectionner.alpha = 1;
					pion_selectionner.posx = null;
					
				}
				/*Cercle*/
				if( cercle.isPetit() == false  && pion_selectionner != null){
					cercle.position_x = pion_selectionner.x;
					cercle.position_y = pion_selectionner.y;
					cercle.retrecir_cercle();
				}
				else if ( cercle.isPetit() == false ){
					cercle.retrecir_cercle();
				}
			}


			//stage.addEventListener(MouseEvent.MOUSE_UP, fl_ReleaseToDrop);
			/*relachement du pion deplacer*/
			function fl_ReleaseToDrop(event:MouseEvent):void
			{
				apré_relache_souris();
			}


			//stage.addEventListener(MouseEvent.MOUSE_DOWN, Click_pion);
			/*Selection du pion à deplacer*/
			function Click_pion(pion:MouseEvent):void{
				
				if( pion.target != null && pion.target is Element && pion.target.type < 6 && pion.target.aligne != true){
					
					pion.target.selectionner = true;
					pion.target.enr = pion.target.col*spacing+offsetX;
					pion.target.positionMouseX = pion.target.mouseX;
					pion.target.alpha = 0.7;
					
					pion_selectionner = pion.target;
					//trace("pion_selectionner.type " + pion_selectionner.type);
					cercle.change_color(pion_selectionner.type);
					cercle.position_x = pion_selectionner.x;
					cercle.position_y = pion_selectionner.y;
					cercle.grossir_cercle();
					gameSprite.setChildIndex(cercle, gameSprite.numChildren-1);
					
					/*Place au 1er plan*/
					if( gameSprite.contains(grid[pion_selectionner.col][pion_selectionner.row]) ){
						gameSprite.setChildIndex(grid[pion_selectionner.col][pion_selectionner.row], gameSprite.numChildren-1);
					}
					main.stop();
				}
			}


			/***********************************

						Corespondance

			***********************************/

			import classe.Point_Burster;

			function pions_tousse_ateri( grid ){
				for(var col=0; col<6; col++) {
					for(var row=0; row<13; row++) {
						if( grid[col][row] != null && grid[col][row].ateri != true && grid[col][row].y == grid[col][row].row*spacing+deplacement.offsetY){
							grid[col][row].ateri = true;
							//grid[col][row].alpha = 1;
							//trace(grid[col][row].y == grid[col][row].row*spacing+deplacement.offsetY)
						}
					}
				}
			}

			function verif_pion_non_ater_exist(tab:Array):Boolean{
				for(var i:int=0; i<tab.length; i++) {
					for(var j:int=0; j<tab[i].length; j++) {
						if(tab[i][j] != null && tab[i][j].ateri == false ){
							return true;
						}
					}
				}
				return false;
			}

			/*Pour le grid (Tableau au complet)*/
			function verif_pion_non_ater_exist_g(grid:Array):Boolean{
				for(var i:int=0; i<6; i++) {
					for(var j:int=0; j<12; j++) {
						if(grid[i][j] != null && grid[i][j].ateri == false ){
							return true;
						}
					}
				}
				return false;
			}

			function pion_aligne_exist(grid:Array):Boolean{
				for(var col=0; col<6; col++) {
					for(var row=0; row<12; row++) {
						if( grid[col][row] != null && grid[col][row].aligne == true ){
							return true;
						}
					}
				}
				return false;
			}

			function fn_glace( tab:Array, grid:Array ){	
				for(var i=0; i<tab.length; i++){
					for(var e=0; e<tab[i].length; e++){
						if( tab[i][e] != null /*&& tab[i][e].can_deplace != true*/  ){
							fct_Glace.addGlace(tab[i][e], new Glace(50, 0, 0), grid);
						}
					}
				}
			}

			var PointBuster:Point_Burster = new Point_Burster();
			var nb_chain:int = 2;



			/*Pour manipuler les correspondances trouvées*/
			function fndAndRemoveMatches(gameSprite:Sprite, grid:Array):void {
				if( pion_selectionner != null )
					deplacement.replacer(grid, pion_selectionner.col, pion_selectionner.row);
				
				/* Obtenir la liste des correspondances !*/
				var matches:Array = validiter.lookForMatches( grid );
				var tab:Array = new Array();
				
				var verif_ater:Boolean = verif_inite.verif_avec_y( grid, deplacement.offsetY );
				
				var comb_effet:Number;
				var nb_boule_comb_effet;
				
				/*Pour l'effet tourbillon*/
				if(nb_chain > 1 && nb_chain < 9){
					nb_boule_comb_effet = nb_chain;
				}
				else{
					nb_boule_comb_effet = 8;
				}
					
				
				/*Pour chain combo*/
				if( matches.length != 0 && verif_pion_non_ater_exist(matches) ){
					nb_chain++;
					PointBuster.PointBurst(gameSprite, nb_chain-2, matches[0][0].x, matches[0][0].y+50+25, 2);
					sound.joue_song( nb_chain-1 );
					
					effe.effet_combo(gameSprite, matches[0][0].x, matches[0][0].y, 30, nb_boule_comb_effet, 2, 2);
					effe.set_rayon( (nb_chain-2)*30 );
					fond_gride.recolor();
				}
				else if( matches.length == 0 && verif_ater && !pion_aligne_exist(grid) ){
					if(nb_chain >= 4 && nb_chain < 6){
						sound.tada1.play();
					}
					else if(nb_chain >= 6 && nb_chain < 7){
						sound.tada2.play();
					}
					else if(nb_chain >= 7){
						sound.tada3.play();
					}
					
					effe.set_rayon(20);/*rayon des effets*/
					nb_chain = 2;
					fond_gride.recolor();
				}

				//trace( verif_ater )
				if( verif_ater ){
					
					pions_tousse_ateri( grid );
					deplacement.assert( verif_ater && !verif_pion_non_ater_exist_g(grid) )
				}

				

				for(var i:int=0; i<matches.length; i++) {
					
					if( i == 0 && matches[i][0].aligne != true && matches[i].length*matches.length > 3 ){
						//trace(" com "+matches.length+" matches[i].length "+i+" "+matches[i].length);
						PointBuster.PointBurst(gameSprite, matches[i].length*matches.length, matches[i][0].x, matches[i][0].y+25, 1);
						sound.joue_song(Math.ceil(Math.random()*25+1));
						if( nb_chain == 2)
							effe.effet_combo(gameSprite, matches[i][0].x, matches[i][0].y, 30, nb_boule_comb_effet, 2, 2);
					}
					
					for(var j:int=0; j<matches[i].length; j++) {
					
						if ( gameSprite.contains(matches[i][j]) && matches[i][j].aligne != true ){
							//trace("aligner "+i+" "+j+" = "+matches[i][j].type);
							matches[i][j].aligne = true;
							//fond_gride.change_color_case(matches[i][j].col, matches[i][j].row);
							tab.push(matches[i][j]);
						}
					}

				}
				
				animation_clignote(gameSprite, tab);
				
				
				
			}

			var vitesse_clignotement:int = 100;
			var temp_attente_avt_supr:int = 300;/*temp d'attente avant l'animation de la supression des pion*/

			/* Fais clignoter les Pions */
			function animation_clignote(gameSprite:Sprite, tab:Array ):Array{
				var compt = 0;
				
				/*timer pour repetition*/
				var time_clignote:Timer = new Timer(vitesse_clignotement, 7);
				time_clignote.addEventListener(TimerEvent.TIMER, time);
				time_clignote.start();
				//pause_timer_local(time_clignote);
				
				function time(){
					//pause_timer_local(time_clignote);
					change_style_image(gameSprite, tab);
					compt++;
					if(compt == 7){
						/*Attente avant disparition*/
						setTimeout(animation_suppression_pion, temp_attente_avt_supr, gameSprite, tab)
					}
				}
				
				return tab;
			}


			/* Change l'image du pion */
			function change_style_image( gameSprite:Sprite, tab:Array ){
				
				for(var i:int=0; i<tab.length; i++) {
					if(tab[i] != null && gameSprite.contains(tab[i]) ){
						
						if( tab[i].color_change == true){
							tab[i].color_change = false;
						
							//trace("ok")
							tab[i].gotoAndStop( tab[i].type );
						
						}
						else {
							tab[i].color_change = true;
							//trace("ok 4")
							tab[i].gotoAndStop( tab[i].type+"a3r" );
							
						}
					}
				}
				
			}



			/*vitesse de l'animation de disparition des pion un par un*/
			var vitesse_anime_supresion:int = 200;

			/**/
			function animation_suppression_pion(gameSprite:Sprite, tab:Array){
				var i=0;
				
				/*timer pour repetition*/
				var time_supression:Timer = new Timer(vitesse_anime_supresion, tab.length+1);
				time_supression.addEventListener(TimerEvent.TIMER, time);
				time_supression.start();
				//pause_timer_local(time_supression);
				
				
				function time(){
					//pause_timer_local(time_supression);
					if( i < tab.length ) {
						if( tab[i] != null && gameSprite.contains(tab[i]) && tab[i].alpha != 0 ){
							tab[i].alpha = 0;
							
							gameScore += 100*nb_chain-1;
							
							effe.effet_frame(gameSprite, tab[i].x, tab[i].y, tab[i], 2);
							
							sound.joue_song_plop(i+1, nb_chain-1);
							
							/**/
							//reload_objectif(tab[i]);
						}
						
					}
					else{
						suppression_pion(gameSprite, tab);
					
					}
					i++;
				}
				
			}

			/**/
			function suppression_pion(gameSprite:Sprite, tab:Array){

				for(var i=0; i<tab.length; i++){
					if( tab[i] != null && gameSprite.contains(tab[i]) ){
						gameSprite.removeChild(tab[i]);
						grid[tab[i].col][tab[i].row] = null;
						
						/*fais chuter les pions*/
						verif_inite.affectAbove_basic( grid, tab[i] );
					}
				}
				
				/**/
				if(verifi_objectif()){
					tuto_suivant();
				}
				else if(savoir_nb_pion_type_valide(grid) == false){
					tuto_remake();
				}
			}

			/*************************************


							Tuto


			*************************************/


			function efface_tab(){
				gameSprite.alpha = 0;
				parent.removeEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler_2);
				removeChild(gameSprite);
				if( maskeTuto.maske_position( nvx ) != false ){
					maske.del_maske();
				}
			}


			//compte le nombre de pion dans le tableau selon un type donné
			function compte_pion_type(tab:Array, tipe:Number):Number{
				var nb:Number=0;
				for(var col=0;col<6;col++){
					for(var row=0;row<12;row++){
						if(grid[col][row] != null && grid[col][row].type == tipe ){
							nb++;
						}
					}
				}
				return nb;
			}

			//savoir si le nombre de pion dans le tab et valide pour continuer
			function savoir_nb_pion_type_valide(tab):Boolean{
				//voir si le nombre de pion n'est pas >=3 ou =0
				for(var nbtipe=0; nbtipe<6; nbtipe++){
					if( compte_pion_type(tab, nbtipe) < 3 && compte_pion_type(tab, nbtipe) != 0  ){
						return false;
					}
				}
				return true;
			}

			//Fin verification

			function tuto_remake(){
				efface_tab();
				
				stage.removeEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler_2);
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveThatMouse);
				stage.removeEventListener(MouseEvent.MOUSE_UP, fl_ReleaseToDrop);
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, Click_pion);
				
				inite();
			}

			function tuto_suivant(){
				efface_tab();
				nvx++;
				if(nvx > tab_tuto.nb_max){
					//nvx = 1;
					if(this.parent.parent != null){
						this.parent.parent["maVar"] = "retour_map";
					}
				}
				
				
				stage.removeEventListener(Event.ENTER_FRAME, fl_EnterFrameHandler_2);
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveThatMouse);
				stage.removeEventListener(MouseEvent.MOUSE_UP, fl_ReleaseToDrop);
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, Click_pion);
				if( nvx <= tab_tuto.nb_max ){
					inite();
					if(nvx == 11)
						timer_monter_start();
				}
				trace(nvx," niveau ");
			}

			function verifi_objectif():Boolean{
				for(var col=0; col<6; col++){
					for(var row=0; row<12; row++){
						if(grid[col][row] != null && grid[col][row].type < 6){
							return false;
						}
					}
				}
				return true;
			}

			function compt_nb_pion(types:Number):Number{
				var compt:Number=0;
				for(var col=0;col<6;col++){
					for(var row=0;row<12;row++){
						if(grid[col][row]!=null && grid[col][row].type==types){
							compt++;
						}
					}
				}
				return compt
			}


			function fl_SetKeyPressed(event:KeyboardEvent):void{
				
				switch (event.keyCode)
				{
					case Keyboard.Q :
					{
						monte_vite();
						break;
					}
					case Keyboard.ENTER :
					{

						break;
					}
				}
			}

			


			function fl_SwipeHandler(event:TransformGestureEvent):void{
				switch(event.offsetX)
				{
					// glissement vers la droite
					case 1:
					{
						break;
					}
					// glissement vers la gauche
					case -1:
					{
						break;
					}
				}

				switch(event.offsetY)
				{
					// glissement vers le bas
					case 1:
					{		
						// code personnalisé
						break;
					}
					// glissement vers le haut
					case -1:
					{
						monte_vite();
						break;
					}
				}
			}

			/*Monter des pions*/
			function monte( gameSprite:Sprite, grid:Array ){
				/*Monter le cadre*/
				deplacement.offsetY -= 10;
				
				if(deplacement.offsetY < (-49) ){
					
					/*Descendre le cadre*/
					deplacement.offsetY += 50;
					
					for(var col=0; col<6; col++){
						for(var row=0; row<13; row++){
							
							/*Redonne leur couleur original au pion de la derniere ligne*/
							/*if( row == 12 ){
								couleur_origine( grid, col, row );
							}*/
							
							/*Monter les pion d'une ligne*/
							if (grid[col][row] != null){
								grid[col][row].row--;
								grid[col][row-1] = grid[col][row];
								grid[col][row] = null;
							}
						}
					}
					
					/*Ajouter des noveau pion a la derniere ligne*/
					for(col=0; col<6; col++){
						inite_grid.saisi.addPiece(gameSprite, grid, col, 12, 6, "b");
						grid[col][12].can_elimine = false;
					}
					
					/*Eviter qu'il y'est des corespondance dans la nouvel ligne*/
					/*var tab_new_element:Array = validiter.alignement_une_ligne(grid, 12);
					
					for(col=0; col<tab_new_element.length; col++){
						if( tab_new_element[col].length != 0 ){
							tab_new_element[col][0].type = 1+((tab_new_element[col][0].type + 1) % numPieces);
							tab_new_element[col][0].gotoAndStop(tab_new_element[col][0].type);
						}
						
					}
					
					var tab_new_el = validiter.tab_pion_similaire_au_dessu(grid, 12);
					
					for(col=0; col<tab_new_el.length; col++){
						tab_new_el[col].type = 1+((tab_new_el[col].type + 1) % numPieces);
						tab_new_el[col].gotoAndStop(tab_new_el[col].type);
					}*/
					
					//inite_grid.saisi.noircir_dernier_ligne_pion(grid);

				}

			}

			var vitesse_de_monter:int = 2000;
			var timer_monter:Timer = new Timer( vitesse_de_monter );

			function timer_monter_start(){
				timer_monter.addEventListener(TimerEvent.TIMER, time_monte);
				timer_monter.start();
			}


			function timer_monter_stop(){
				timer_monter.stop();
				timer_monter.removeEventListener(TimerEvent.TIMER, time_monte);
			}

			/*verfie si un objet touche le plafond*/
			function touche_plafond( grid:Array ):Boolean{
				for(var col=0; col<6; col++){
					if( grid[col][0] != null && grid[col][0].y <= 0 ){
						return true;
					}
				}
				return false;
			}


			/*Sert as savoir combien de fois la monter rapide doit etre effectué*/
			var nb_monte_utile:int = 0;
			var compt = 0;

			function time_monte(inputEvent:TimerEvent){
				if( timer_monter.delay == 20){
					compt++;
					//trace(compt);
					if( compt == nb_monte_utile ){
						compt = 0;
						timer_monter.delay = vitesse_de_monter;
					}
				}
				if( !touche_plafond(grid) && !pion_aligne_exist(grid) && verif_inite.verif_avec_y(grid, deplacement.offsetY) ){
					monte(gameSprite, grid);
				}
				
			}


			function monte_vite(){
				if( timer_monter.delay == vitesse_de_monter && compt == 0 && grid[0][12] != null && grid[0][12].y == grid[0][12].row*deplacement.spacing+deplacement.offsetY){
					//trace(timer_monter.delay)
					timer_monter.delay = 20;
					
					/*Calcul le nombre nécessaire de monté rapide qui devra etre effectuer*/
					nb_monte_utile = (50-Math.abs(deplacement.offsetY))/10;
				}
			}

			/*initialisation stage*/
			if( stage ){
				inite();
			}
			else{
				addEventListener(Event.ADDED_TO_STAGE, init);
			}

			function init(e:Event):void{
				removeEventListener(Event.ADDED_TO_STAGE, init);
				inite();
			}

			//stop();
		}
		//stop();
	}
}