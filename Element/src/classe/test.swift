import SpriteKit

class Test: SKSpriteNode {


    init() {
        let texture = SKTexture(imageNamed: "whateverImage.png")
        super.init(texture: texture, color: UIColor.clearColor(), size: texture.size())
        userInteractionEnabled = true
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Change Texture

    func updateTexture(newTexture: SKTexture) {
      self.texture = newTexture
   }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let location = (touches.first! ).locationInNode(scene!)
        position = scene!.convertPoint(location, toNode: parent!)

        print("touchesBegan: \(location)")
    }

    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let location = (touches.first! ).locationInNode(scene!)
        position = scene!.convertPoint(location, toNode: parent!)

        print("touchesMoved: \(location)")

    }

    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let location = (touches.first! ).locationInNode(scene!)
        position = scene!.convertPoint(location, toNode: parent!)

        print("touchesEnded: \(location)")
    }
}