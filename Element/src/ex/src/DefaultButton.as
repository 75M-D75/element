/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at

           http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License.

  The Original Code is ActionScriptFacile Framework.

  The Initial Developer of the Original Code is
  Matthieu  - http://www.actionscript-facile.com
  Portions created by the Initial Developer are Copyright (C) 2010
  the Initial Developer. All Rights Reserved.

  Contributor(s) :

*/

package
{

	import com.actionscriptfacile.ui.button.Button;
	import com.as3facileexemple.skin.classic.DefaultButtonSkin;// import du skin du boutton
	
	import flash.display.Sprite;
	
	
	/**
	 * Exemple d'utilisation du composant Button.
	 * @author Matthieu
	 */
	public class DefaultButton extends Sprite 
	{
		private var oButton : Button;
		
		public function DefaultButton():void 
		{
			// création un objet de type Button
			oButton = new Button();
			
			// définission du texte
			oButton.label = ".";
			
			// Application de la skin par défaut 
			// [ Attention ! Cette skin utilise le fichier ui.swc qui doit être ajouté à la liste des composants à passer au compilateur ]
			oButton.applySkin( new DefaultButtonSkin() );
			
			// ajout à la displaylsit
			addChild( oButton );
			
			// définition de sa taille en pixels
			oButton.resize( 130, 25 );
						
			// acccès au composant de type UITextField (labelField)
			oButton.labelField.alignCenter(); // centre le texte
			oButton.labelField.changeFormat("color", 0x000000);// changement de la couleur du texte
			oButton.labelField.changeFormat("size", 12);// changement de la taille de la police du texte
			oButton.labelField.changeFormat("font", "Arial");// changement de la police du texte
		}

		public function getObject() : Button
		{
			return	oButton;
		}
	}
}