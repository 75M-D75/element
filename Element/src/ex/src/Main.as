﻿/**

  The Initial Developer of the Original Code is
  Matthieu  - http://www.actionscript-facile.com
  Portions created by the Initial Developer are Copyright (C) 2010
  the Initial Developer. All Rights Reserved.

  Contributor(s) :

 */
package
{
	import asfiles.events.PlayerEvent;
	import asfiles.events.MetaEvent;
	import asfiles.utils.Player;
	
	import flash.media.Video;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.display.Sprite;
	
	import com.actionscriptfacile.ui.combo.element.ComboBoxElement;
	import com.actionscriptfacile.ui.utils.UIMargins;
	import com.as3facileexemple.skin.classic.DefaultComboBoxSkin;
	import com.actionscriptfacile.ui.combo.ComboBox;
	
	import com.google.ads.instream.api.CustomContentAd;
	import com.google.ads.instream.api.FlashAdCustomEvent;
	import com.google.ads.instream.api.AdSizeChangedEvent;
	import com.google.ads.instream.api.FlashAd;
	import com.google.ads.instream.api.VideoAd;
	import com.google.ads.instream.api.VastVideoAd;
	import com.google.ads.instream.api.AdLoadedEvent;
	import com.google.ads.instream.api.AdTypes;
	import com.google.ads.instream.api.Ad;
	import com.google.ads.instream.api.FlashAdsManager;
	import com.google.ads.instream.api.VideoAdsManager;
	import com.google.ads.instream.api.AdsManagerTypes;
	import com.google.ads.instream.api.AdEvent;
	import com.google.ads.instream.api.AdsManager;
	import com.google.ads.instream.api.AdsRequestType;
	import com.google.ads.instream.api.AdsRequest;
	import com.google.ads.instream.api.AdErrorEvent;
	import com.google.ads.instream.api.AdsLoadedEvent;
	import com.google.ads.instream.api.AdsLoader;



	import com.demonsters.debugger.MonsterDebugger;

	/**
	 * La fameuse classe Main qui étend Sprite :)
	 * 
	 */
	public class Main extends Sprite
	{
		private static const PUBLISHER_ID : String = "ca-games-test";
		// pour les tests ca-video-googletest1 / ca-games-test / ca-audio-test
		
		// Les différents types de séquences AdSense
		private static const VIDEO_AD_AFV : String = "AFV video ad";
		private static const TEXT_AD_AFV : String = "AFV text ad";
		private static const FULLSLOT_AD_AFV : String = "AFV Full Slot";
		private static const VIDEO_AD_AFV_WINS : String = "Dynamic Allocation AFV video wins";
		private static const VIDEO_AD_COMPANIONS_DCLK_WINS : String = "Dynamic Allocation DoubleClick InStream ad with HTML companions wins";
		private static const FLASH_AD_DCLK_WINS : String = "Dynamic Allocation DoubleClick Flash-in-Flash ad wins";
		private static const TEXT_AD_AFV_WINS : String = "Dynamic Allocation  AFV text overlay wins";
		private static const IMAGE_AD_AFV_WINS : String = "Dynamic Allocation AFV image ad wins";
		private static const FLASH_AD_COMPANIONS_DCLK_WINS : String = "Dynamic Allocation DoubleClick Flash-in-Flash ad with HTML" + " companions wins";
		private static const VIDEO_AD_DCLK : String = "DoubleClick InStream video ad";
		private static const VAST_DCLK : String = "DoubleClick VAST 2.0 ad with" + " companions";
		private static const FLASH_AD_DCLK : String = "DoubleClick Flash-In-Flash ad";
		
		private var adsManager : AdsManager;
		private var adsLoader : AdsLoader;
		
		private var oContainerAds : Sprite;
		
		private var oTypePubSequence : ComboBox;
		private var oTypePubAds : ComboBox;
		
		private var sChoosenSequence : String;
		private var sChoosenType : String;
		private var oPlayer : Player;
		private var oAdsVideo : Video;
		

		/**
		 * Constructeur.
		 * 
		 */
		public function Main()
		{
			// attend la fin de l'initialisation de la scène
			this.addEventListener(Event.ADDED_TO_STAGE, onReady, false, 0, true);
		}

		/**
		 * Démarrage de l'application.
		 * définit le nombre d'images par secondes dans les paramètres de compilation
		 * -default-frame-rate 40
		 * 
		 */
		private function onReady(event : Event) : void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onReady);

			// Init De MonsterDebugger
			MonsterDebugger.initialize(this);
			MonsterDebugger.trace(this, "Start Application");

			oContainerAds = new Sprite();
			oContainerAds.x = 30;
			oContainerAds.y = 50;
			this.addChild(oContainerAds);
			
			setupVideo();
			setupUi();
			setupAds();
		}

		/**
		 * Ajout d'une vidéo pour les tester les publicités Adsense.
		 * 
		 */
		private function setupVideo() : void
		{
			oPlayer = new Player();	
			oPlayer.x = 10;
			oPlayer.y = 130;	
			oPlayer.addEventListener (MetaEvent.ON_META, onMyMetaData);
			oPlayer.addEventListener (PlayerEvent.ON_PROGRESS, onProgress);
			addChild ( oPlayer );
			
			var oBtnPlayPause:DefaultButton = new DefaultButton();
			oBtnPlayPause.getObject().label = "Play / Pause";
			oBtnPlayPause.getObject().x = 10;
			oBtnPlayPause.getObject().y = oPlayer.y + 235;
			oBtnPlayPause.getObject().addEventListener ( MouseEvent.CLICK, onPauseClick );
			addChild ( oBtnPlayPause );
			
			// Récupération du film avec une variable FLASHVARS (path)
			var vars:Object = this.root.loaderInfo.parameters;
			oPlayer.play (vars.path != null ? vars.path : "http://www.actionscript-facile.com/ressources/videos/beyond-black-mesa.flv");

		}
		
		
		/**
		 * Play / Pause sur la vidéo.
		 * 
		 */
		private function onPauseClick(event : MouseEvent) : void
		{
			MonsterDebugger.trace(this, "---> onPauseClick");
			oPlayer.pause();
		}

		private function onProgress(event : PlayerEvent) : void
		{
		}

		private function onMyMetaData(event : MetaEvent) : void
		{
			MonsterDebugger.trace(this, "- onMyMetaData");
			oPlayer.pause();
		}

		
		/**
		 * Initialisation de l'interface utilisateur.
		 * 
		 */
		private function setupUi() : void
		{
			// combobox qui contient les différentes séquences Adsense
			oTypePubSequence = new ComboBox();
			oTypePubSequence.applySkin(new DefaultComboBoxSkin());
			// Définition du format du Titre de la ComboBox
			oTypePubSequence.currentLabel = "Choisir séquence AdSense";
			oTypePubSequence.currentElement.labelField.changeFormat("font", "Arial", -1, -1, true);
			oTypePubSequence.currentElement.labelField.changeFormat("color", Math.random() * 0X00FFFFFF, -1, -1, true);
			oTypePubSequence.currentElement.labelField.changeFormat("size", 13, -1, -1, true);
			oTypePubSequence.currentElement.labelField.changeFormat("bold", true, -1, -1, true);
			oTypePubSequence.currentElement.labelField.alignCenter();

			oTypePubSequence.resize(185, 40);
			oTypePubSequence.x = 10;
			oTypePubSequence.y = 10;
			oTypePubSequence.componentsHeight = 25;
			// hauteur des éléments de la CombobBox
			oTypePubSequence.margins = new UIMargins(2, 2, 2, 2);
			// ajout des marges au contenu de la liste
			oTypePubSequence.addEventListener(Event.CHANGE, onTypePubSequenceChangeHandler);

			// combobox pour les différents types de pub Adsense
			oTypePubAds = new ComboBox();
			oTypePubAds.applySkin(new DefaultComboBoxSkin());
			// Définition du format du Titre de la ComboBox
			oTypePubAds.currentLabel = "Choisir type AdSense";
			oTypePubAds.currentElement.labelField.changeFormat("font", "Arial", -1, -1, true);
			oTypePubAds.currentElement.labelField.changeFormat("color", Math.random() * 0X00FFFFFF, -1, -1, true);
			oTypePubAds.currentElement.labelField.changeFormat("size", 13, -1, -1, true);
			oTypePubAds.currentElement.labelField.changeFormat("bold", true, -1, -1, true);
			oTypePubAds.currentElement.labelField.alignCenter();

			oTypePubAds.resize(175, 40);
			oTypePubAds.x = oTypePubSequence.x + oTypePubSequence.width + 10;
			oTypePubAds.y = oTypePubSequence.y;
			oTypePubAds.componentsHeight = 25;
			// hauteur des éléments de la CombobBox
			oTypePubAds.margins = new UIMargins(2, 2, 2, 2);
			// ajout des marges au contenu de la liste
			oTypePubAds.addEventListener(Event.CHANGE, onTypePubAdsChangeHandler);

			// Création du bouton Valider
			var oBtnLoadAds : DefaultButton = new DefaultButton();
			oBtnLoadAds.getObject().label = "Charger Adsense";
			oBtnLoadAds.getObject().x = oTypePubAds.x + oTypePubAds.width + 10;
			oBtnLoadAds.getObject().y = oTypePubAds.y;
			oBtnLoadAds.getObject().addEventListener(MouseEvent.CLICK, onBtnAdsClickHandler);

			this.populateSequenceTypeComboBox();

			this.addChild(oTypePubSequence);
			this.addChild(oTypePubAds);
			this.addChild(oBtnLoadAds);
		}

		private function onBtnAdsClickHandler(event : MouseEvent) : void
		{
			MonsterDebugger.trace(this, "---> onBtnAdsClickHandler");
			this.cleanup();
			this.defineAds();
		}

		/**
		 * Supprime l'ancienne publicité.
		 * 
		 */
		private function cleanup() : void
		{
			if (adsManager)
			{
				removeListeners();
				
				if (adsManager.type == AdsManagerTypes.VIDEO)
				{
					// enlève la publicité Adsense de la vidéo en cours de lecture
					MonsterDebugger.trace(this, "DELETE VIDEO PUB ADS");

					oAdsVideo.clear();
					removeChild(oAdsVideo);
					oAdsVideo = null;
				}	
				
				adsManager.unload();
				adsManager = null;
				MonsterDebugger.trace(this, "Ad unloaded");
			}
		}

		/**
		 * Remplit la combobox avec les différentes séquences AdSense.
		 * 
		 */
		private function populateSequenceTypeComboBox() : void
		{
			var boxElement : ComboBoxElement;

			// ajout des éléments dans la combobox Séquence Adsense
			var aSequence : Array = new Array();
			aSequence.push(VIDEO_AD_AFV);
			aSequence.push(TEXT_AD_AFV);
			aSequence.push(FULLSLOT_AD_AFV);
			aSequence.push(VIDEO_AD_AFV_WINS);
			aSequence.push(VIDEO_AD_COMPANIONS_DCLK_WINS);
			aSequence.push(FLASH_AD_DCLK_WINS);
			aSequence.push(TEXT_AD_AFV_WINS);
			aSequence.push(IMAGE_AD_AFV_WINS);
			aSequence.push(FLASH_AD_COMPANIONS_DCLK_WINS);
			aSequence.push(VIDEO_AD_DCLK);
			aSequence.push(VAST_DCLK);
			aSequence.push(FLASH_AD_DCLK);

			// ajout des éléments dans la combobox Séquence Adsense
			for (var i : int = 0; i < aSequence.length; i++)
			{
				boxElement = oTypePubSequence.addElement(aSequence[i], aSequence[i]);
				boxElement.labelField.changeFormat("font", "Arial");
				boxElement.labelField.changeFormat("color", Math.random() * 0X00FFFFFF);
				boxElement.labelField.changeFormat("size", 13);
			}

			// ajout des éléments dans la combobox Type Adsense
			var aType : Array = new Array();
			aType.push("Par Défaut");
			aType.push(AdsRequestType.VIDEO);
			aType.push(AdsRequestType.TEXT_OVERLAY);
			aType.push(AdsRequestType.TEXT_FULL_SLOT);
			aType.push(AdsRequestType.GRAPHICAL);
			aType.push(AdsRequestType.GRAPHICAL_OVERLAY);
			aType.push(AdsRequestType.GRAPHICAL_FULL_SLOT);
			aType.push(AdsRequestType.TEXT_OR_GRAPHICAL);
			aType.push(AdsRequestType.FULL_SLOT);
			aType.push(AdsRequestType.OVERLAY);

			// ajout des éléments dans la combobox
			for (var j : int = 0; j < aType.length; j++)
			{
				boxElement = oTypePubAds.addElement(aType[j], aType[j]);
				boxElement.labelField.changeFormat("font", "Arial");
				boxElement.labelField.changeFormat("color", Math.random() * 0X00FFFFFF);
				boxElement.labelField.changeFormat("size", 13);
			}
		}

		/**
		 * Changement du type de séquence AdSense.
		 * 
		 */
		private function onTypePubSequenceChangeHandler(event : Event) : void
		{
			MonsterDebugger.trace(this, "onTypePubSequenceChangeHandler");
			sChoosenSequence = oTypePubSequence.currentLabel;
		}

		private function onTypePubAdsChangeHandler(event : Event) : void
		{
			MonsterDebugger.trace(this, "onTypePubAdsChangeHandler");
			sChoosenType = oTypePubAds.currentLabel;
		}

		/**
		 * Initialisation des publicités google adsende.
		 * 
		 */
		private function setupAds() : void
		{
			// Création du AdsLoader
			adsLoader = new AdsLoader();
			stage.addChild(adsLoader);
			adsLoader.addEventListener(AdsLoadedEvent.ADS_LOADED, onAdsLoaded);
			adsLoader.addEventListener(AdErrorEvent.AD_ERROR, onAdError);
		}
		
		
		/**
		 * Permet de créer une requète
		 * pour obtenir une publicités à partir des paramètres transmis.
		 * 
		 */
		private function createAdsRequest(adSlotWidth : Number, adSlotHeight : Number, adTagUrl : String, adType : String, channels : String, contentId : String, publisherId : String, disableCompanions : Boolean) : AdsRequest
		{
			var oAdsRequest : AdsRequest = new AdsRequest();
			oAdsRequest.adSlotWidth = adSlotWidth;
			oAdsRequest.adSlotHeight = adSlotHeight;

			oAdsRequest.adTagUrl = adTagUrl;
			
			if(sChoosenType!="Par Défaut") oAdsRequest.adType = sChoosenType;
			else oAdsRequest.adType = adType;

			var aChannels : Array = [channels];
			oAdsRequest.channels = aChannels;
			oAdsRequest.contentId = contentId;
			oAdsRequest.disableCompanionAds = disableCompanions;

			oAdsRequest.publisherId = publisherId;

			return oAdsRequest;
		}

		/**
		 * Création et chargement de la publicité Adsense définie.
		 * 
		 */
		private function defineAds() : void
		{
			MonsterDebugger.trace(this, "defineAds : " + sChoosenSequence);

			switch (sChoosenSequence)
			{
				case VIDEO_AD_AFV:
					adsLoader.requestAds(createAdsRequest(450, // adSlotWidth 
					250, // adSlotHight 
					"", // adTagUrl 
					AdsRequestType.VIDEO, // adType 
					"", // channels 
					"123", // contentId 
					PUBLISHER_ID, // publisherId 
					false));
					// disableCompanionAds
					break;
				case TEXT_AD_AFV:
					adsLoader.requestAds(createAdsRequest(450, // adSlotWidth 
					250, // adSlotHight 
					"", // adTagUrl 
					AdsRequestType.TEXT_OVERLAY, // adType 
					"", // channels 
					"123", // contentId 
					PUBLISHER_ID, // publisherId 
					false));
					// disableCompanionAds
					break;
				case FULLSLOT_AD_AFV:
					adsLoader.requestAds(createAdsRequest(400, // adSlotWidth 
					250, // adSlotHight 
					"", // adTagUrl 
					AdsRequestType.GRAPHICAL_FULL_SLOT, // adType 
					"", // channels 
					"123", // contentId 
					PUBLISHER_ID, // publisherId 
					false));
					// disableCompanionAds
					break;
				case VIDEO_AD_AFV_WINS:
					adsLoader.requestAds(createAdsRequest(300, // adSlotWidth 
					250, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/AngelaSite;" + "foo=prodtest;sz=728x90;dcmt=text/html", // adTagUrl 
					AdsRequestType.VIDEO, // adType 
					"", // channels 
					"123", // contentId 
					null, // publisherId 
					false));
					// disableCompanionAds
					break;
				case VIDEO_AD_COMPANIONS_DCLK_WINS:
					adsLoader.requestAds(createAdsRequest(500, // adSlotWidth 
					500, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/AngelaSite;" + "foo=instreamwins;sz=728x90;dcmt=text/html", // adTagUrl 
					AdsRequestType.VIDEO, // adType 
					"angela", // channels 
					"123", // contentId 
					null, // publisherId 
					false));
					// disableCompanionAds
					break;
				case FLASH_AD_DCLK_WINS:
					adsLoader.requestAds(createAdsRequest(500, // adSlotWidth 
					500, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/AngelaSite;" + "kw=dclkf2f;sz=120x350;ord=3577745;dcmt=text/html",
					// adTagUrl 
					AdsRequestType.OVERLAY, // adType 
					"angela", // channels 
					"123", // contentId 
					null, // publisherId 
					true));
					// disableCompanionAds
					break;
				case TEXT_AD_AFV_WINS:
					adsLoader.requestAds(createAdsRequest(450, // adSlotWidth 
					250, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/AngelaSite;" + "foo=prodtest;sz=728x90;dcmt=text/html", // adTagUrl 
					AdsRequestType.TEXT_OVERLAY, // adType 
					"", // channels 
					"123", // contentId 
					null, // publisherId 
					false));
					// disableCompanionAds
					break;
				case IMAGE_AD_AFV_WINS:
					adsLoader.requestAds(createAdsRequest(300, // adSlotWidth 
					250, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/AngelaSite;" + "foo=prodtest;sz=728x90;dcmt=text/html", // adTagUrl 
					AdsRequestType.GRAPHICAL_FULL_SLOT, // adType 
					"", // channels 
					"123", // contentId 
					null, // publisherId 
					false));
					// disableCompanionAds
					break;
				case FLASH_AD_COMPANIONS_DCLK_WINS:
					adsLoader.requestAds(createAdsRequest(300, // adSlotWidth 
					250, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/AngelaSite;" + "foo=f2fwins;sz=728x90;dcmt=text/html", // adTagUrl 
					AdsRequestType.OVERLAY, // adType 
					"angela", // channels 
					"123", // contentId 
					null, // publisherId 
					false));
					// disableCompanionAds
					break;
				case VIDEO_AD_DCLK:
					adsLoader.requestAds(createAdsRequest(500, // adSlotWidth 
					500, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/AngelaSite;" + "kw=dclkvideo;sz=120x350;ord=3577745;dcmt=text/html",
					// adTagUrl 
					AdsRequestType.VIDEO, // adType 
					"", // channels 
					null, // contentId 
					null, // publisherId 
					false));
					// disableCompanionAds
					break;
				case VAST_DCLK:
					adsLoader.requestAds(createAdsRequest(500, // adSlotWidth 
					500, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/N270.132652." + "1516607168321/B3442378.3;dcadv=1379578;sz=0x0;" + "ord=3577745;dcmt=text/xml", // adTagUrl 
					AdsRequestType.VIDEO, // adType 
					"", // channels 
					null, // contentId 
					null, // publisherId 
					false));
					// disableCompanionAds
					break;
				case FLASH_AD_DCLK:
					adsLoader.requestAds(createAdsRequest(500, // adSlotWidth 
					500, // adSlotHight 
					"http://ad.doubleclick.net/pfadx/AngelaSite;" + "kw=dclkf2f;sz=120x350;ord=3577745;dcmt=text/html",
					// adTagUrl 
					AdsRequestType.OVERLAY, // adType 
					"", // channels 
					null, // contentId 
					null, // publisherId 
					false));
					// disableCompanionAds
					break;
				default:
					MonsterDebugger.trace(this, "Paramètres création Adsense inconnus !!!");
			}
		}

		/**
		 * Erreur de chargement des publicités.
		 * 
		 */
		private function onAdError(event : AdErrorEvent) : void
		{
			MonsterDebugger.trace(this, "onAdError !!!");
			MonsterDebugger.trace(this, event);
		}

		/**
		 * Publicités chargées.
		 * 
		 */
		private function onAdsLoaded(event : AdsLoadedEvent) : void
		{
			MonsterDebugger.trace(this, "onAdsLoaded");
			MonsterDebugger.trace(this, event);

			// Get AdsManager
			adsManager = event.adsManager;

			adsManager.addEventListener(AdErrorEvent.AD_ERROR, onAdError);
			adsManager.addEventListener(AdEvent.CONTENT_PAUSE_REQUESTED, onPauseRequested);
			adsManager.addEventListener(AdEvent.CONTENT_RESUME_REQUESTED, onResumeRequested);

			adsManager.addEventListener(AdLoadedEvent.LOADED, onAdLoaded);
			adsManager.addEventListener(AdEvent.STARTED, onAdStarted);
			adsManager.addEventListener(AdEvent.CLICK, onAdClicked);

			displayAdsInformation(adsManager);

			if (adsManager.type == AdsManagerTypes.FLASH)
			{
				MonsterDebugger.trace(this, "adsManager.type : " + AdsManagerTypes.FLASH);

				var flashAdsManager : FlashAdsManager = adsManager as FlashAdsManager;
				flashAdsManager.addEventListener(AdSizeChangedEvent.SIZE_CHANGED, onFlashAdSizeChanged);
				flashAdsManager.addEventListener(FlashAdCustomEvent.CUSTOM_EVENT, onFlashAdCustomEvent);

				flashAdsManager.x = 30;
				flashAdsManager.y = 150;
				MonsterDebugger.trace(this, "Calling load, then play");
				flashAdsManager.load();
				flashAdsManager.play();
			}
			else if (adsManager.type == AdsManagerTypes.VIDEO)
			{
				MonsterDebugger.trace(this, "adsManager.type : " + AdsManagerTypes.VIDEO);

				var videoAdsManager : VideoAdsManager = adsManager as VideoAdsManager;
				videoAdsManager.addEventListener(AdEvent.STOPPED, onVideoAdStopped);
				videoAdsManager.addEventListener(AdEvent.PAUSED, onVideoAdPaused);
				videoAdsManager.addEventListener(AdEvent.COMPLETE, onVideoAdComplete);
				videoAdsManager.addEventListener(AdEvent.MIDPOINT, onVideoAdMidpoint);
				videoAdsManager.addEventListener(AdEvent.FIRST_QUARTILE, onVideoAdFirstQuartile);
				videoAdsManager.addEventListener(AdEvent.THIRD_QUARTILE, onVideoAdThirdQuartile);
				videoAdsManager.addEventListener(AdEvent.RESTARTED, onVideoAdRestarted);
				videoAdsManager.addEventListener(AdEvent.VOLUME_MUTED, onVideoAdVolumeMuted);
				MonsterDebugger.trace(this, "Setting click tracking");
				
				// clickMovieClip est déclaré dans le fla, sur le stage.
				videoAdsManager.clickTrackingElement = oContainerAds;
				
				MonsterDebugger.trace(this, "Calling load, then play");
				
				oAdsVideo = new Video();
				oAdsVideo.x = oPlayer.x;
				oAdsVideo.y = oPlayer.y;
				addChild(oAdsVideo);
				videoAdsManager.load(oAdsVideo);
				videoAdsManager.play();
			}
			else if (adsManager.type == AdsManagerTypes.CUSTOM_CONTENT)
			{
				// Cannot call play() since it is custom content.
				// You can get the content string from the ad and further process it as
				// required.
				for each (var adCustom:CustomContentAd in adsManager.ads)
				{
					MonsterDebugger.trace(this, adCustom.content);
				}
			}
		}

		private function onVideoAdVolumeMuted(event : AdEvent) : void
		{
		}

		private function onVideoAdRestarted(event : AdEvent) : void
		{
		}

		private function onVideoAdThirdQuartile(event : AdEvent) : void
		{
		}

		private function onVideoAdFirstQuartile(event : AdEvent) : void
		{
		}

		private function onVideoAdMidpoint(event : AdEvent) : void
		{
		}

		private function onVideoAdPaused(event : AdEvent) : void
		{
		}

		private function onVideoAdStopped(event : AdEvent) : void
		{
		}

		private function onFlashAdCustomEvent(event : FlashAdCustomEvent) : void
		{
		}

		private function onFlashAdSizeChanged(event : AdSizeChangedEvent) : void
		{
		}

		/**
		 * Affiche les informations de la publicité en cours.
		 * 
		 */
		private function displayAdsInformation(adsManager : AdsManager) : void
		{
			MonsterDebugger.trace(this, "AdsManager type: " + adsManager.type);

			var ads : Array = adsManager.ads;

			MonsterDebugger.trace(this, ads.length + " ads loaded");

			for each (var ad:Ad in ads)
			{
				try
				{
					// APIs defined on Ad
					MonsterDebugger.trace(this, "type: " + ad.type);
					MonsterDebugger.trace(this, "id: " + ad.id);
					MonsterDebugger.trace(this, "traffickingParameters: " + ad.traffickingParameters);
					MonsterDebugger.trace(this, "surveyUrl: " + ad.surveyUrl);
					// Check the companion type from flashVars to decide whether to use
					// GUT or getCompanionAds() to load companions.
					/*if (!useGUT)
					{
					renderHtmlCompanionAd(ad.getCompanionAds(CompanionAdEnvironments.HTML, 300, 250), "300x250");
					renderHtmlCompanionAd(ad.getCompanionAds(CompanionAdEnvironments.HTML, 728, 90), "728x90");
					}*/
					if (ad.type == AdTypes.VAST)
					{
						var vastAd : VastVideoAd = ad as VastVideoAd;
						MonsterDebugger.trace(this, "description: " + vastAd.description);
						MonsterDebugger.trace(this, "adSystem: " + vastAd.adSystem);
						MonsterDebugger.trace(this, "customClicks: " + vastAd.customClicks);
					}
					else if (ad.type == AdTypes.VIDEO)
					{
						// APIs defined on all video ads
						var videoAd : VideoAd = ad as VideoAd;
						MonsterDebugger.trace(this, "author: " + videoAd.author);
						MonsterDebugger.trace(this, "title: " + videoAd.title);
						MonsterDebugger.trace(this, "ISCI: " + videoAd.ISCI);
						MonsterDebugger.trace(this, "deliveryType: " + videoAd.deliveryType);
						MonsterDebugger.trace(this, "mediaUrl: " + videoAd.mediaUrl);
						// getCompanionAdUrl will throw error for VAST ads.
						MonsterDebugger.trace(this, "getCompanionAdUrl: " + ad.getCompanionAdUrl("flash"));
					}
					else if (ad.type == AdTypes.FLASH)
					{
						// API defined on FlashAd
						var flashAd : FlashAd = ad as FlashAd;
						if (flashAd.asset != null)
						{
							MonsterDebugger.trace(this, "asset: " + flashAd.asset);
							MonsterDebugger.trace(this, "asset x: " + flashAd.asset.x);
							MonsterDebugger.trace(this, "asset y: " + flashAd.asset.y);
							MonsterDebugger.trace(this, "asset height: " + flashAd.asset.height);
							MonsterDebugger.trace(this, "asset width: " + flashAd.asset.width);
						}
						else
						{
							MonsterDebugger.trace(this, "Error: flashAsset is null.");
						}
					}
				}
				catch (error : Error)
				{
					MonsterDebugger.trace(this, "Error type: " + error + " message: " + error.message);
				}
			}
		}

		private function onAdClicked(event : AdEvent) : void
		{
			MonsterDebugger.trace(this, "onAdClicked");
		}

		private function onAdStarted(event : AdEvent) : void
		{
			MonsterDebugger.trace(this, "onAdStarted");
			oPlayer.pause();// mise en pause de la vidéo pour lancer Adsense Vidéo
		}

		private function onAdLoaded(event : AdLoadedEvent) : void
		{
			MonsterDebugger.trace(this, "onAdLoaded");
		}

		private function onVideoAdComplete(event : AdEvent) : void
		{
			MonsterDebugger.trace(this, "onVideoAdComplete");
		}

		/**
		 * Supprime tous les écouteurs sur adsManager.
		 * 
		 */
		private function removeListeners() : void
		{
			adsManager.removeEventListener(AdEvent.CONTENT_PAUSE_REQUESTED, onPauseRequested);
			adsManager.removeEventListener(AdEvent.CONTENT_RESUME_REQUESTED, onResumeRequested);
			adsManager.removeEventListener(AdEvent.COMPLETE, onVideoAdComplete);
			adsManager.removeEventListener(AdErrorEvent.AD_ERROR, onAdError);
			adsManager.removeEventListener(AdEvent.CLICK, onAdClicked);

			adsManager.removeEventListener(AdLoadedEvent.LOADED, onAdLoaded);
			adsManager.removeEventListener(AdEvent.STARTED, onAdStarted);

			if (adsManager.type == AdsManagerTypes.VIDEO)
			{
				var videoAdsManager : VideoAdsManager = adsManager as VideoAdsManager;
				videoAdsManager.removeEventListener(AdEvent.STOPPED, onVideoAdStopped);
				videoAdsManager.removeEventListener(AdEvent.PAUSED, onVideoAdPaused);
				videoAdsManager.removeEventListener(AdEvent.COMPLETE, onVideoAdComplete);
				videoAdsManager.removeEventListener(AdEvent.MIDPOINT, onVideoAdMidpoint);
				videoAdsManager.removeEventListener(AdEvent.FIRST_QUARTILE, onVideoAdFirstQuartile);
				videoAdsManager.removeEventListener(AdEvent.THIRD_QUARTILE, onVideoAdThirdQuartile);
				videoAdsManager.removeEventListener(AdEvent.RESTARTED, onVideoAdRestarted);
				videoAdsManager.removeEventListener(AdEvent.VOLUME_MUTED, onVideoAdVolumeMuted);				
			}
			else if (adsManager.type == AdsManagerTypes.FLASH)
			{
				var flashAdsManager : FlashAdsManager = adsManager as FlashAdsManager;
				flashAdsManager.removeEventListener(AdSizeChangedEvent.SIZE_CHANGED, onFlashAdSizeChanged);
				flashAdsManager.removeEventListener(FlashAdCustomEvent.CUSTOM_EVENT, onFlashAdCustomEvent);
			}
		}

		private function onResumeRequested(event : AdEvent) : void
		{
			MonsterDebugger.trace(this, "onResumeRequested");
		}

		private function onPauseRequested(event : AdEvent) : void
		{
			MonsterDebugger.trace(this, "onPauseRequested");
		}
	}
}
