/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at

           http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License.

  The Original Code is ActionScriptFacile Framework.

  The Initial Developer of the Original Code is
  Matthieu  - http://www.actionscript-facile.com
  Portions created by the Initial Developer are Copyright (C) 2010
  the Initial Developer. All Rights Reserved.

  Contributor(s) :

*/

package com.as3facileexemple.skin.classic
{
	// Import des classes gérant la partie graphique du composant dans le fla (movieclip)
	// Provient de ui.swc (créé avec la compilation de UI.fla)
	import com.as3facile.skin.button.ButtonDownSkin;	import com.as3facile.skin.button.ButtonOverSkin;	import com.as3facile.skin.button.ButtonUpSkin;
	
	import com.actionscriptfacile.skin.Skin;
	import com.actionscriptfacile.ui.button.Button;
	
	
	/**
	 * Définition du skin utilisé pour un composant Button
	 * 
	 * @author Matthieu
	 */
	public class DefaultButtonSkin extends Skin
	{
		
		public function DefaultButtonSkin() 
		{
			// Affectation de chaque élément du bouton à un skin
			setSkin( Button.BUTTON_DOWN_SKIN , ButtonDownSkin );// élément du bouton, classe graphique (movieclip) associée
			setSkin( Button.BUTTON_OVER_SKIN, ButtonOverSkin );
			setSkin( Button.BUTTON_UP_SKIN , ButtonUpSkin );
		}
		
	}

}