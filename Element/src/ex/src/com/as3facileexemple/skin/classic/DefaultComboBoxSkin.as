/*

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at

           http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License.

  The Original Code is ActionScriptFacile Framework.

  The Initial Developer of the Original Code is
  Matthieu  - http://www.actionscript-facile.com
  Portions created by the Initial Developer are Copyright (C) 2010
  the Initial Developer. All Rights Reserved.

  Contributor(s) :

*/
package com.as3facileexemple.skin.classic
{

	import com.as3facile.skin.button.ButtonUpSkin;
	import com.as3facile.skin.button.ButtonOverSkin;
	import com.as3facile.skin.button.ButtonDownSkin;
   	import com.actionscriptfacile.ui.scroll.components.ScrollDownButton;
    import com.actionscriptfacile.ui.scroll.components.ScrollUpButton;
    import com.actionscriptfacile.ui.scroll.components.ScrollerButton;
    import com.actionscriptfacile.skin.Skin;
    import com.actionscriptfacile.ui.combo.ComboBox;
    import com.actionscriptfacile.ui.list.List;
    import com.actionscriptfacile.ui.scroll.components.VerticalScrollBar;
   
    // Import des classes gérant la partie graphique du composant dans le fla (movieclip)
    // Provient de ui.swc (créé avec la compilation de UI.fla)
    import com.as3facile.skin.combo.ComboBoxButtonDownSkin;
    import com.as3facile.skin.combo.ComboBoxButtonOverSkin;
    import com.as3facile.skin.combo.ComboBoxButtonSkin;
    import com.as3facile.skin.combo.ComboBoxElementDownSkin;
    import com.as3facile.skin.combo.ComboBoxElementOverSkin;
    import com.as3facile.skin.combo.ComboBoxElementSkin;
    import com.as3facile.skin.list.ListBackgroundSkin;

    import com.as3facile.skin.scroll.ScrollBarBackgroundSkin;
    import com.as3facile.skin.scroll.ScrollBottomButtonSkin;
    import com.as3facile.skin.scroll.ScrollUpButtonSkin;
    import com.as3facile.skin.scroll.ScrollerButtonSkin;
   
    import com.as3facile.skin.scroll.ScrollerButtonDownSkin;
    import com.as3facile.skin.scroll.ScrollerButtonOverSkin;

    import com.as3facile.skin.scroll.ScrollUpButtonDownSkin;
    import com.as3facile.skin.scroll.ScrollUpButtonOverSkin;
   
    import com.as3facile.skin.scroll.ScrollBottomButtonDownSkin;
    import com.as3facile.skin.scroll.ScrollBottomButtonOverSkin;
   

   
   
   
    /**
     * Définition du skin utilisé pour un composant ComboBox
     *
     * @author Matthieu
     */
    public class DefaultComboBoxSkin extends Skin
    {
       
        public function DefaultComboBoxSkin()
        {
           /* setSkin( ComboBox.COMBOBOX_BUTTON_DOWN_SKIN , ComboBoxButtonDownSkin );
            setSkin( ComboBox.COMBOBOX_BUTTON_OVER_SKIN, ComboBoxButtonOverSkin );
            setSkin( ComboBox.COMBOBOX_BUTTON_UP_SKIN , ComboBoxButtonSkin );*/

			setSkin( ComboBox.COMBOBOX_BUTTON_DOWN_SKIN , ButtonDownSkin );// élément du bouton, classe graphique (movieclip) associée
			setSkin( ComboBox.COMBOBOX_BUTTON_OVER_SKIN, ButtonOverSkin );
			setSkin( ComboBox.COMBOBOX_BUTTON_UP_SKIN , ButtonUpSkin );


            setSkin( ComboBox.COMBOBOX_ELEMENT_DOWN_SKIN , ComboBoxElementDownSkin );
            setSkin( ComboBox.COMBOBOX_ELEMENT_OVER_SKIN, ComboBoxElementOverSkin );
            setSkin( ComboBox.COMBOBOX_ELEMENT_UP_SKIN , ComboBoxElementSkin );

            setSkin( List.LIST_BACKGROUND_SKIN, ListBackgroundSkin  );

            // background
            setSkin( VerticalScrollBar.SCROLL_VERTICAL_BACKGROUND_SKIN, ScrollBarBackgroundSkin );

            // scroller
            setSkin( ScrollerButton.SCROLLER_VERTICAL_DOWN_SKIN, ScrollerButtonDownSkin );
            setSkin( ScrollerButton.SCROLLER_VERTICAL_OVER_SKIN, ScrollerButtonOverSkin );
            setSkin( ScrollerButton.SCROLLER_VERTICAL_UP_SKIN, ScrollerButtonSkin );

            // scroll up
            setSkin( ScrollUpButton.SCROLLUP_VERTICAL_DOWN_SKIN, ScrollUpButtonDownSkin );
            setSkin( ScrollUpButton.SCROLLUP_VERTICAL_OVER_SKIN, ScrollUpButtonOverSkin );
            setSkin( ScrollUpButton.SCROLLUP_VERTICAL_UP_SKIN, ScrollUpButtonSkin );

            // scroll down
            setSkin( ScrollDownButton.SCROLLDOWN_VERTICAL_DOWN_SKIN, ScrollBottomButtonDownSkin );
            setSkin( ScrollDownButton.SCROLLDOWN_VERTICAL_OVER_SKIN, ScrollBottomButtonOverSkin );
            setSkin( ScrollDownButton.SCROLLDOWN_VERTICAL_UP_SKIN, ScrollBottomButtonSkin );
        }
       
    }

}